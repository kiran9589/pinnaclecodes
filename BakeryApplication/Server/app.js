var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cron = require("node-cron");

var pool = require('../Server/config');
var _ = require('underscore');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var masterRouter = require('./routes/master');
var offerRouter = require('./routes/offer');
var orderRouter = require('./routes/order');
var productRouter = require('./routes/product');
var roleRouter = require('./routes/role');
const messageRouter = require('./routes/message');
const templateRouter = require('./routes/templatemessage');


var app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/productImg', express.static('./public/images/product'));
app.use('/offerImg', express.static('./public/images/offer'));
app.use('/userImg', express.static('./public/images/user'));
app.use('/categoryImg', express.static('./public/images/category'));

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/offer', offerRouter);
app.use('/product', productRouter);
app.use('/master', masterRouter);
app.use('/order', orderRouter);
app.use('/role', roleRouter);
app.use('/message', messageRouter);
app.use('/template', templateRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// This is cron job execute to update all expired offers
cron.schedule("5 0 * * *", function() {
  console.log("running job every day at midnight 12:05 am");
  var query = `update offer set is_active = false, updated_on = 'now()' where to_date < now() and is_active=true`
  pool.query(query, (error, results) => {
    if(error){
      console.log(" Error occurred while updating offer : ",error);
    }  
  });
});

module.exports = app;
