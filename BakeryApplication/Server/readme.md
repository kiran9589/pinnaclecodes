Database Installation and configuration : 

    After installation of psql follow below steps:

    1) Connect to db
        > psql postgres
        
    2) Check information of connected db by which user
        postgres=# \conninfo
        
    3) Create new role for your use 
        postgres=# CREATE USER root WITH PASSWORD 'password';
        
    4) Create database: 
        postgres=# CREATE DATABASE bakery;
        
    5) Grant priviliges :
        postgres=# GRANT ALL PRIVILEGES ON DATABASE bakery to root;
        
        ==== If have to REVOKE PRIVILEGES  then use this
        postgres=# REVOKE ALL PRIVILEGES ON DATABASE bakery from root;
        
        ==== This is optional = Add access to created role
        postgres=# ALTER ROLE root WITH CREATEDB;
        
    6) Exit from current role
    postgres=# \q
    
    7) connect to db using user
        psql -U root -h 127.0.0.1 bakery

        
    8) Some importatan commands:
        \q 	>  Exit psql connection
        \c 	> Connect to a new database
        \dt 	> List all tables
        \du 	> List all roles
        \list 	> List databases
        
        drop database dbname


============ Application running process:
goto Application folder and execute following command
node index.js

Import backup database :

psql -h hostname -d databasename -U username -f file.sql

default server running: 
sudo npm install pm2 -g
pm2
pm2 ./bin/www 
pm2 start ./bin/www 
pm2 show www
pm2 logs www
pm2 help
pm2 --help
pm2 list



adminUser:
username: superaddmin
pwd: welcome 

Local: 
pwd : Welcome@123