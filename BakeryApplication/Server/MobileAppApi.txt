1) User creation:
	url : '{{host}}:{{port}}/user/create'
	method: 'POST'
	body : form-data
	payload :{
		userdetails : {
			"name": "Dattaatray Suryawanshi",
			"mobile": "9028143962", 1) User creation:
	url : '{{host}}:{{port}}/user/create'
	method: 'POST'
	body : form-data
	payload :{
		userdetails : {
			"name": "Dattaatray Suryawanshi",
			"mobile": "9028143962", 
			"email": "dattaatray_suryawanshi@gmail.com",
			"roleId": 1       
		}
		fileInput: file		
	}
		
	res:{
		"status": "Success",
		"message": "User created successfully."
	}
	
2) User creation by referalCode:
	url : '{{host}}:{{port}}/user/create'
	method: 'POST'
	body : form-data
	payload :{
		userdetails : {
			"name": "Kiran B",
			"mobile": "9552875545", 
			"email": "",
			"roleId": 1,
			"referBy": "RDvHJnht"    
		}

		fileInput: file		
	}
		
	res:{
		"status": "Success",
		"message": "User created successfully."
	}
			"email": "dattaatray_suryawanshi@gmail.com",
			"roleId": 1       
		}
		fileInput: file		
	}
		
	res:{
		"status": "Success",
		"message": "User created successfully."
	}
	
2) User creation by referalCode:
	url : '{{host}}:{{port}}/user/create'
	method: 'POST'
	body : form-data
	payload :{
		userdetails : {
			"name": "Kiran B",
			"mobile": "9552875545", 
			"email": "",
			"roleId": 1,
			"referBy": "RDvHJnht"    
		}

		fileInput: file		
	}
		
	res:{
		"status": "Success",
		"message": "User created successfully."
	}
	
3) Delete user
	URL: '{{host}}:{{port}}/user/delete/3'   -- (3 - is user ID)

	METHOD:  DELETE

	RES:  {
	"status": "Success",
	"message": "User has been deactivate successfully."
	}

4) Reactivate user :  // This is for temprary call for client side. For testing purpose only

	URL: '{{host}}:{{port}}/user/reactivate/3'  -- (3 - is user ID)

	METHOD:  GET

	RES:  {
	"status": "Success",
	"message": "User has been reactivated successfully."
	}

5) send OTP for login.

	URL: '{{host}}:{{port}}/message/sendotp'
	Method: POST
	Payload {
	"mobile_no": <<MobileNumber>>
	}

6) verify OTP for login validation.

	URL: '{{host}}:{{port}}/message/verifyotp'
	Method: POST
	Payload {
		"mobile_no": <<MobileNumber>>,
		"otp": 2376
	}

7) Add address : 
      URL : "{{host}}:{{port}}/user/add/address"
      Method: POST
      Payload : {
	    "userId": 1,
	    "address": "new address 2",
	    "city": "Mumbai",
	    "state": "ZH",
	    "country": "",
	    "pincode": 411041,
	    "addressType": "home",
	    "isDefault": false
     }
      Response: {
	    "status": "Success",
	    "message": "User address added successfully."
      }

8) Update Address:
      URL : "{{host}}:{{port}}/user/address/update"
      Method: POST
      Payload : {
	"id": 10,
	"userId": 1,
	"address": "new test 10",
	"city": "Pune",
	"state": "",
	"country": "",
	"pincode": 411045,
	"addressType": "other",
	"isDefault": true
	}
      Response: {
	    "status": "Success",
	    "message": "User address details updated successfully."
	}

9) Delete address: 
      URL : "{{host}}:{{port}}/user/address/delete/7"
      Method: DELETE
      Response: {
    "status": "Success",
    "message": "Address has been deleted successfully."
}

10) Get address: 
      URL : "{{host}}:{{port}}/user/address/1"
      Method: GET
      Response: [
	    {
		"id": 10,
		"address": "new test 10",
		"city": "Pune",
		"state": "",
		"country": "",
		"pincode": 411045,
		"address_type": "other",
		"is_active": true,
		"is_default": true
	    },
	    {
		"id": 4,
		"address": "jgdkfgd",
		"city": "Pune",
		"state": "",
		"country": "",
		"pincode": 411013,
		"address_type": "office",
		"is_active": true,
		"is_default": false
	    },
	    {
		"id": 3,
		"address": "D-23, MNahgdloi, hjdfgsdj",
		"city": "Pune",
		"state": "MH",
		"country": "IN",
		"pincode": 411013,
		"address_type": "home",
		"is_active": true,
		"is_default": false
	    },
	    {
		"id": 8,
		"address": "new address 2",
		"city": "New Mumbai",
		"state": "Mh",
		"country": "",
		"pincode": 411040,
		"address_type": "office",
		"is_active": true,
		"is_default": false
	    },
	    {
		"id": 9,
		"address": "new address 2",
		"city": "Mumbai",
		"state": "ZH",
		"country": "",
		"pincode": 411041,
		"address_type": "home",
		"is_active": true,
		"is_default": false
	    },
	    {
		"id": 6,
		"address": "new address",
		"city": "Nashik",
		"state": "Mh",
		"country": "",
		"pincode": 411045,
		"address_type": "other",
		"is_active": true,
		"is_default": false
	    },
	    {
		"id": 7,
		"address": "new address 1",
		"city": "Mumbai",
		"state": "Mh",
		"country": "",
		"pincode": 411043,
		"address_type": "office",
		"is_active": true,
		"is_default": false
	    }
	]

11) Get RewardPoints (Wallet) :

	URL: '{{host}}:{{port}}/user/rewardPoints/1'  -- (1 - is user ID)

	METHOD:  GET


12) Refer app link with friend

	URl : '{{host}}:{{port}}/message/newReferals'
	payload: {
		"sender": 9028143962,
		"receivers": [9552875545, 7387438077, 8237815306],
		"referalLink": "http://prod.test.com" // this is optional when our app get depployed on playstore we will fix it from backend
	}

	Res: {
	    "message": "Refral links sent successfully"
	}

13) Get categories :
	url : '{{host}}:{{port}}/master/categories'
	method: 'GET',
	res : [{
			"id": 1,
			"name": "CUPCAKES",
			"created_on": "2019-06-15T10:52:37.621Z",
			"updated_on": null
		},
		{
			"id": 2,
			"name": "MOUSSE CAKES",
			"created_on": "2019-06-15T10:54:14.178Z",
			"updated_on": null
		},
		{
			"id": 3,
			"name": "CUSTOMIZED CAKES",
			"created_on": "2019-06-15T10:54:27.803Z",
			"updated_on": null
		},
		{
			"id": 4,
			"name": "LAYERED CAKES",
			"created_on": "2019-06-15T10:54:41.400Z",
			"updated_on": null
		}
	]


14) Dashboard / product list : 
	url : '{{host}}:{{port}}/product/list'
	method: 'POST'
	payload :{
		"start": 0,
		"limit": 2
	}
	
	res : {
			"totalCount": "3",
			"products": [
				{
					"p_id": 1,
					"productCode": "",
					"productName": "BELGIAN TRUFFLE",
					"category": "CUPCAKES",
					"imageName": "1560597533887_BELGIANTRUFFLE-32.jpg",
					"imagePath": "/productImg/1560597533887_BELGIANTRUFFLE-32.jpg",
					"weightPrice": [
						"{\"weight\":\"0\",\"price\":32}"
					]
				},
				{
					"p_id": 4,
					"productCode": "",
					"productName": "BLACK FOREST",
					"category": "CUPCAKES",
					"imageName": "1560598693997_BLACKFOREST-38.jpg",
					"imagePath": "/productImg/1560598693997_BLACKFOREST-38.jpg",
					"weightPrice": [
						"{\"weight\":\"0\",\"price\":38}"
					]
				},
				{
					"p_id": 5,
					"productCode": "",
					"productName": "BLUEBERRY",
					"category": "CUPCAKES",
					"imageName": "1560600270197_BLUEBERRY-60.jpg",
					"imagePath": "/productImg/1560600270197_BLUEBERRY-60.jpg",
					"weightPrice": [
						"{\"weight\":\"0\",\"price\":60}"
					]
				}
			]
		}


15) Get products by Category 
	url : '{{host}}:{{port}}/product/list'
	method: 'POST'
	payload :{
		"start": 0,
		"limit": 2,
		"category": 1
	}
	
	res : {
			"totalCount": "3",
			"products": [
				{
					"p_id": 1,
					"productCode": "",
					"productName": "BELGIAN TRUFFLE",
					"category": "CUPCAKES",
					"imageName": "1560597533887_BELGIANTRUFFLE-32.jpg",
					"imagePath": "/productImg/1560597533887_BELGIANTRUFFLE-32.jpg",
					"weightPrice": [
						"{\"weight\":\"0\",\"price\":32}"
					]
				},
				{
					"p_id": 4,
					"productCode": "",
					"productName": "BLACK FOREST",
					"category": "CUPCAKES",
					"imageName": "1560598693997_BLACKFOREST-38.jpg",
					"imagePath": "/productImg/1560598693997_BLACKFOREST-38.jpg",
					"weightPrice": [
						"{\"weight\":\"0\",\"price\":38}"
					]
				},
				{
					"p_id": 5,
					"productCode": "",
					"productName": "BLUEBERRY",
					"category": "CUPCAKES",
					"imageName": "1560600270197_BLUEBERRY-60.jpg",
					"imagePath": "/productImg/1560600270197_BLUEBERRY-60.jpg",
					"weightPrice": [
						"{\"weight\":\"0\",\"price\":60}"
					]
				}
			]
		}

16) Get product details by product id:
	url : '{{host}}:{{port}}/product/1'
	method: 'GET'
	res :  {
		"p_id": 1,
		"productCode": "",
		"productName": "BELGIAN TRUFFLE",
		"category": "CUPCAKES",
		"imageName": "1560597533887_BELGIANTRUFFLE-32.jpg",
		"imagePath": "/productImg/1560597533887_BELGIANTRUFFLE-32.jpg",
		"weightPrice": [
			"{\"weight\":\"0\",\"price\":32}"
		],
		"reviews": [
			{
				"review": "very good profuct3",
				"userName": "Dattaatray Suryawanshi",
				"rating": 1
			},
			{
				"review": "very good profuct3",
				"userName": "Kiran B",
				"rating": 5
			}
		]
	}

17) Post review on product : 
	url : '{{host}}:{{port}}/product/review'
	method: 'POST'
	payload :{
		"userId": 2,
		"productId": 1,
		"review": "very good profuct3",
		"rating":5
	}
	
	res : {
		"status": "Success",
		"message": "Product review updated successfully."
	}

18) Update Review: 
	URl:  '{{host}}:{{port}}/product/updatereview'
	Method: "POST'
	Request Payload : {
		        "reviewId": 22,
		         "review": "CHeck review update",
		         "rating":2
	}
	Res : {
	    "status": "Success",
	    "message": "Product review updated successfully."
	}

19) Delete Review: 
	url : '{{host}}:{{port}}/product/review/22' (22- Review Id)
	METHOD: 'DELETE'
	Resp: {
	    "status": "Success",
	    "message": "Review deleted successfully."
	}

20) Get all offers :
	url : '{{host}}:{{port}}/offer/list'
	method: 'POST'
	payload :{
		"start": 0,
		"limit": 2
	}
	
	res : {
			"totalCount": "3",
			"offers": [
				{
					"o_id": 6,
					"offerValue": 120,
					"offerType": "flat",
					"offerDesc": "Flat 120 off on all product for first time transaction",
					"imageName": "fileInput_1560603303346_android-icon-72x72.png",
					"imagePath": "/offerImg/fileInput_1560603303346_android-icon-72x72.png",
					"offerCode": "FLAT120",
					"offerValidFrom": "2019-01-01",
					"offerValidTo": "2019-12-31"
				},
				{
					"o_id": 7,
					"offerValue": 10,
					"offerType": "point",
					"offerDesc": "On purchasing of 100 you will get 10 points",
					"imageName": null,
					"imagePath": null,
					"offerCode": "PT10",
					"offerValidFrom": "2019-01-01",
					"offerValidTo": "2019-12-31"
				},
				{
					"o_id": 8,
					"offerValue": 10,
					"offerType": "percent",
					"offerDesc": "10% off on all products",
					"imageName": null,
					"imagePath": null,
					"offerCode": "GET10",
					"offerValidFrom": "2019-01-01",
					"offerValidTo": "2019-12-31"
				}
			]
		}
	
	
21) Create order without refral points:
	url : '{{host}}:{{port}}/order/create'
	method: 'POST'
	payload : {
		"userId": 2,
		"paymentMode": "CREDIT_CARD",
		"paymentStatus": "Complete",
		"transactionNum":"TXN009377UTyv01",
		"paymentDate": "2019-06-15T14:32:00.000",
		"totalAmount":"1200",
		"orderDate": "2019-06-15T14:30:00.000",
		"offerId": 4,
		"productList":[
			{"productId": 1,"weight":null,"quantity":1,"price": "20.00"},
			{"productId": 4,"weight":null,"quantity":1,"price": "50.00"},
			{"productId": 5,"weight":"1kg","quantity":1,"price": "250.00"}
		]		
	}
	
	res : {
		"status": "Success",
		"message": "Order has been placed successfully."
	}
	
22) Create order with refral points:
	url : '{{host}}:{{port}}/order/create'
	method: 'POST'
	payload :{
		"userId": 1,
		"paymentMode": "CREDIT_CARD",
		"paymentStatus": "Complete",
		"transactionNum":"TXN009377UTyv01",
		"paymentDate": "2019-05-28T14:34:00.000",
		"totalAmount":"1000",
		"orderDate": "2019-05-28T14:34:00.000",
		"pointUsed": -10, // This will send only if user used refral points or is any offer give refral points
		"productList":[
			{"productId": 1,"offerId":null,"quantity":1,"price": "450.00"},
			{"productId": 14,"offerId":4,"quantity":1,"price": "450.00"},
			{"productId": 18,"offerId":1,"quantity":1,"price": "450.00"}
		]
		
	}
	
	res : {
		"status": "Success",
		"message": "Order has been placed successfully."
	}

	
23) Get orders:
	url : '{{host}}:{{port}}/order/list'
	method: 'POST'
	payload :{
		"userId":1,
		"start":0,
		"limit":10
	}
	
	res :{
		"totalCount": "2",
		"orders": [
			{
				"orderId": 1,
				"paymentMode": "CREDIT_CARD",
				"paymentStatus": "Complete",
				"transactionNum": "TXN009377UTyv01",
				"paymentDate": "2019-05-28 14:34:00",
				"orderStatus": "INITIATE",
				"totalAmount": "1000.00",
				"orderDate": "2019-05-28 14:34:00"
			},
			{
				"orderId": 2,
				"paymentMode": "CREDIT_CARD",
				"paymentStatus": "Complete",
				"transactionNum": "TXN009377UTyv01",
				"paymentDate": "2019-05-28 14:34:00",
				"orderStatus": "INITIATE",
				"totalAmount": "1000.00",
				"orderDate": "2019-05-28 14:34:00"
			}
		]
	}

24) Get Template list:
GET ALL
URL:   '{{host}}:{{port}}/template'

 

GET BY ID
URL:   '{{host}}:{{port}}/template/1' (1- Template id)

Method: GET

MT1: Greeting type,

MT2: Reference message for others

MT3: Cake ordered message,

MT4: About us

 

Response: [

    {

        "id": 1,

        "msg": "Greetings from Couches ..\n    Hope, your cake experience was good. \n    We have come up with a referral scheme, wherein if you refer us to your near/dear ones, you will earn 5% reward points, if the referee orders. ( 1 point = to Rs 1/-). This can be redeemed during your next order.\n    You can send next message which has your reference link.\n    Thanks",

        "msg_type": "MT1",

        "created_on": "2019-07-23T21:02:23.794Z",

        "updated_on": null

    },

    {

        "id": 2,

        "msg": "Hi, I ordered a cake from Couches, and found it to be very well baked and delicious.\n     Most importantly the rates are very competitive. \n     I highly recommend Couches .. a must try cake to make your event more pleasant.",

        "msg_type": "MT2",

        "created_on": "2019-07-23T21:02:37.414Z",

        "updated_on": null

    },

    {

        "id": 3,

        "msg": "Greetings from Couches ..\n    Get ready to have a blast !\n    Your cake will be delivered today right to your doorstep. Pls provide OTP to delivery boy for confirmation.\n    Thanks",

        "msg_type": "MT3",

        "created_on": "2019-07-23T21:02:46.239Z",

        "updated_on": null

    },

    {

        "id": 4,

        "msg": "Welcome to the COUCHES .. THE PATISSERIE, a Pune based online bakery!\nIt all started by KDB, who embarked on her career as a Chef from Institute of Hotel Management, Jaipur.\nWith a passion for baking, she took her talent and creativity to the next level by baking a very finest range of customized cakes that can immediately bring a Joy to any event. Here we season each of our cake with the European and modern techniques to take you to the rich creamy world.\nWith a belief that‘Cakes are the Trademarks of celebration’, she started an online Premium Cake venture- COUCHES .. THE PATISSERIE!",

        "msg_type": "MT4",

        "created_on": "2019-07-23T21:02:54.934Z",

        "updated_on": null

    }

]