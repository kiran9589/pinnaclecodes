var pool = require('../../config');
var _ = require('underscore');
var moment = require('moment');
const { sendMail } = require('../utils/commonutil');

const createOrder = (request, response) => {
    const {userId,paymentMode,paymentStatus,transactionNum,paymentDate,totalAmount,orderDate, pointUsed,offerId} = request.body
    var productList = JSON.stringify(request.body.productList);
    var orderId = null; 
    var status = "INITIATE";
    
    pool.query('INSERT INTO orders (user_id, payment_mode,payment_status,transaction_num,payment_date,status,total_amount,order_date ,offer_id,created_on) VALUES ($1, $2, $3, $4, $5, $6, $7,$8,$9,now())  RETURNING id', [userId,paymentMode,paymentStatus,transactionNum,paymentDate,status,totalAmount,orderDate,offerId], (error, result) => {
        if (error) {
            response.status(500).json("There is some intruption whlie placing your order. Please try in while.")
        }

        orderId = result.rows[0].id;
        if(orderId){
            initOrderItems(response,orderId, productList);
            pool.query('select * from orders where user_id= '+userId, (error, result) => {
                if(error){
                    return false;
                }
                if(result.rows.length === 1){
                    initRandRPoints(userId)
                }
            });

            if(pointUsed){
                creditDebitReferPoint(null, pointUsed, userId, null);
            }
        } 
        //response.status(200).json({"status": "Success", "message":"Order has been placed successfully."});
    })
}



function initRandRPoints(userId){
    var query =  'select * from reward_and_recognition where reciever_id= '+userId+' and is_active = true';
    pool.query(query, (error, result) => {
        if(error){
            return false;
        }

        if(result.rows.length > 0){
            var randRobject = result.rows[0];
            pool.query('select * from refer_type', (error, referResult) => {
                if(referResult.rows){
                    _.each(referResult.rows, function(referType, referTypeIndex) {
                        if(referType.type === 'sender'){
                            // Credit point for sender 
                            creditDebitReferPoint(randRobject.id, referType.points ,randRobject.sender_id, referType.id);
                        } else if(referType.type === 'reciever'){
                            // Credit point for reciever 
                            creditDebitReferPoint(randRobject.id, referType.points ,randRobject.reciever_id, referType.id);
                        }
                    });

                    pool.query('UPDATE reward_and_recognition SET is_active = $1, updated_on = now() WHERE id = $2',[false, randRobject.id],
                        (error, results) => {
                            if (error) {
                                return false;
                            }
                            return true;
                        }
                    )
                }
            });
        }
    });
}

function creditDebitReferPoint(randRId, points ,userId, referTypeId){
    pool.query('INSERT INTO credit_randr_point (randr_id,points,user_id,refer_type_id, created_on) VALUES ($1, $2, $3, $4, now())', [randRId, points,userId, referTypeId], (error, result) => {
        if (error) {
            return false;
        }
        return true;
    })
}

function initOrderItems(response, orderId, productList){
    var productList = JSON.parse(productList);

    _.each(productList, function(product, prodIndex) {
        var productId = product.cart_productId,
            quantity = product.cart_quantity,
            weight = product.cart_weight,
            price = product.cart_price,
            nameOnCake = product.nameoncake
            deliveryDate = product.delivery_date,
            occasion = product.occasion;
    
        pool.query('INSERT INTO order_items (order_id ,product_id, quantity, price, weight,name_on_cake, delivery_date, occasion) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [orderId ,productId, quantity, price, weight, nameOnCake,deliveryDate, occasion], (error, result) => {
            if (error) {
                response.status(500).json("There is some intruption whlie adding order items. Please try in while.")
            }

            if(productList.length === prodIndex + 1){
                getOrderDetails(orderId, response, null);
                // response.status(200).json({"status": "Success", "message":"Order has been placed successfully."});
            }
        })
    });  

}

const getOrders = (request, response) => {
    var userId = request.body.userId
    const { start, limit} = request.body
    // const status = request.body.status;
    // var statusList = ["INITIATE", "CONFIRMED","ONTHEWAY"]
    // if(status === 'past'){
    //     statusList = ["CANCELED","DELIVERED"]
    // }

    var orders = [];
    //  and status in ('${statusList.join("','")}')
    var query = `SELECT o.*, count(o.*) OVER() AS full_count from orders o where user_id =${userId} ORDER BY o.order_date DESC`;

    if(_.has(request.body, "start") && _.has(request.body, "limit")){
        query = `SELECT o.*, count(o.*) OVER() AS full_count from orders o 
            where user_id =${userId}
            ORDER BY o.order_date DESC
            OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`;
    }

    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var totalCount = 0;
        if(results){
            _.each(results.rows, function(order) {
                var orderDetails = { 
                    orderId: order.id,
                    paymentMode: order.payment_mode, 
                    paymentStatus: order.payment_status,
                    transactionNum: order.transaction_num,
                    paymentDate: moment(order.payment_date).format('YYYY-MM-DD HH:mm:ss'),
                    orderStatus: order.status,
                    totalAmount: order.total_amount,
                    orderDate: moment(order.order_date).format('YYYY-MM-DD HH:mm:ss')
                }
    
                totalCount = order.full_count;
                orders.push(orderDetails);
            });
        }
        
        response.status(200).json({totalCount: totalCount, orders:orders})
    })
}

const getAllOrders = (request, response) => {
    var orders = [];
    var query = `SELECT o.*, count(o.*) OVER() AS full_count from orders o ORDER BY o.order_date DESC`;

    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var totalCount = 0;
        if(results){
            _.each(results.rows, function(order) {
                var orderDetails = { 
                    orderId: order.id,
                    paymentMode: order.payment_mode, 
                    paymentStatus: order.payment_status,
                    transactionNum: order.transaction_num,
                    paymentDate: moment(order.payment_date).format('YYYY-MM-DD HH:mm:ss'),
                    orderStatus: order.status,
                    totalAmount: order.total_amount,
                    orderDate: moment(order.order_date).format('YYYY-MM-DD HH:mm:ss')
                }
    
                totalCount = order.full_count;
                orders.push(orderDetails);
            });
        }
        
        response.status(200).json({totalCount: totalCount, orders:orders})
    })
}

const pendingOrders = (request, response) => {
    var orders = [];
    var query = `SELECT o.*, count(o.*) OVER() AS full_count from orders o where o.status not in ('DELIVERED', 'CANCELED', 'INCOMPLETE') ORDER BY o.order_date DESC`;

    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var totalCount = 0;
        if(results){
            _.each(results.rows, function(order) {
                var orderDetails = { 
                    orderId: order.id,
                    paymentMode: order.payment_mode, 
                    paymentStatus: order.payment_status,
                    transactionNum: order.transaction_num,
                    paymentDate: moment(order.payment_date).format('YYYY-MM-DD HH:mm:ss'),
                    orderStatus: order.status,
                    totalAmount: order.total_amount,
                    orderDate: moment(order.order_date).format('YYYY-MM-DD HH:mm:ss')
                }
    
                totalCount = order.full_count;
                orders.push(orderDetails);
            });
        }
        
        response.status(200).json({totalCount: totalCount, orders:orders})
    })
}

const updateOrderStatus = (request, response) => {
    const {id, status} = request.body;    
    pool.query("update orders SET status = $2, updated_on='now()' WHERE id = $1",[id, status], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while updating order status.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"Order status updated successfully."});
    });
}
  
const getOrderById = (request, response) => {
    const id = parseInt(request.params.id)
    const userId = parseInt(request.params.userId)
    getOrderDetails(id, response, userId);
}


function getOrderDetails(id, response, userId){
    var query = `SELECT o.*, ofr.id "offerId", ofr.offer_desc, a.name "userName", a.mobile_no, aa.address, aa.city, aa.state, aa.pincode, aa.country  FROM orders o
                inner join account a on a.id = o.user_id 
                inner join account_address aa on aa.user_id = a.id and aa.is_default = true
                left join offer ofr on ofr.id = o.offer_id                
                WHERE o.id = $1`
                
    pool.query(query, [id], (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var order = results.rows[0];
        var orderDetails = {};
        if(order){
            orderDetails = { 
                orderId: order.id,
                paymentMode: order.payment_mode, 
                paymentStatus: order.payment_status,
                transactionNum: order.transaction_num,
                paymentDate: moment(order.payment_date).format('YYYY-MM-DD HH:mm:ss'),
                orderStatus: order.status,
                totalAmount: order.total_amount,
                offerId : order.offerId || null,
                offerDesc : order.offer_desc || "",
                orderDate: moment(order.order_date).format('YYYY-MM-DD HH:mm:ss'),
                userName: order.userName,
                userMobile: order.mobile_no,
                products: [],
                address : {
                    "address": order.address,
                    "city": order.city,
                    "state": order.state,
                    "pincode": order.pincode,
                    "country": order.country
                }
            }
        }
        getOrderItems(id, orderDetails, response, userId)
        
    })

}


function getOrderItems(orderId, orderDetails, response, userId){
    var entityType = 'product';
    var query = `select p.id As "producId", p.product_code, p.product_name, p.product_desc, oi.price, oi.quantity, oi.weight, oi.name_on_cake, oi.delivery_date, oi.occasion, i.img_name, i.img_path
                from order_items oi
                inner join product p on p.id = oi.product_id
                left join images i on i.entity_id = p.id and i.entity_type = '${entityType}'
                where oi.order_id = ${orderId}`;

    console.log("==== orderDetails :", orderDetails);
    pool.query(query, (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }
       
        _.each(results.rows, function(orderItem, orderIndex) {
            var orderItemDetails = { 
                producId : orderItem.producId,
                productCode : orderItem.product_code, 
                productName : orderItem.product_name, 
                productDesc : orderItem.product_desc, 
                price : orderItem.price, 
                weight: orderItem.weight,
                nameOnCacke: orderItem.name_on_cake,
                deliveryDate: moment(orderItem.delivery_date).format('YYYY-MM-DD HH:mm:ss'),
                quantity : orderItem.quantity,
                imageName: orderItem.img_name,
                imagePath: orderItem.img_path,
                occasion: orderItem.occasion,
                isAlreadyReview: false,
                review: {}
            }
            orderDetails.products.push(orderItemDetails);
        });

        if(userId){
            reviewCheck(userId, orderDetails, response);
        } else {
            const req = sendMail(orderDetails, 'daxasystems@gmail.com');
            if(req){
                console.log("After send statement");
                response.status(200).json(orderDetails);
            }            
        }
    });
}

function reviewCheck(userId, orderDetails, response){
    var successCount = 0;
    _.each(orderDetails.products, function(product, orderIndex) {
        pool.query('select * from product_review where product_id=$1 and user_id= $2', [product.producId, userId] ,(isError, isResult) => {
            if(isError){
                return false;
            }
            
            successCount = successCount +1;
            if(isResult.rowCount === 1){
                product.isAlreadyReview = true;
               product.review = {
                    id : isResult.rows[0].id,
                    rating: isResult.rows[0].rating,
                    review: isResult.rows[0].review
                }
            }

            if(orderDetails.products.length === successCount){
                response.status(200).json(orderDetails);
            }
        });
    });
    
}


module.exports = {
    createOrder,
    updateOrderStatus,
    getOrders,
    getOrderById,
    getAllOrders,
    pendingOrders
}