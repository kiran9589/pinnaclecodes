var pool = require('../../config');

const createRole = (request, response) => {
    const {name} = request.body    
    pool.query('INSERT INTO role (role_name, created_on) VALUES ($1,now())', [name], (error, result) => {
      if (error) {
        throw error
      }
      console.log("res : ", response);
      response.status(201).send(`Role added with ID: ${result.insertId}`)
    })
}


module.exports = {
    createRole
}