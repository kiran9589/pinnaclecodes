var pool = require('../../config');
var commonUtilFile = require('../utils/commonutil');
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
var _ = require('underscore');
var moment = require('moment');

const createUser = (request, response) => {
    var userDetails
    if(typeof request.body.userdetails == 'string'){
        userDetails = JSON.parse(request.body.userdetails);
    } else {
        userDetails = request.body.userdetails
    }
    
    const {name, email, referBy, mobile} = userDetails;
    const user_id = mobile
    //const password = encrypt(userDetails.password);
    const roleId = userDetails.roleId;
    var userId = null;
    const randomString = randomStringGen();
    
    pool.query(`select * from account where user_id='${user_id}' or email='${email}'` , (userExistError, userExistResult) => {
        if(userExistError){; 
            console.log(" userExistError: ", userExistError);
            response.status(500).json({"status": "Fail", "message":"Error occured while checking existing user.Please try again."});
        } else{
            if(userExistResult && userExistResult.rows.length>0){
                var userObj = userExistResult.rows[0];
                if(userObj.is_active === false && userObj.is_deleted === true){
                    response.status(500).json({"status": "Fail", "message":"Your account is deactivated. Please contact to administrator for activate your account."});
                } else {
                    response.status(500).json({"status": "Success", "message":"Mobile number or email is already registerd in system.Please contact to administrator."});
                }                
            } else {
                // pool.query('INSERT INTO account (name,user_id, password, email, date_of_birth, address, gender, iv, envkey, user_refer_code,created_on) VALUES ($1, $2, $3, $4, $5, $6, $7,$8, $9, $10,now()) RETURNING id', [name,user_id, password.encryptedData ,email,dob, address, gender, password.iv, password.key, randomString], (error, result) => {
                    pool.query('INSERT INTO account (name, user_id, email, user_refer_code, mobile_no,created_on) VALUES ($1, $2, $3, $4, $5,now()) RETURNING id', [name, user_id, email, randomString, mobile], (error, result) => {
                    if (error) {
                        console.log(" error ", error);
                        response.status(500).json({"status": "Fail", "message":"Error occured while creating new user.Please try again."});
                    }
              
                    userId = result.rows[0].id;
                    if(userId){
                      assignRole(roleId, userId);
                      if(request.file){
                          uploadImage(userId, request.file, response)
                      }
                      
                      if(referBy && randomString){
                          registerRAndRProgram(referBy.toString(), randomString.toString())
                      }
                    }      
                    response.status(200).json({"status": "Success", "message":"User has been created successfully."});
                  })
            }
        }

        
        
    });
     

}

const createAdminUser = (request, response) => {
    const {name, email, mobile, user_id} = request.body;
    const password = encrypt(request.body.password);
    const roleId = request.body.roleId;
    
    pool.query(`select * from account where user_id='${user_id}'  or email='${email}' or mobile_no='${mobile}'` , (userExistError, userExistResult) => {
        if(userExistError){; 
            console.log(" userExistError: ", userExistError);
            response.status(500).json({"status": "Fail", "message":"Error occured while checking existing user.Please try again."});
        } else{
            if(userExistResult && userExistResult.rows.length>0){user_id
                var userObj = userExistResult.rows[0];
                if(userObj.is_active === false && userObj.is_deleted === true){
                    response.status(500).json({"status": "Fail", "message":"Your account is deactivated. Please contact to administrator for activate your account."});
                } else {
                    response.status(500).json({"status": "Success", "message":"Mobile number or email or user name is already registerd in system.Please contact to administrator."});
                }                
            } else {
                 pool.query('INSERT INTO account (name, user_id, password, email, iv, envkey, mobile_no, created_on) VALUES ($1, $2, $3, $4, $5, $6, $7,now()) RETURNING id', [name, user_id, password.encryptedData ,email, password.iv, password.key, mobile], (error, result) => {
                    if (error) {
                        console.log(" error ", error);
                        response.status(500).json({"status": "Fail", "message":"Error occured while creating new user.Please try again."});
                    }
                    if(!_.isEmpty(result.rows)){
                        var userId = result.rows[0].id;
                        if(userId){
                        assignRole(roleId, userId);
                        }      
                        response.status(200).json({"status": "Success", "message":"Admin account has been created successfully."});
                    }                    
                  })
            }
        }

        
        
    });
     

}

const addAddress = (request, response) => {
    const {userId, address, city, state, country, pincode, addressType, isDefault, contact_no} = request.body;
    
    pool.query('INSERT INTO account_address (user_id, address, city, state, country, pincode, address_type, is_default, mobile_no, created_on) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, now()) RETURNING id', [userId, address, city, state, country, pincode, addressType, isDefault, contact_no], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while adding user address.Please try again."});
        }

        if(isDefault){
            var newAddressId = result.rows[0].id;
            pool.query("UPDATE account_address SET is_default = false WHERE id <> $1 and user_id = $2",
                [newAddressId, userId], (changeError, changeResult) => {
                if (changeError) {
                    response.status(500).json({"status": "Fail", "message":"Error occured while updating user address.Please try again."});
                }      
                response.status(200).json({"status": "Success", "message":"User address added successfully."});
            });
        } else {
            response.status(200).json({"status": "Success", "message":"User address added successfully."});
        }
        
    })
};

const updateAddress = (request, response) => {
    const {id, address, city, state, country, pincode, addressType, contact_no} = request.body;

    pool.query('UPDATE account_address SET address=$2, city=$3, state=$4, country=$5, pincode=$6, address_type=$7, mobile_no=$8,updated_on=now() WHERE id = $1', [id, address, city, state, country, pincode, addressType, contact_no], (error, results) => {
        if (error) {
            //throw error
            console.log(" error : ", error);
            response.status(500).json({"status": "Fail", "message":"Error occured while updating user address.Please try again."});
        } else {

            response.status(200).json({"status": "Success", "message":"User address details updated successfully."});
            // if(_.isEmpty(results.rows) && results.rowCount ===0){
            //     response.status(500).json({"status": "Fail", "message":"Please change default address then try again. "});    
            // } else {
            //     if(isDefault){
            //         pool.query("UPDATE account_address SET is_default = false WHERE id <> $1",
            //             [id], (changeError, changeResult) => {
            //             if (changeError) {
            //                 response.status(500).json({"status": "Fail", "message":"Error occured while updating user address.Please try again."});
            //             }      
            //             response.status(200).json({"status": "Success", "message":"User address details updated successfully."});
            //         });
            //     } else {
            //         response.status(200).json({"status": "Success", "message":"User address details updated successfully."});
            //     }
            // }             
        }
    })
}

const changeDefault = (request, response) => {
    const {id, isDefault} = request.body;

    pool.query('UPDATE account_address SET is_default=$2, updated_on=now() WHERE id = $1', [id, isDefault], (error, results) => {
        if (error) {
            console.log(" error : ", error);
            response.status(500).json({"status": "Fail", "message":"Error occured while chaning default address.Please try again."});
        } else {
            if(isDefault){
                pool.query("select user_id from account_address WHERE id = $1",
                    [id], (checkExisitError, checkExisitResult) => {
                    if (checkExisitError) {
                        console.log("checkExisit error: ", checkExisitError);
                        // response.status(500).json({"status": "Fail", "message":"Error occured while updating user address.Please try again."});
                    }      
                    
                    if(!_.isEmpty(checkExisitResult.rows)){
                        var userId = checkExisitResult.rows[0].user_id
                        console.log(" usre Id: ", userId);
                        pool.query("UPDATE account_address SET is_default = false, updated_on=now() WHERE id <> $1 and user_id=$2",
                            [id, userId], (changeError, changeResult) => {
                            if (changeError) {
                                console.log("change error: ", changeError);
                                // response.status(500).json({"status": "Fail", "message":"Error occured while updating user address.Please try again."});
                            }      
                            response.status(200).json({"status": "Success", "message":"Changed user default address successfully."});
                        });
                    }
                });

                

            } else {
                response.status(200).json({"status": "Success", "message":"Changed user default address successfully."});
            }             
        }
    })
}

const deleteAddress = (request, response) => {
    const id = parseInt(request.params.id);

    pool.query('UPDATE account_address SET is_active = $1, is_default= $2 WHERE id = $3 and is_default = $4', [false, false , id, false], (error, results) => {
        if (error) {
            //throw error
            response.status(500).json({"status": "Fail", "message":"Error occured while deleting user address.Please try again."});
        } else {

            if(_.isEmpty(results.rows) && results.rowCount ===0){
                response.status(500).json({"status": "Fail", "message":"Please change default address then try again. "});    
            } else {
                response.status(200).json({"status": "Success", "message":"Address has been deleted successfully."});
            }            
        }
    })
}

function registerRAndRProgram(sender, reciever){
    pool.query(`select id, user_refer_code from account where user_refer_code in ($1,$2)`, [sender, reciever],(error, results) => {
        if (error) {
            console.log("Record Not found ", error);
            return false;
        }

        var senderId = null, recieverId = null;
        _.each(results.rows, function(user){
            if(user.user_refer_code === sender){
                senderId = user.id
            } else if(user.user_refer_code === reciever){
                recieverId = user.id
            }
        });

        if (senderId != null && recieverId != null){   
            pool.query('INSERT INTO reward_and_recognition (sender_id ,reciever_id,created_on) VALUES ($1, $2,now()) RETURNING id', [senderId , recieverId], (error, result) => {
                if (error) {
                    console.log(" Error reward_and_recognition : ", error);
                    return false;
                }

                if(results.rows){
                    console.log("R and R id: ", results.rows[0].id);
                    return true;
                }

            });
        }

    })
}

function randomStringGen() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }

    return randomstring.toString();
    
}

function uploadImage(userId, file, res){
    var entityType = 'user',
        entityId = userId,
        imgName = file.filename,
        imgPath = '/userImg/' + file.filename;

    pool.query('INSERT INTO images (entity_type,entity_id,img_name, img_path,created_on) VALUES ($1, $2, $3, $4, now())', [entityType,entityId, imgName, imgPath], (error, result) => {
        if (error) {
            throw error
        }
        return true;
    })
}

const updateUserImage = (request, response) => {
    const {id} = JSON.parse(request.body.userdetails);
    var file = request.file;

    var entityType = 'user',
        entityId = id,
        imgName = file.filename,
        imgPath = '/userImg/' + file.filename;

    pool.query("UPDATE images SET img_name = $1, img_path = $2 WHERE entity_id = $3 and entity_type = $4",
         [imgName, imgPath, entityId, entityType], (error, result) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Error occured while updating user profile image.Please try again."});
        }      
        response.status(200).json({"status": "Success", "message":"User profile image updated successfully."});
    });
}
  

function assignRole(roleId, userId){
    pool.query('INSERT INTO account_role (user_id,role_id, grant_date) VALUES ($1, $2, now())', [userId,roleId], (error, result) => {
        if (error) {
          //throw error
          return false;
        }
        return true;
    })
}

const updateUser = (request, response) => {
    // const id = parseInt(request.params.id)
    const { name,email,dob, gender, id} = request.body

    pool.query('UPDATE account SET name = $1, email = $2, date_of_birth = $3, gender = $4 WHERE id = $5',
        [name,email,dob, gender, id],
        (error, results) => {
            if (error) {
                //throw error
                response.status(500).json({"status": "Fail", "message":"Error occured while updating user details.Please try again."});
            }
            //response.status(200).send(`User modified with ID: ${id}`);
            response.status(200).json({"status": "Success", "message":"User details has been updated successfully."});
        }
    )
}
 

const reactivateUser = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('UPDATE account SET is_active = $1, is_deleted= $2 WHERE id = $3',
        [true, false, id],
        (error, results) => {
            if (error) {
                //throw error
                response.status(500).json({"status": "Fail", "message":"Error occured while reactivating user details.Please try again."});
            }
            //response.status(200).send(`User modified with ID: ${id}`);
            response.status(200).json({"status": "Success", "message":"User has been reactivated successfully."});
        }
    )
}

const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('UPDATE account SET is_active = $1, is_deleted= $2 WHERE id = $3', [false, true, id], (error, results) => {
        if (error) {
            //throw error
            response.status(500).json({"status": "Fail", "message":"Error occured while deleting user.Please try again."});
        }

        response.status(200).json({"status": "Success", "message":"User has been deactivated successfully."});
        //response.status(200).send(`User deleted with ID: ${id}`)
    })
}

const getUsers = (request, response) => {
    const { start, limit} = request.body
    var entityType = 'user';

    var query = `SELECT a.id, a.name, a.user_id, a.email,a.mobile_no, a.date_of_birth, a.gender, i.img_name, i.img_path, count(a.*) OVER() AS full_count FROM account a
                left join images i on i.entity_id = a.id and i.entity_type = $1 ORDER BY id ASC`;

    if(_.has(request.body, "start") && _.has(request.body, "limit")){
        query = `SELECT a.id, a.name, a.user_id, a.email,a.mobile_no, a.date_of_birth, a.gender, i.img_name, i.img_path, count(a.*) OVER() AS full_count FROM account a
                 left join images i on i.entity_id = a.id and i.entity_type = $1
                 ORDER BY id ASC OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
    }

    pool.query(query, [entityType],(error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var users = [];
        var totalCount = 0;
        _.each(results.rows, function(user){
            var userDetails = {
                "id": user.id,
                "name": user.name,
                "userName": user.user_id,
                "email": user.email,
                "date_of_birth": moment(user.date_of_birth).format('YYYY-MM-DD'),
                "mobile_no": user.mobile_no,
                "gender": user.gender,
                "active": user.is_active,
                "imageName": user.img_name,
                "imagePath": user.img_path
            };

            totalCount = user.full_count;
            users.push(userDetails);
        });

        response.status(200).json({totalCount: totalCount ,users: users});
    })
}
  
const getUserById = (request, response) => {
    const id = parseInt(request.params.id)
    var entityType = 'user';
    var query = `SELECT a.id, a.name, a.user_id, a.email,a.mobile_no, a.date_of_birth, a.gender, i.img_name, i.img_path FROM account a
    left join images i on i.entity_id = a.id and i.entity_type = $2 WHERE a.id = $1`;

    pool.query(query , [id, entityType], (error, results) => {
        if (error) {
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        var userDetails = {}
        _.each(results.rows, function(user){
            userDetails = {
                "id": user.id,
                "name": user.name,
                "userName": user.user_id,
                "email": user.email,
                "date_of_birth": moment(user.date_of_birth).format('YYYY-MM-DD'),
                "mobile_no": user.mobile_no,
                "gender": user.gender,
                "active": user.is_active,
                "imageName": user.img_name,
                "imagePath": user.img_path
            }
        });
        response.status(200).json(userDetails)
    })
}

function encrypt(text) {
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex'), key : key.toString('hex') };
}

function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex');
    let envkey = Buffer.from(text.key, 'hex');
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(envkey), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

const getUserAddress = (request, response) => {
    const id = parseInt(request.params.id)
    var query = `SELECT id, address, city, state, country, pincode, address_type, is_active,is_default, mobile_no "contact_no" from account_address WHERE user_id = $1 and is_active = $2`;

    pool.query(query , [id, true], (error, results) => {
        if (error) {
            console.log(" error : ", error);
            response.status(500).json({"status": "Fail", "message":"Something went to wrong.Please contact to administrator."});
        }

        if(_.isEmpty(results.rows)){
            response.status(200).json([])
        } else {
            response.status(200).json(results.rows)
        }        
    })
}

const login = (request, response) => {
    const {user_id, password} = request.body
    
    pool.query(`SELECT a.*, r.role_name, r.id "roleId" FROM account a 
                inner join account_role ar on a.id = ar.user_id
                inner join role r on ar.role_id = r.id
                WHERE a.user_id = $1`, [user_id], (error, results) => {
        if(error){
            console.log(" errors : ", error);
        }
        
        if(_.isEmpty(results.rows)){
            response.status(500).json({status: "Failure", message:"Invalid Username"})
        }

        if(results.rows[0]){
            var encryptString = decrypt({encryptedData: results.rows[0].password, iv: results.rows[0].iv, key: results.rows[0].envkey});
            console.log(" encryptString:", encryptString);
            if(password === encryptString && results.rows.length !== 0){
                var userDetails = {}

                var userObj = results.rows;
                _.each(userObj, function(user){
                    userDetails = {
                        "id": user.id,
                        "name": user.name,
                        "userName": user.user_id,
                        "email": user.email,
                        "date_of_birth": moment(user.date_of_birth).format('YYYY-MM-DD'),
                        "mobile_no": user.mobile_no,
                        "gender": user.gender,
                        "role":{
                            "id": user.roleId,
                            "name": user.role_name
                        }
                    }
                });
                response.status(200).json({status: "success", userDetails:userDetails});
            } else {
                response.status(500).json({status: "Failure", message:"Password is incorrect."})
                return;
            }
        }
        
    });
}

const getUserDetails = (request, response) => {
    const {mobile_no} = request.body
    
    pool.query('SELECT * FROM account WHERE mobile_no = $1 and user_id = $1', [mobile_no], (error, results) => {
        if(error){
            console.log(" errors : ", error);
        }

        if(!_.isEmpty(results.rows)){
            var userDetails = {}
            var userObj = results.rows;
            _.each(userObj, function(user){
                userDetails = {
                    "id": user.id,
                    "name": user.name,
                    "userName": user.user_id,
                    "email": user.email,
                    "date_of_birth": moment(user.date_of_birth).format('YYYY-MM-DD'),
                    "mobile_no": user.mobile_no,
                    "gender": user.gender,
                    "referalCode": user.user_refer_code,
                    "rewardPoints": 0
                }
            });
            getRefralPoints(userDetails, response)
        }
        
    });
}

function getRefralPoints(userDetails, response){
    pool.query('SELECT sum(points) as "points" FROM credit_randr_point WHERE user_id = $1', [userDetails.id], (error, credit_randr_point_results) => {
        if(credit_randr_point_results.rows){
            randRPoints = credit_randr_point_results.rows[0].points || 0;
            userDetails["rewardPoints"] = parseInt(randRPoints);     
        } 
        response.status(200).json(userDetails);
    });
}

const getRewardPoints = (request, response) => {
    const userId =  parseInt(request.params.id)
    pool.query('SELECT sum(points) as "points" FROM credit_randr_point WHERE user_id = $1', [userId], (error, credit_randr_point_results) => {
        var userDetails = {"rewardPoints": 0};
        if(credit_randr_point_results.rows){
            randRPoints = credit_randr_point_results.rows[0].points || 0;
            userDetails["rewardPoints"] = parseInt(randRPoints);     
        } 
        response.status(200).json(userDetails);
    });
};

module.exports = {
    createUser,
    updateUser,
    updateUserImage,
    deleteUser,
    getUsers,
    getUserById,
    login,
    addAddress,
    deleteAddress,
    getUserAddress,
    updateAddress,
    getUserDetails,
    getRewardPoints,
    reactivateUser,
    changeDefault,
    createAdminUser
}