const pool = require('../../config');
var mailer = require("nodemailer");
var moment = require('moment');
var _ = require('underscore');
// const { google } = require("googleapis");
// const OAuth2 = google.auth.OAuth2;

function randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    console.log("randomstring: ", randomstring);
    return randomstring;
}

/**
 * 
 * @param {*} method name such as post, get etc
 * @param {*} name of request, verify otp, sendotp or message 
 */
const formRequest = (msg, to) => {
    console.log("form request method entry");
    // TODO move all urls and static values to properties or config file
    console.log(msg, to);
    const options = {
        method: 'POST',
        url: 'https://api.msg91.com/api/v2/sendsms',
        qs: { country: '91' },
        headers: {
            authkey: process.env.AUTHKEY,
            'Content-Type': 'application/json'
        },
        body:
        {
            sender: 'COUCHE',
            route: '4',
            country: '91',
            sms: [{ message: msg, to: to }]
        },
        json: true
    };
    return options;
}

const insertOTP = (mobile_no, otp) => {
    console.log(mobile_no, otp);
    return new Promise((reject, resolve) => {
        pool.query('INSERT INTO otp_verification (mobile_no, otp, otp_timestamp) VALUES ($1,$2,now())', [mobile_no, otp], (error, result) => {
            
            if (error) {
                console.log("Error :", error);
                
                return reject(error);
            }
            // console.log("res : ", JSON.stringify(result), JSON.stringify(error));
            return resolve(result);
        });
    });
}

const sendMail = async (orderDetails, to) =>{
//     const clientId = "1020602125896-ff7s4f894htkc4rht7k5ua63va3rqd0m.apps.googleusercontent.com";
//     const clientSecret = "JfpEya5LZ7WEH-qO5yRLemTa";
//     const refershToken = "1/DofVj_AiEgDvuCg4JPOQXpUiwwBGagfxeqqGN0KsmZs" 

//     const oauth2Client = new OAuth2(
//         clientId, // ClientID
//         clientSecret, // Client Secret
//         "https://developers.google.com/oauthplayground" // Redirect URL
//    );

//    // generate a url that asks permissions for Blogger and Google Calendar scopes
// const scopes = [
//     'https://www.googleapis.com/auth/blogger',
//     'https://www.googleapis.com/auth/calendar'
//   ];
  
//   const url = oauth2Client.generateAuthUrl({
//     // 'online' (default) or 'offline' (gets refresh_token)
//     access_type: 'offline',
  
//     // If you only need one scope you can pass it as a string
//     scope: scopes
//   });

//   console.log(" url: ", url);

//    oauth2Client.setCredentials({
//         refresh_token: refershToken
//    });
   
//    const accessToken = oauth2Client.getAccessToken()
//    console.log(" accessToken: ", accessToken);

   var smtpTransport =  mailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
        user: 'kirandbaste9589@gmail.com',
        pass: '9028143962'
    }    
    // service: "gmail",
        // host: 'smtp.gmail.com',
        // port: 465,
        // secure: true,
        // auth: {
        //      type: "OAuth2",
        //      user: "daxasystems@gmail.com",
        //      clientId: clientId,
        //      clientSecret: clientSecret,
        //      refreshToken: refershToken,
        //      accessToken: accessToken
        // }
   });

//    smtpTransport.set('oauth2_provision_cb', (user, renew, callback)=>{
//         let accessToken = userTokens[user];
//         if(!accessToken){
//             return callback(new Error('Unknown user'));
//         }else{
//             return callback(null, accessToken);
//         }
//     });
   console.log(" Reciever mail id ", to);    // mailer.createTransport({
    //     host: "smtp.gmail.com",
    //     port: 587,
    //     secure: false,
    //     auth: {
    //         user: 'kirandbaste9589@gmail.com',
    //         pass: '9028143962'
    //     }
    // });
   
    var productString = "";
    _.each(orderDetails.products, function(product, index) {
        productString = productString + 
                        (index +1 ) + ") Cake Name: "+ product.productName +"<br />"
                        +"Weight: "+ (product.weight || "0") +"<br />"
                        +"Price: "+ product.price +"<br />"
                        +"Qantity: "+ product.quantity +"<br />"
                        +"Delivery Date: "+ moment(product.deliveryDate).format('DD MMM YYYY') +"<br/>"
                        +"Name on cake: "+ (product.nameOnCacke || " ") +"<br/>"
                        +"Occasion: "+ (product.occasion || " " )+ "<br/><br/><br/>"
    });


    var mail = {
        from: "kirandbaste9589@gmail.com",
        to: to,
        subject: "New order request - "+ orderDetails.orderId,
        generateTextFromHTML: true,
        html: "Hi Couches, <br/> <br /> <b>" + orderDetails.userName + "</b> has ordered below cakes. <br/> <br/> Details are as below: <br/> <br/>"+ productString + "<b> Total amount paid : "+ orderDetails.totalAmount +" Rs</br> <br/>"
        +"<br/><b>Address : </b>" + orderDetails.address.address +" , <br/> " + orderDetails.address.city +", <br/> " + orderDetails.address.state + " , " + orderDetails.address.country + " - "+ orderDetails.address.pincode
        +"<br/><br/> Thanks." 
    }

    
    
    var tets = await smtpTransport.sendMail(mail, function(error, response){
        if(error){
            console.log("throwing error : ", error);
        }else{
            console.log("Message sent: ", mail.html);
            return true;
        }
    
        smtpTransport.close();
    });

}

module.exports = {
    formRequest,
    randomString,
    insertOTP,
    sendMail
};