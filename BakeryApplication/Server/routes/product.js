var express = require('express');
var router = express.Router();
var multer = require('multer');
var products = require('../src/controllers/product')


// For accessing product images 
var Storage = multer.diskStorage({
  destination: function(req, file, callback) {
      callback(null, './public/images/product');
  },
  filename: function(req, file, callback) {
      callback(null, Date.now() + "_" + file.originalname);
  }
});

var upload = multer({
  storage: Storage
}).single('fileInput')

router.post('/create', upload, products.createProduct)
router.post('/update', products.updateProduct)
router.post('/updateImage', upload, products.updateProductImage)
router.post('/list', products.getProducts)
router.get('/:id', products.getProductById)
router.delete('/:id', products.deleteProduct)
//router.post('/:offerId', products.getProductsByOffer) // Get products by offer id
router.post('/review', products.createReview)
router.post('/updatereview', products.updateReview)
router.delete('/review/:id', products.deleteReview)

module.exports = router;