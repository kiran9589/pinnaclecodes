/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import NavigationStack from './src/navigation/NavigationStack';
import ReadContact from './src/dashboard/ReadContact';
import AddAddress from './src/address/AddAddress';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => NavigationStack);
