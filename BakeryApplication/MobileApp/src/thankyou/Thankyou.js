/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View,StatusBar,PermissionsAndroid,ListView,Image} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text, Title, Left, Button, Icon, Right} from 'native-base';

import styles from './thankyou.style';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class Thankyou extends Component<Props> {


    render() {
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />
                    </Left>

                    <Body>
                    {/*<Title style={styles.main_salon_name}>Offers</Title>*/}
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                <Text style={styles.welcome}>Thank You!</Text>
                    <Text style={styles.instructions}>Order Place</Text>
                </Content>
            </Container>


                    );
    }
}

