import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert,StatusBar,Text,
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left } from 'native-base';
import styles from './mobile.style';
import Toast from "react-native-simple-toast";

var Realm = require('realm');
let realm;

export default class MobileNo extends Component<> {

    constructor(props) {

        super(props);
        
        try {
            realm = new Realm({ path: 'CakeDatabase.realm'});
            var User_Details = realm.objects('User_Details');
            var mobile_number=User_Details[0].mobile_number;
            
        }
        catch (e) {
        }this.state = {
            reg_first_str:"R"+"egister",
            reg_cancle_str:"C"+"ancel",
            ActivityIndicator:false,
            mobileNo: mobile_number,
            userID: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,

            empty_phone_no:false,

        };
    }
    onEnterName = (TextInputValue) =>{
        if(TextInputValue.trim() != 0){
            this.setState({user_name : TextInputValue, ErrorStatus_name : false,}) ;
        }else{
            this.setState({user_name : TextInputValue, ErrorStatus_name : true}) ;
        }
    }
    onEnterMobileno = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({mobileNo : TextInputValues, ErrorStatus_mobile : false,empty_phone_no:false}) ;
        }else{
            this.setState({mobileNo : TextInputValues, ErrorStatus_mobile : true}) ;
        }
    }

    buttonClickListener = () =>{
        const { mobileNo,user_name }  = this.state ;
        if (user_name == "" ){
            alert("Please Enter Booking Person Name");
        }
        if (mobileNo == ""){
            alert("Please Enter Mobile Number");
        }
    }

/*=============================
* componentDidMount Method
* =============================*/
componentDidMount(){
    if(this.props.navigation.state.params.rotateID === 9)
    {
        this.setState({
            blankmodal: false,
        });
    }
   else if(this.props.navigation.state.params.rotateID === 4 ) {
        this.setState({
            blankmodal: false,
        })
    }

    else {
        this.setState({
            blankmodal: true,
        })
    }
}

/*==============================
* Login Modal Cancle
* ==============================*/
modalCancle = () => {
    var val=this.props.navigation.state.params.rotateID;
    if(val === 1)
    {
        this.props.navigation.navigate('DescriptionScreen');
        this.setState({blankmodal: false,});
    }
    else if ( val === 2)
    {
        this.props.navigation.navigate('MyDrawerNavigator');
        this.setState({blankmodal: false,})
    }
    else if (val === 3){
        this.props.navigation.navigate('Mycart');
        this.setState({blankmodal: false,})
    }
    else if ( val === 5)
    {
        this.props.navigation.navigate('MyDrawerNavigator');
        this.setState({blankmodal: false,})
    }
}
/*=======================================
*Method Send Mobile Number to Server
* =======================================*/

Function_TO_RequestOTP = (mobile_number) =>
    {
       // this.props.navigation.navigate('Mycart');


        if(this.state.ErrorStatus_mobile!=true)
        {

            this.setState({ActivityIndicator_Loading: true}, () => {
                fetch('http://3.13.212.113:3000/message/sendotp',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                mobile_no: this.state.mobileNo,

                            })

                    }).then((response) => response.json()).then((response) => {


                    if (response.status != 'Fail') {
                        Toast.show(response.message, Toast.SHORT);
                        this.setState({ActivityIndicator_Loading: false});
                        this.props.navigation.navigate('Verification_Screen', {mobile_number});
                    } else {
                        this.props.navigation.navigate('PersonalDetailOption',{mobile_no:this.state.mobileNo});

                        Toast.show(response.message, Toast.SHORT);
                        this.setState({ActivityIndicator_Loading: false});

                    }


                }).catch((error) => {
                    console.error(error);

                    this.setState({ActivityIndicator_Loading: false});
                });
            });
        }

        else
        {
            this.setState({
                empty_phone_no:true,
            })
        }
    }

    /*Method Send Mobile Number to Server End */

    render() {

        return(
            <Container style={{backgroundColor:'#FCECF4',}}>
                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row',width:'40%'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                    <Body>
                         <Title style={styles.main_salon_name}>Login</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={styles.image_container}>
                        <Image
                            source={require('../../images/logo_small.png')}
                            style={styles.logo}
                        />
                    </View>
                    <Text style={styles.otp_display_text}>Enter your registered mobile number to sign in.</Text>
                    <Text style={styles.text_name}>Enter Mobile Number</Text>

                    <View style={{flexDirection:'row',marginTop:'3%'}}>
                        <View style={styles.code_Section_SectionStyle}>

                            <TextInput
                                ref='input_mobileno'
                                maxLength={10}
                                editable={false}
                                style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',alignItems:'center',textAlign:'center'}}
                                placeholder="+91"
                                keyboardType = 'numeric'
                                autoFocus={true}
                                underlineColorAndroid="transparent"/>

                        </View>
                        <View style={this.state.empty_phone_no?styles.error_SectionStyle:styles.SectionStyle}>

                            <TextInput
                                ref='input_mobileno'
                                maxLength={10}
                                editable={true}
                                style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                placeholder="Enter Mobile Number"
                                keyboardType = 'numeric'
                                autoFocus={true}
                                underlineColorAndroid="transparent"
                                onChangeText = {(TextInputText) =>  this.onEnterMobileno(TextInputText)} />

                        </View>
                    </View>

                    {this.state.empty_phone_no?
                        <Text style={styles.error_message}>Phone Number is Mandetory</Text>:null}

                </Content>



                    <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                             onPress={()=>this.Function_TO_RequestOTP(this.state.mobileNo)}>


                        {
                            this.state.ActivityIndicator_Loading ? <ActivityIndicator color='#fff' size='large'
                                                                                      style={styles.ActivityIndicatorStyle}/> :

                                <Text style={{
                                    color: '#fff',
                                    fontSize: 14,
                                    fontWeight: 'bold',
                                    fontFamily: 'Calibri Bold',
                                }}
                                      onPress={() => this.Function_TO_RequestOTP(this.state.mobileNo)}>Continue </Text>
                        }
                    </Button>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.ActivityIndicator}>
                    <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center',
                        backgroundColor:'#D977AB'}}>
                        <ActivityIndicator size={'large'} color={'#fff'}/>
                        <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                    </View>

                </Modal>

                {/*Register Pop Up Modal*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.blankmodal}>
                    <View style={{ backgroundColor:'rgba(0, 0, 0, 0.8)',paddingTop:'10%',flex:1,
                        justifyContent:'center',alignItems:'center' }}>

                        <View style={{backgroundColor:'#fff',borderRadius:0,margin:20}}>
                            <Text style={{textAlign:'center',fontSize:15,paddingTop:20,paddingBottom:20,paddingLeft:'12%',paddingRight:'12%',
                                fontWeight:'bold',fontFamily:'Calibri Bold',}}>
                                You are not registered. Please Register to continue
                            </Text>
                            <View style={{flexDirection:'row'}}>

                                <View style={{flexDirection:'row',}}>

                                    <Button  style={{backgroundColor:'#E1A7C5',width:'50%',padding:10,bottom:0,
                                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=> {this.setState({blankmodal:false})}}>
                                        <Text style={{color:'#000',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=> {this.setState({blankmodal:false})}}>
                                            Register </Text>
                                    </Button>

                                    <Button  style={{backgroundColor:'#333333',width:'50%',padding:10,bottom:0, justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>this.modalCancle()}>

                                        <Text style={{color:'#fff',fontSize:12,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>this.modalCancle()}>Cancel</Text>

                                    </Button>
                                </View>



                            </View>
                        </View>
                    </View>

                </Modal>



            </Container>
        );
    }
}
