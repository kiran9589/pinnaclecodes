import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert,StatusBar,
} from 'react-native';
import { Container, Header, Item, Input, Icon,Text, Button,Right,Content,Body,Title,Left } from 'native-base';
import styles from './mobile.style';
import Toast from "react-native-simple-toast";

var Realm = require('realm');
let realm;

export default class LoginMyAcccount extends Component<> {

    constructor(props) {

        super(props);


        try{
            realm = new Realm({ path: 'CakeDatabase.realm'});
            var User_Details = realm.objects('User_Details');
            var mobile_number=User_Details[0].mobile_number;
            this.props.navigation.navigate('DemoMyAccount');
            console.warn('tr block called');
        }
        catch (e) {
            console.warn("Mobile NUmber Not Found under catch block.");
        }

        this.state = {
            ActivityIndicator:false,
            mobileNo: mobile_number,
            userID: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,
            empty_phone_no:false,

        };
    }
    onEnterName = (TextInputValue) =>{
        if(TextInputValue.trim() != 0){
            this.setState({user_name : TextInputValue, ErrorStatus_name : false,}) ;
        }else{
            this.setState({user_name : TextInputValue, ErrorStatus_name : true}) ;
        }
    }
    onEnterMobileno = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({mobileNo : TextInputValues, ErrorStatus_mobile : false,empty_phone_no:false}) ;
        }else{
            this.setState({mobileNo : TextInputValues, ErrorStatus_mobile : true}) ;
        }
    }

    buttonClickListener = () =>{
        const { mobileNo,user_name }  = this.state ;
        if (user_name == "" ){
            alert("Please Enter Booking Person Name");
        }
        if (mobileNo == ""){
            alert("Please Enter Mobile Number");
        }
    }



    /*=======================================
    *Method Send Mobile Number to Server
    * =======================================*/

    Function_TO_RequestOTP = (mobile_number) =>
    {
        // this.props.navigation.navigate('Mycart');

        if(this.state.mobileNo!='')
        {

            console.log("Mobile Number Screen Called");
            console.log("Mobile Number 70 Send OTP Called ");
            this.setState({ActivityIndicator_Loading: true}, () => {
                console.log("Personal Details 214 Send OTP Called ");
                fetch('http://3.13.212.113:3000/message/sendotp',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                mobile_no: this.state.mobileNo,

                            })

                    }).then((response) => response.json()).then((response) => {


                    if (response.message != 'Error while inserting otp into database') {
                        Toast.show(response.message, Toast.SHORT);
                        this.setState({ActivityIndicator_Loading: false});
                        this.props.navigation.navigate('Verification_Screen', {mobile_number});
                        console.warn("Mobile number is already register");
                    } else {
                        Toast.show(response.message, Toast.SHORT);
                        this.setState({ActivityIndicator_Loading: false});
                        this.props.navigation.navigate('PersonalDetailOption', {mobile_number});
                        console.warn("Mobile number is not register");

                    }


                }).catch((error) => {
                    console.error(error);

                    this.setState({ActivityIndicator_Loading: false});
                });
            });
        }

        else
        {
            this.setState({
                empty_phone_no:true,
            })
        }
    }

/*===================================
* Function To Open SIgnup Screen
* ===================================*/

OpenSignUpScreen=()=>{
    this.props.navigation.navigate('PersonalDetailOption');
}

    /*Method Send Mobile Number to Server End */

    render() {

        return(
            <Container style={{backgroundColor:'#FCECF4',}}>
                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=>this.props.navigation.navigate('DescriptionScreen')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />
                    </Left>

                    <Body>
                        <Title style={styles.main_salon_name}>Login</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={styles.image_container}>
                        <Image
                            source={require('../../images/logo_app.png')}
                            style={styles.logo}
                        />
                    </View>

                    <Text style={styles.text_name}>Enter Mobile Number</Text>
                    <View style={this.state.empty_phone_no?styles.error_SectionStyle:styles.SectionStyle}>

                        <TextInput
                            ref='input_mobileno'
                            maxLength={10}
                            editable={true}
                            style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                            placeholder="Enter Register Mobile Number"
                            keyboardType = 'numeric'
                            autoFocus={true}
                            underlineColorAndroid="transparent"
                            onChangeText = {(TextInputText) =>  this.onEnterMobileno(TextInputText)} />

                    </View>
                    {this.state.empty_phone_no?
                        <Text style={styles.error_message}>Phone Number is Mandetory</Text>:null}

                </Content>

                <View style={{flexDirection:'row'}}>
                    <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                             onPress={()=>this.Function_TO_RequestOTP(this.state.mobileNo)}>


                        {
                            this.state.ActivityIndicator_Loading ? <ActivityIndicator color='#fff' size='large'
                                                                                      style={styles.ActivityIndicatorStyle}/> :

                                <Text style={{
                                    color: '#fff',
                                    fontSize: 14,
                                    fontWeight: 'bold',
                                    fontFamily: 'Calibri Bold',
                                }}
                                      onPress={() => this.Function_TO_RequestOTP(this.state.mobileNo)}>Continue </Text>
                        }
                    </Button>


                </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.ActivityIndicator}>
                    <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center',
                        backgroundColor:'#333333'}}>
                        <ActivityIndicator size={'large'} color={'#fff'}/>
                        <Text style={{color:'#fff'}}>Please Wait .....</Text>
                    </View>

                </Modal>

            </Container>
        );
    }
}
