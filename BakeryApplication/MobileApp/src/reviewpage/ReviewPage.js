/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
    StatusBar,
    PermissionsAndroid,
    FlatList,
    Image,
    Alert,
    ActivityIndicator, TouchableOpacity,
} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text, Title, Left, Button, Icon, Right} from 'native-base';
import styles from "../reviewpage/reviewpage.style";
import  Rating from 'react-native-easy-rating';



type Props = {};
export default class ReviewPage extends Component<Props> {

    constructor() {
        super();

        this.state = {

            review_data:[],
            isLoading: true,
            refreshing:false,



        }
    }


    handleBackPress = (item_overallRating) => {
        this.props.navigation.navigate('DescriptionScreen',{item_overallRating});
    }

    componentDidMount()
    {
        this.loadreview();
    }


    /*========================
   * Load Product Data
   * ========================*/

 loadreview=()=>{
        console.warn("Review Page::"+this.props.navigation.state.params.product_id);
        fetch('http://3.13.212.113:3000/product/'+this.props.navigation.state.params.product_id)

            .then((response) => response.json())
            .then((responseJson) => {
                console.warn("Data Fetch");
                console.log("Category Data"+responseJson);
              //  let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                var listData=responseJson;
                this.setState({
                    isLoading:false,
                    //dataSource: ds.cloneWithRows(responseJson.reviews),
                    dataSource: responseJson.reviews,



                }, function(){

                });
                console.warn("datasouce"+JSON.stringify(this.state.dataSource));
            })
            .catch((error) =>{
                console.error(error);
            });
 }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent onPress={()=>this.handleBackPress(this.props.navigation.state.params.item_overallRating)} style={{top:10}}>
                            <Icon name='arrow-back' />
                        </Button>

                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>

                    </Left>

                    <Body>
                         <Title style={styles.main_salon_name}>Review Page</Title>
                    </Body>


                </Header>

                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:5}}>


                    {this.state.dataSource.map((item)=>{
                        return  <Card style={styles.main_card_style}>
                            <CardItem>
                                <Body style={styles.card_body_style}>
                                <Text style={styles.main_offer_style}>{item.userName}</Text>
                                <Rating
                                    rating={item.rating}
                                    max={5}
                                    iconWidth={18}
                                    iconHeight={18}
                                    iconSelected={require('../../images/star_rate.png')}
                                    editable={false}
                                    onRate={(rating) => this.setState({rating: rating})}/>
                                <Text style={styles.submain_offer_style}>{item.review}</Text>

                                </Body>

                            </CardItem>

                        </Card>
                    })}




{/*

                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({rowData}) =>

                            <Card style={styles.main_card_style}>
                                <CardItem>
                                    <Body style={styles.card_body_style}>
                                        <Text style={styles.main_offer_style}>Username - {rowData.userName}</Text>
                                        <Text style={styles.submain_offer_style}>Review - {rowData.review}</Text>
                                        <View style={{flexDirection:'row',marginTop:7}}>
                                            <Text style={[styles.text_name,{fontSize:15,fontWeight:'bold'}]}>Rating </Text>
                                            <Rating
                                                rating={rowData.rating}
                                                max={5}
                                                iconWidth={20}
                                                iconHeight={20}
                                                iconSelected={require('../../images/star_rate.png')}
                                                editable={false}
                                                onRate={(rating) => this.setState({rating: rating})}/>
                                        </View>

                                    </Body>

                                </CardItem>

                            </Card>
                        }
                    />
*/}


{/*
                    <Card style={styles.main_card_style}>
                        <CardItem>
                            <Body style={styles.card_body_style}>
                            <Text style={styles.main_offer_style}>Username - Arjun Dalal</Text>
                            <Text style={styles.submain_offer_style}>Review - Product is very good</Text>
                            <View style={{flexDirection:'row',marginTop:7}}>
                                <Text style={[styles.text_name,{fontSize:15,fontWeight:'bold'}]}>Rating </Text>
                                <Rating
                                    rating={2}
                                    max={5}
                                    iconWidth={20}
                                    iconHeight={20}
                                    iconSelected={require('../../images/star_rate.png')}
                                    editable={false}
                                    onRate={(rating) => this.setState({rating: rating})}/>
                            </View>

                            </Body>

                        </CardItem>

                    </Card>


                    <Card style={styles.main_card_style}>
                        <CardItem>
                            <Body style={styles.card_body_style}>
                            <Text style={styles.main_offer_style}>Username - Arjun Dalal</Text>
                            <Text style={styles.submain_offer_style}>Review - Product is very good</Text>
                            <View style={{flexDirection:'row',marginTop:7}}>
                                <Text style={[styles.text_name,{fontSize:15,fontWeight:'bold'}]}>Rating </Text>
                                <Rating
                                    rating={2}
                                    max={5}
                                    iconWidth={20}
                                    iconHeight={20}
                                    iconSelected={require('../../images/star_rate.png')}
                                    editable={false}
                                    onRate={(rating) => this.setState({rating: rating})}/>
                            </View>
                            </Body>
                        </CardItem>
                    </Card>*/}
                    {/*
                    <Card style={{borderRadius:10}}>
                        <CardItem>
                            <Body style={{textAlign:'center',justifyContent:'center',
                                alignItems:'center'}}>
                            <Text style={styles.offer_description}>
                                Get 30 % Discount on Your First Order use Code :-
                            </Text>
                            <Text style={{fontSize:18,fontFamily:'Lato-Bold',padding:5,color:'green'}}>
                                GET90
                            </Text>
                            </Body>
                        </CardItem>
                    </Card>*/}
                </Content>
            </Container>
        );
    }
}


