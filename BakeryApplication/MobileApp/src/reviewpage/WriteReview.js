
import React, {Component} from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, StatusBar,
    Modal, TouchableWithoutFeedback, Alert,ActivityIndicator,
} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button, Text,Right,Title, Subtitle,Body,Tab, Tabs,Left } from 'native-base';
import styles from '../myaccount/writereview.style';
import  Rating from 'react-native-easy-rating';
import Textarea from 'react-native-textarea';
import Toast from 'react-native-simple-toast';




type Props = {};
export default class WriteReview extends Component<Props> {


    constructor(props) {

        super(props);

        this.state = {
            isLoading: true,
            paymentmodal: false,
            promomodal: false,
            selected_payment_btn: null,
            selected_promo_btn: null,
            address: '',
            ErrorStatus_address: true,
            ActivityIndicator:false,
            empty_des:false,
            rating:0,



        };
    }
onEnteraddress = (TextInputValue) =>{
        if(TextInputValue.trim() != 0){
            this.setState({address : TextInputValue, ErrorStatus_address : false,empty_des:false}) ;
        }else{
            this.setState({address : TextInputValue, ErrorStatus_address : true}) ;
        }
}

/*=======================
* Check Empty Des
* =======================*/
empty_dec = () => {
    if(this.state.ErrorStatus_address === true)
    {
        this.setState({empty_des:true})
    }
    else if(this.state.rating === 0)
    {
        Toast.show("Rating should not be zero", Toast.SHORT);

    }
    else {
        this.uploadReview();
    }
}


/*============================
* Function to Upload Review
* ============================*/


uploadReview=()=>{
    alert(this.props.navigation.state.params.user_id+this.props.navigation.state.params.product_id);
    this.setState({ActivityIndicator:true})
    return fetch('http://3.13.212.113:3000/product/review',
        {
            method: 'POST',
            headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            body: JSON.stringify(
                {
                    userId: this.props.navigation.state.params.user_id,
                    productId: this.props.navigation.state.params.product_id,
                    review: this.state.address,
                    rating:this.state.rating,
                })

        }).then((response) => response.json()).then((response) =>
    {

        if(response.status!=false)
        {
            Toast.show(response.message, Toast.SHORT);
            this.setState({ActivityIndicator:false});
            this.props.navigation.navigate('MyOrderDetails');
        }
        else
        {
            this.setState({ActivityIndicator:false});
            Toast.show("Review Not Updated Successfully", Toast.SHORT);
        }
        // console.log("The Data is = " + JSON.stringify(this.state.data).toString());
        //console.log("The weightPrice is = " + JSON.stringify(this.state.weightPrice).toString());

    }).catch((error) =>
    {
        console.error(error);

    });
}

onpressback=()=>{
        this.props.navigation.navigate('MyOrder');
}
    render() {
        return (
            <Container style={{backgroundColor: '#FCECF4'}}>

                <Header
                    style={{backgroundColor: '#333333', paddingTop: 5, paddingLeft: 10, paddingRight: 10, height: 65}}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent onPress={()=>this.onpressback()} style={{top:10}}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>


                    <Body style={{padding: 5,}}>
                     <Title style={{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'35%',color:'#fff',fontWeight:'bold',}}>
                        Post Review</Title>

                    </Body>
                    {/*                    <Right>
                        <TouchableOpacity>
                            <Image source={require('../../images/white_logout.png')}
                                   style={{width:22,height:22,right:20,marginTop:2}}/>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Image source={require('../../images/black_cart.png')}
                                   style={{width:24,height:24,right:5,marginTop:2}}/>
                        </TouchableOpacity>
                    </Right>*/}

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>
                    <View style={{flexDirection: 'row', padding: 10}}>
                        <Image source={{uri: this.props.navigation.state.params.pro_img}}
                               style={{width: 75, height: 75, borderRadius: 75 / 2, marginTop: '1%'}}/>
                        <View style={{flexDirection: 'column', marginTop: '10%'}}>
                            <Text style={[styles.client_user_name, {paddingBottom: 5}]} numberOfLines={1}>{this.props.navigation.state.params.pro_name}</Text>
                            <Rating
                                rating={this.state.rating}
                                max={5}
                                iconWidth={20}
                                iconHeight={20}
                                iconSelected={require('../../images/star_rate.png')}
                                editable={true}
                                onRate={(rating) => this.setState({rating: rating})}/>


                        </View>
                    </View>
                    <View style={styles.divide_section}/>
                    <Textarea
                        containerStyle={styles.textareaContainer}
                        style={styles.textarea}
                        onChangeText={(TextInputText) =>  this.onEnteraddress(TextInputText)}
                        defaultValue={this.state.text}
                        maxLength={120}
                        placeholder={'Enter Your Review'}
                        placeholderTextColor={'#c7c7c7'}
                        underlineColorAndroid={'transparent'}
                    />
                    {this.state.empty_des ?
                        <Text style={styles.error_message}>The Review is Mandetory</Text>:null}

                </Content>
                <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,
                    justifyContent:'center',alignItems:'center',textAlign:'center', }} onPress={()=> this.empty_dec()}>
                    <Text style={{color:'#fff',fontSize:17,fontFamily:'Calibri Regular',}}
                          onPress={()=>this.empty_dec()}>Submit Review</Text>

                </Button>
            </Container>
        );
    }
}
