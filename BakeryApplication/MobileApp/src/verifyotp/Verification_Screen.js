import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert,StatusBar,PermissionsAndroid ,
} from 'react-native';
import {Container, Header, Item, Input, Icon, Text, Button, Right, Content, Body, Title,Left} from 'native-base';
import CodeInput from 'react-native-confirmation-code-input';
import SmsListener from 'react-native-android-sms-listener';
import styles from './otp.style';
import Toast from "react-native-simple-toast";
var Realm = require('realm');
let realm ;
import Snackbar from 'react-native-snackbar';

export default class Verification_Screen extends Component<> {

    constructor(props) {

        super(props);
        try {
            realm = new Realm({ path: 'CakeDatabase.realm'});
            var Stack_Details = realm.objects('Cart_Details');
            var stack_id=Stack_Details[0].stack_id;
            }
            catch (e) {
                var stack_id=0;
            }


        this.state = {
            otpno:'',
            user_id:'',
            ActivityIndicator_Loading: false,
            mobileNo: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            stack_id:stack_id,
            ontransfer: false,
            id:null,
            referalCode:'',

        };

    }
    componentDidMount() {
        this.requestReadSmsPermission();

    }

    /*Function_to_open_HomeScreen = (user_id) => {
        if(global.logintype!='social_login')
        {
            this.props.navigation.navigate('Accept_Personal_Detail',{user_id});
        }
        else {
            this.props.navigation.navigate('ThankYou_Screen');
        }

    }*/
    Function_to_open_HomeScreen = (mobile_number) => {
       //alert("this.state.stack_id"+this.state.stack_id)
        if(this.state.stack_id!=1)
        {
            this.props.navigation.navigate('MyDrawerNavigator');
        }
        else {
            this.props.navigation.navigate('Mycart',{mobile_number});
        }



    }
    _onFulfill(code) {
        // TODO: call API to check code here
        // If code does not match, clear input with: this.refs.codeInputRef1.clear()
        if(code.length<0)
        {
            Toast.show('OTP Should Not be Blank', Toast.SHORT);
        }
        else {

            this.setState({ActivityIndicator_Loading: true}, () => {
                fetch('http://3.13.212.113:3000/message/verifyotp',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                mobile_no: this.props.navigation.state.params.mobile_number,
                                otp: code,

                            })

                    }).then((response) => response.json()).then((response) => {


                    if (response.message != 'invalid otp') {

                        // user craeted into local Database
                        realm = new Realm({ path: 'CakeDatabase.realm' });

                        realm.write(() => {

                            realm.create('User_Details', {
                                mobile_number:this.props.navigation.state.params.mobile_number,
                                login_value:1

                            });

                        });

                            // End user craeted into local Database
                        //Toast.show('Congrats , OTP Verified', Toast.SHORT);
                        Snackbar.show({
                            title: 'User Created Successfully',
                            duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                            color:'#fff',
                            action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                            },
                        });
                        //this.getuserDetails();
                        this.Function_to_open_HomeScreen(this.props.navigation.state.params.mobile_number);

                    } else {
                        Toast.show('Please Enter Correct OTP', Toast.SHORT);

                    }

                    this.setState({ActivityIndicator_Loading: false});

                }).catch((error) => {
                    console.error(error);

                    this.setState({ActivityIndicator_Loading: false});
                });
            });
        }
    }
    /*Function TO CHeck OTP Code*/
    Insert_Data_Into_Verifyotp = (otpno) =>
    {
        if(otpno != '' || otpno != undefined ||otpno != null)
        {
            Toast.show('OTP Should Not be Blank', Toast.SHORT);
        }
        else {
            this.setState({ActivityIndicator_Loading: true}, () => {
                fetch('http://3.13.212.113:3000/message/verifyotp',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                mobile_no: this.props.navigation.state.params.mobile_number,
                                otp: this.state.otpno,

                            })

                    }).then((response) => response.json()).then((response) => {


                    if (response.message != 'invalid otp') {

                        // user craeted into local Database
                        realm = new Realm({ path: 'CakeDatabase.realm' });

                        realm.write(() => {

                            realm.create('User_Details', {
                                mobile_number:this.props.navigation.state.params.mobile_number,
                                login_value:1

                            });

                        });
                        console.warn("The Value in User Details Table are as follows"+JSON.stringify(realm.objects('User_Details')));
                        // End user craeted into local Database

                        Toast.show('Congrats , OTP Verified', Toast.SHORT);
                       // this.getuserDetails();
                    } else {
                        Toast.show('Please Enter Correct OTP', Toast.SHORT);
                    }

                    this.setState({ActivityIndicator_Loading: false});

                }).catch((error) => {
                    console.error(error);

                    this.setState({ActivityIndicator_Loading: false});
                });
            });
        }
    }


    /*Function TO CHeck OTP Code End */

    /*Automatic OTP Reader*/
    async requestReadSmsPermission() {
        try {
            var granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_SMS, {
                    title: 'Auto Verification OTP',
                    message: 'need access to read sms, to verify OTP'
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('READ_SMS permissions granted', granted);
                granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.RECEIVE_SMS, {
                        title: 'Receive SMS',
                        message: 'Need access to receive sms, to verify OTP'
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                    console.log('RECEIVE_SMS permissions granted', granted);
                    SmsListener.addListener(message => {
                        let verificationCodeRegex = /Your one time password : ([\d]{6})/;
                        var num = parseInt(message.body.match(/\d+/),10);
                        this.setState ({
                            otpno:parseInt(message.body.match(/\d+/),10),
                        })
                        this.Insert_Data_Into_Verifyotp(num);
                        if (verificationCodeRegex.test(message.body)) {
                            let verificationCode = message.body.match(verificationCodeRegex)[1]


                            YourPhoneVerificationApi.verifyPhoneNumber(
                                message.originatingAddress,
                                verificationCode
                            ).then(verifiedSuccessfully => {
                                if (verifiedSuccessfully) {
                                    subscription.remove()
                                    return

                                }

                                if (__DEV__) {
                                    console.info(
                                        'Failed to verify phone `%s` using code `%s`',
                                        message.originatingAddress,
                                        verificationCode
                                    )
                                }
                            })
                        }
                    });
                } else {
                //    alert('RECEIVE_SMS permissions denied');
                    console.log('RECEIVE_SMS permissions denied');
                }
            } else {
                //alert('READ_SMS permissions denied');
                console.log('READ_SMS permissions denied');
            }
        } catch (err) {
            alert(err);
        }
    }
    handleBackPress = () => {
        this.props.navigation.navigate('PersonalDetailOption');
    }

    /*Automatic OTP Reader End*/
    render() {

        return(
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}
                          onPress={()=> this.props.navigation.navigate('MobileNo')}>
                        <Button transparent style={{marginTop:10}}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                    <Body>
                        <Title style={styles.main_salon_name}>Verify OTP</Title>

                    </Body>
                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={styles.image_container}>
                        <Image
                            source={require('../../images/logo_small.png')}
                            style={styles.logo}
                        />
                    </View>

                    <Text style={styles.otp_display_text}>Enter OTP sent on your Registered Number </Text>
                    <CodeInput
                        ref="codeInputRef2"
                        codeLength={4}
                        secureTextEntry
                        className={'border-circle'}
                        activeColor='#E1A7C5'
                        inactiveColor='rgba(0,0,0, 1.3)'
                        ignoreCase={true}
                        inputPosition='center'
                        size={40}
                        keyboardType="numeric"
                        autoFocus={true}
                        containerStyle={{ marginTop: 30,}}
                        codeInputStyle={{ borderWidth: 1.5,fontSize:14 }}
                        onFulfill={(code) => this._onFulfill(code)}/>

                    {

                        this.state.ActivityIndicator_Loading ? <ActivityIndicator color='#009688' size='large'style={styles.ActivityIndicatorStyle} /> : null

                    }

                </Content>

                <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                    justifyContent:'center',alignItems:'center',textAlign:'center',}}
                         onPress={()=> this.Insert_Data_Into_Verifyotp()}>
                    <Text style={{color:'#fff',fontSize:14,fontFamily:'Calibri Bold',}}
                          onPress={()=> this.Insert_Data_Into_Verifyotp()}  >Continue</Text>
                </Button>

            </Container>
        );
    }
}
