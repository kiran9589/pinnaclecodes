export default {
    header_style:{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65},
    main_salon_name:{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'26%',color:'#fff',fontWeight:'bold',},
    text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'gray'},
    no_error_text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#CE3152',},
    error_text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#CE3152',},
    SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: '#000', height: 30, borderRadius: 5 , marginTop:0,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:10,},
    number_SectionStyle:{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff',
        borderWidth: .5, borderColor: '#000', height: 30, borderRadius: 5 , marginTop:20,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:5,width:'18%'},
    input_number_SectionStyle:{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff',
        borderWidth: .5, borderColor: '#000', height: 30, borderRadius: 5 , marginTop:20,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:5,width:'65%'},
    ImageStyle: {padding: 5, margin: 5, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'},
    logo: {width: 200, height:80,},
    image_container: {justifyContent: 'center', alignItems: 'center',marginTop:20,},
    otp_display_text:{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:10,color:'#000sss',fontWeight:'bold',textAlign:'center'},




    /*Offer Styles*/
    main_card_style:{
        borderRadius:5,
        width:'100%',
        height:'auto',

    },
    card_body_style:{
        padding:5,
    },
    main_offer_style:{
        textAlign:'left',
        fontSize:15,
        fontWeight:'bold',
        color:'#474A51',
        paddingTop:'1%',
        paddingLeft:0,
        paddingRight:5,
        paddingBottom:2,
    },
    submain_offer_style: {
        textAlign:'left',
        fontSize:12,
        color:'#333',
        paddingTop:1,
        paddingLeft:0,
        paddingRight:5,
        paddingBottom:1,

    },
    sub_offer_style:{
        textAlign:'left',
        fontSize:12,
        color:'#333',
        paddingLeft:0,
        paddingRight:5,
        paddingBottom:5,
    },

    more_btn:{
        color:'#7B98C1',
        fontSize:15,
        paddingTop:3,
        fontWeight:'bold',
        paddingLeft:0,
        paddingRight:3,
        paddingBottom:5,
    },

    underline_view:{
        width:'100%',
        height:0.5,
        backgroundColor:'#666',
        margin:5,
    },
    promo_code:{
        flexDirection:'row',
        padding:10,
        height:55,
        width:'auto',

    },
    first_offer:{
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:0,
        paddingRight:5,
        backgroundColor:'#FFFAE6',
        borderColor:'#ccc',
        borderWidth:0.5,
        borderTopRightRadius:5,
        borderBottomRightRadius:5,
        justifyContent:'center',
        alignItems:'center',
    },
    second_offer:{
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:5,
        paddingRight:5,
        backgroundColor:'#FFFAE6',
        borderColor:'#ccc',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        borderWidth:0.5,
        borderRadius:5,
        marginLeft:-7,
    },
    promo_code_text_details:{
        textAlign:'center',
        color:'#000',
        fontSize:14,
        fontWeight:'bold'
    },
}