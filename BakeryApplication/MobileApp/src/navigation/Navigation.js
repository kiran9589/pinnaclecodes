import React, { Component } from 'react';

import {
    StyleSheet,
    Platform,
    View, TextInput,
    Image,Modal,
    TouchableOpacity,TouchableHighlight,
    YellowBox,ScrollView,
    ImageBackground,
    BackHandler, DeviceEventEmitter
} from 'react-native';

import { Container, Content, Icon, Header, Body ,Text,  Button,Drawer,SideBar,Left,Right,Title} from 'native-base'

import { createDrawerNavigator,DrawerItems,createStackNavigator,createAppContainer,createBottomTabNavigator,createSwitchNavigator } from 'react-navigation';

import Offers from '../offers/Offers';

import Notification from '../notification/Notification';

import MyAccount from '../myaccount/MyAccount';
import DemoMyAccount from '../myaccount/DemoMyAccount';

import Help from '../help/Help';
import Dashboard from'../dashboard/Dashboard';
import LoginMyAcccount from'../mobileno/LoginMyAcccount';
import AccountWithoutLogin from "../myaccount/AccountWithoutLogin";


const HomeActivity_StackNavigator = createStackNavigator({
    Home: {
        screen: Dashboard,
        navigationOptions: {

            header:null,


        }
    },


});


const Offers_StackNavigator = createStackNavigator({
    Offers: {
        screen: Offers,
        navigationOptions: {

            header:null,


        }
    },
});


const Notification_StackNavigator = createStackNavigator({
    Notification: {
        screen: Notification,
        navigationOptions: {

            header:null,


        }
    },
});
const Profile_StackNavigator = createSwitchNavigator({
    MyAccount: {
        screen: AccountWithoutLogin,
        navigationOptions: {

            header:null,


        }
    },
});
const Help_StackNavigator = createStackNavigator({
    Help: {
        screen: Help,
        navigationOptions: {

            header:null,


        }
    },
});

const MyDrawerNavigator = createBottomTabNavigator({


        Home: {
            screen: HomeActivity_StackNavigator,
            navigationOptions:{
                tabBarLabel:'Home',
                tabBarIcon:({ tintcolor }) => (
                    <Icon type="Entypo" name="home"
                          style={{fontSize: 24, color: '#D977AB',}}/>
                ),
            }
        },


        Offers: {
            screen: Offers_StackNavigator,
            navigationOptions:{
                tabBarLabel:'Offers',
                tabBarIcon:({ tintcolor }) => (
                    <Icon type="MaterialIcons" name="local-offer"
                          style={{fontSize: 24, color: '#D977AB',}}/>
                          ),
            }
        },


        Account: {
            screen: Profile_StackNavigator,
            navigationOptions:{
                tabBarLabel:'My Account',
                tabBarIcon:({ tintcolor }) => (
                    <Icon type="MaterialCommunityIcons" name="account"
                          style={{fontSize: 24, color: '#D977AB',}}/>
                          ),
            },

        },
        Help_Center: {
            screen: Help_StackNavigator,
            navigationOptions:{
                tabBarLabel:'Help Center',
                tabBarIcon:({ tintcolor }) => (
                    <Icon type="Entypo" name="help-with-circle"
                          style={{fontSize: 24, color: '#D977AB',}}/>
                ),
            }

        },


    },
    {
        tabBarOptions: {
            activeTintColor: '#D977AB',
            inactiveTintColor: 'gray',
            showIcon: true,
            style: {
                backgroundColor: 'white',
                paddingTop:5,
            }
        },


    },
);


const styles = StyleSheet.create({

    MainContainer :{

        paddingTop: (Platform.OS) === 'ios' ? 20 : 0,

    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#FFF'
    },
    drawerHeader: {
        height: 170,
        backgroundColor: '#e5e5e5',
    },
    drawerImage: {
        height:140,
        width: '100%',
    },
    drawerImages:{
        width:70,
        height:70,
        marginTop:5,
        marginLeft:10,
    },
    inputtext:{
        flex:1,
        fontSize:16,
        padding:10,
        fontFamily:'Calibri Regular',
        height:45,
        width:300,
        color:'black',

    },
    decoratetext:{
        justifyContent:'center',
        fontSize:18,
        color:'#5d19e3',
        fontFamily:'Calibri Regular',
        marginTop:5,
        marginLeft:10


    },
    optionalText: {
        textAlign:'center',
        alignItems:'center',
        justifyContent:'center',
        fontSize:20,
        color:'#F82E56',
        fontStyle:'italic',


    },
    icons: {
        alignItems:'flex-end',
        justifyContent:'flex-end',
        marginLeft:'85%',
        width:28,
        height:28,
        marginTop:20,
    },

    newmodal: {
        flex: 1,
        marginTop:50,
        backgroundColor:'#fff'
    },

});

export default createAppContainer(MyDrawerNavigator);