import React, { Component } from 'react';

import { AppRegistry, StatusBar,StyleSheet, Text, View, Button,Image } from 'react-native';

import { StackNavigator,createSwitchNavigator,createAppContainer,createStackNavigator } from 'react-navigation';

import MyDrawerNavigator from '../../src/navigation/Navigation';

import DescriptionScreen from '../../src/description/DescriptionScreen';

import ItemList from '../../src/itemlist/ItemList';

import MobileNo from '../../src/mobileno/MobileNo';

import Verification_Screen from '../../src/verifyotp/Verification_Screen';

import Mycart from '../../src/mycart/Mycart';

import PersonalDetailOption from '../../src/personaldetail/PersonalDetailOption';

import Welcome from '../../src/welcome/Welcome';

import Aboutus from '../../src/aboutus/Aboutus';

import Thankyou from '../../src/thankyou/Thankyou';

import ReviewPage from '../../src/reviewpage/ReviewPage';

import Category from "../../src/category/Category";

import WriteReview from "../../src/reviewpage/WriteReview";

import MyOrder from "../../src/myaccount/MyOrder";

import EditProfile from "../../src/myaccount/EditProfile";

import MyCartOffer from '../offers/MyCartOffer';

import MyOrderDetails from '../myaccount/MyOrderDetails';

import DemoMyAccount from '../myaccount/DemoMyAccount';

import AccountWithoutLogin from '../myaccount/AccountWithoutLogin';

import LoginMyAcccount from '../mobileno/LoginMyAcccount';

import RewardScreen from '../myaccount/RewardScreen';

import ReadContact from '../dashboard/ReadContact';

import GetAllAddress from '../address/GetAllAddress';

import  AddAddress from '../address/AddAddress';

import  AddContactByDrawer from '../address/AddContactByDrawer';

import  SuccessScreen from '../statusscreen/SuccessScreen';

import  FailureScreen from '../statusscreen/FailureScreen';

import  EmptyCartScreen from '../mycart/EmptyCartScreen';

import  BlankAddress from '../address/BlankAddress';


const Demoscreen=createSwitchNavigator({
    Welcome: { screen: Welcome,
        navigationOptions:{
            header:null,


        }},
    MyDrawerNavigator: { screen: MyDrawerNavigator,
        navigationOptions:{
            header:null,


        }},

})


const NavigationStack = createStackNavigator(
    {


        Demoscreen: { screen: Demoscreen,
            navigationOptions:{
                header:null,


            }},


        ItemList: { screen: ItemList,
            navigationOptions:{
                header:null,


            }},

        Category: { screen: Category,
            navigationOptions:{
                header:null,


            }},


        DescriptionScreen: { screen: DescriptionScreen,
            navigationOptions:{
                header:null,
            }},



        Verification_Screen: { screen: Verification_Screen,
            navigationOptions:{
                header:null

            }},
        MobileNo: { screen: MobileNo,
            navigationOptions:{
                header:null

            }},


        PersonalDetailOption: { screen: PersonalDetailOption,
            navigationOptions:{
                header:null

            }},
        Mycart: { screen: Mycart,
            navigationOptions:{
                header:null
            }},
        Aboutus: { screen: Aboutus,
            navigationOptions:{
                header:null
            }},

        Thankyou: { screen: Thankyou,
            navigationOptions:{
                header:null
            }},
        ReviewPage: { screen: ReviewPage,
            navigationOptions:{
                header:null
            }},
        WriteReview: { screen: WriteReview,
            navigationOptions:{
                header:null
            }},
        MyOrder: { screen: MyOrder,
            navigationOptions:{
                header:null
            }},
        MyCartOffer: { screen: MyCartOffer,
            navigationOptions:{
                header:null
            }},
        EditProfile: { screen: EditProfile,
            navigationOptions:{
                header:null
            }},
        MyOrderDetails: { screen: MyOrderDetails,
            navigationOptions:{
                header:null
            }},
        AccountWithoutLogin: { screen: AccountWithoutLogin,
            navigationOptions:{
                header:null
            }},

        LoginMyAcccount: { screen: LoginMyAcccount,
            navigationOptions: {
                header: null

            }},
        RewardScreen: { screen: RewardScreen,
            navigationOptions: {
                header: null

            }},
        ReadContact: { screen: ReadContact,
            navigationOptions: {
                header: null

            }},
        GetAllAddress: { screen: GetAllAddress,
            navigationOptions: {
                header: null

            }},
        AddAddress: { screen: AddAddress,
            navigationOptions: {
                header: null

            }},
        SuccessScreen: { screen: SuccessScreen,
            navigationOptions: {
                header: null

            }},
        FailureScreen: { screen: FailureScreen,
            navigationOptions: {
                header: null

            }},
        EmptyCartScreen: { screen: EmptyCartScreen,
            navigationOptions: {
                header: null

            }},
        AddContactByDrawer: { screen: AddContactByDrawer,
            navigationOptions: {
                header: null

            }},
        BlankAddress: { screen: BlankAddress,
            navigationOptions: {
                header: null

            }},
    });

export default createAppContainer(NavigationStack);



