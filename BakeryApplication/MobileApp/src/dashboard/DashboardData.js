/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, StatusBar,ActivityIndicator,
    Modal, TouchableWithoutFeedback, Alert, ListView
} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button, Text,Right,Title, Subtitle,Body,Tab, Tabs,Left } from 'native-base';
import styles from './myaccount.style';
var Realm = require('realm');
let realm;


type Props = {};
export default class DemoMyAccount extends Component<Props> {

    constructor(props) {

        super(props);


        try{
            console.warn("Under Try Block");
            realm = new Realm({ path: 'CakeDatabase.realm'});
            var User_Details = realm.objects('User_Details');
            var mobile_number=User_Details[0].mobile_number;
            var loading_state=true;
            console.warn("Under Try Block mobile_number ==== "+ mobile_number);

        }
        catch (e) {
            this.props.navigation.navigate('LoginMyAcccount');
            // var mobile_number=this.props.navigation.state.params.mobile_no;
            console.warn("catch block mobile number:==="+mobile_number);
            var loading_state=false ;

        }
        this.state = {
            isLoading: loading_state,
            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,
            mobile_number:mobile_number,
            user_name:'',
            user_email:'',
            user_mobile:'',
            rewardPoints:'',
            id:null,
            direct_login:"1",


        };
    }
    Function_togoto_EditActivity = (user_name,user_mobile,user_email,user_id) => {



        this.props.navigation.navigate('EditProfile',{user_name,user_mobile,user_email,user_id});
    }

    refreshPage=()=>{

        console.log("this.state.mobile_number ==== "+this.state.mobile_number);
        console.warn("this.state.mobile_number ==== "+this.state.mobile_number);
        return fetch('http://3.13.212.113:3000/user/details',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },

                body: JSON.stringify(
                    {
                        mobile_no:this.state.mobile_number,

                    })

            }).then((response) => response.json()).then((response) =>
        {
            this.setState({
                id:response.id,
                user_name:response.name,
                user_email:response.email,
                user_mobile:response.mobile_no,
                rewardPoints:response.rewardPoints,
                isLoading: false,
            });

            // console.log("The Data is = " + JSON.stringify(this.state.data).toString());
            //console.log("The weightPrice is = " + JSON.stringify(this.state.weightPrice).toString());

        }).catch((error) =>
        {
            console.error(error);
            this.setState({
                isLoading: false,
            });

        });

    }

    componentWillMount() {
        console.warn("Under componentWillMount");
        this.refreshPage();
    }


    componentDidMount()
    {
        console.warn("this.props.navigation.state.params"+this.props.navigation.state.params);
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                console.warn("Page Refresh");
                console.warn("this.props.navigation.state.params==="+this.props.navigation.state.params);
                console.warn("under will focus subsription ==== "+ this.state.mobile_no);
                this.refreshPage();


            }
        );
        // alert("The Correct Mobile Number is = "+ this.state.mobile_number);
        this.refreshPage();
    }




    myorder = (user_id) => {
        this.props.navigation.navigate('MyOrder',{user_id});
    }
    setPaymentModalVisible(visible) {

        this.setState({paymentmodal: visible});
    }

    openAlertMessage=()=>{
        Alert.alert(
            'Logout',
            'Are You Sure You want to logout from App. ',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size={'large'} color={'#000'}/>
                    <Text style={{color: '#000'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#FCECF4'}}>

                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />

                    </Left>


                    <Body style={{padding:5,}}>
                        <Title style={{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'12%',color:'#fff',fontWeight:'bold',}}>
                            My Account</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>

                    {/*Main Header Title*/}
                    <View style={{backgroundColor:'#D977AB',marginBottom:'3%'}}>
                        <View style={{alignItems:'flex-end',justifyContent:'flex-end',paddingRight:0,paddingBotom:5,}}>
                            <TouchableOpacity onPress={()=>this.Function_togoto_EditActivity(this.state.user_name,this.state.user_mobile,this.state.user_email,this.state.id)}>
                                <Icon type="Entypo" name="edit"
                                      style={{paddingRight:'5%',color:'#fff', fontSize:18,paddingTop:'1%'}}/>
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection:'column',paddingTop:0,paddingBottom:0,alignItems:'center',justifyContent:'center',}}>

                            <Image source={require('../../images/bekery_user.png')}
                                   style={{width: 95, height: 95, borderRadius: 95/2,}} />

                            <Text style={[styles.client_user_name,{paddingBottom:3,fontWeight:'bold'}]}>{this.state.user_name}</Text>
                            <Text style={[styles.client_user_name,{fontSize:14,marginTop:'3%',}]}> {this.state.user_mobile}</Text>

                            <Text style={[styles.client_user_name,{fontSize:14,marginTop:'2%',marginBottom:'1%'}]}>{this.state.user_email}</Text>

                        </View>
                    </View>

                    {/*Options List */}

                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                        <TouchableOpacity onPress={()=>this.myorder(this.state.id)}>

                            <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>My Orders</Text>
                                <View style={styles.list_order_seperator}/>
                                <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}>View All Orders</Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                        <TouchableOpacity onPress={()=>alert("Data Will Comming Soon")}>
                            <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Refer Points</Text>
                                <View style={styles.list_order_seperator}/>
                                <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}>View All Refer Point</Text>
                            </View>
                        </TouchableOpacity>
                    </View>



                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                        <TouchableOpacity onPress={()=>this.openAlertMessage()}>
                            <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Logout</Text>

                            </View>
                        </TouchableOpacity>
                    </View>

                </Content>

            </Container>
        );
    }
}
