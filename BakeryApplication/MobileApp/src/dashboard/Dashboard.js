import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert,StatusBar,PermissionsAndroid,
} from 'react-native';
import { Container, Header, Item, Input, Icon,Text, Button,Right,Content,Body,Title,DatePicker,Card, CardItem,Left,Badge   } from 'native-base';
import Toast from 'react-native-simple-toast';
import Carousel from 'react-native-smart-carousel';
import ContactsWrapper from 'react-native-contacts-wrapper';

import styles from './dashboard.style';


import slider_one from '../../images/slider_one.png';
import slider_two from '../../images/slider_two.jpg';
import slider_three from '../../images/slider_three.jpg';
import offer_one from '../../images/offer_one.png'
import offer_wo from '../../images/offer_wo.jpg';
import offer_three from '../../images/offer_three.jpg';
import Permissions from 'react-native-permissions'

var Realm = require('realm');
let realm ;



const datacarousel = [
    {
        "id": 349964,
        "imagePath": slider_one, // URL

    },
    {
        "id": 349403,
        "imagePath": slider_two, // imported
    },
    {
        "id": 349404,
        "imagePath": slider_three, // imported
    },

];


const offercarousel = [
    {
        "id": 378890,
        "imagePath": offer_one, // URL

    },
    {
        "id": 978895,
        "imagePath": offer_wo, // imported
    },
    {
        "id": 978855,
        "imagePath": offer_three, // imported
    },

];
export default class Dashboard extends Component<> {

    constructor(props) {

        super(props);
        realm = new Realm({
            path: 'CakeDatabase.realm',
            schema: [{name: 'Cart_Details',
                properties:
                    {
                        cart_cake_id: {type: 'int',   default: 0},
                        stack_id: {type: 'int',   default: 0},
                        cart_image:'string',
                        cart_name:'string',
                        cart_productId: {type: 'int',   default: 0},
                        cart_weight: 'string',
                        cart_quantity: 'string',
                        cart_price: 'string',
                        Base_price: 'string',
                        nameoncake:'string',
                        delivery_date:'string',
                        occassion:'string',

                    }},
                {name: 'User_Details',
                    properties:
                        {
                            mobile_number:'string',
                            login_value:{type: 'int',   default: 0},

                        }}]
        });
        this.state = {
            removeitemmodal:false,
            message_send_loader:false,
            total_cart_count:0,
            local_mobile_no:'',
        };

    }


/*=======================
* Open Reward Screen
* =======================*/

readcontacts=(mobile_number)=>{
    this.props.navigation.navigate('ReadContact',{mobile_number});
}
/*============================
* Code to send refer message
* ============================*/

Function_To_sendReferMessage = (single_contact) =>
{

            this.setState({message_send_loader: true}, () => {
                fetch('http://3.13.212.113:3000/message/newReferals',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                sender: 8793694590,
                                receivers: [+919404584761,8793694590],
                                referalLink: ""


                            })

                    }).then((response) => response.json()).then((response) => {

                    if (response.message != "Refral links sent successfully") {
                        this.setState({message_send_loader: false});
                        Toast.show("Message Dose Not Send", Toast.SHORT);
                    } else {
                        this.setState({message_send_loader: false});
                        Toast.show("Message Send Successfully", Toast.SHORT);

                    }


                }).catch((error) => {
                    console.error(error);
                    this.setState({message_send_loader: false});
                });
            });
        }



/*==============================
* Asking Permission
* ================================*/
    _requestPermission = () => {
        Permissions.request('contacts').then(response => {
            // Returns once the user has chosen to 'allow' or to 'not allow' access
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            if (response === "authorized") {
               this.onButtonPressed();

            } else {
                this._requestPermission()
            }
        })
    }


checkPermission=()=>{
    Permissions.check('contacts').then(response => {
        // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        if (response === "authorized") {
            this.onButtonPressed();
        } else {
            this._requestPermission();
        }

    })
}

/*=========================
* Get Contact
* ==========================*/
    onButtonPressed = () => {
        ContactsWrapper.getContact()
            .then((contact) => {
                // Replace this code

                fetch('http://3.13.212.113:3000/message/newReferals',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                sender: 8793694590,
                                receivers: [contact['phone']],
                                referalLink: ""


                            })

                    }).then((response) => response.json()).then((response) => {

                    if (response.message != "Refral links sent successfully") {
                        this.setState({message_send_loader: false});
                        Toast.show("Message Dose Not Send", Toast.SHORT);
                    } else {
                        this.setState({message_send_loader: false});
                        Toast.show("Message Send Successfully", Toast.SHORT);

                    }


                }).catch((error) => {
                    console.error(error);
                    this.setState({message_send_loader: false});
                });
            })
            .catch((error) => {
                console.log("ERROR CODE: ", error.code);
                console.log("ERROR MESSAGE: ", error.message);
            });
    }

/*==================
* End Code
*===================*/
    FunctionToOpenCategory =() => {
        this.props.navigation.navigate('Category');
    }
    FunctionToOpenOffer =() => {
        this.props.navigation.navigate('MyCartOffer');
    }
    setRemoveItemModalVisible(visible) {

        this.setState({removeitemmodal: visible});

    }

    onMyCart=()=>{
        this.props.navigation.navigate('Mycart');
    }

    /*Register User API End*/


componentDidMount(){

    this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {
            try{
                realm = new Realm({ path: 'CakeDatabase.realm'});
                var cart_count = realm.objects('Cart_Details').length;
                this.setState({total_cart_count:cart_count})
            }
            catch (e) {
                this.setState({total_cart_count:0})
            }


        }
    );

}

/*===============================
* Open Read Contat Screen
* ================================*/
open_ReadContact = () => {
    try {
        realm = new Realm({ path: 'CakeDatabase.realm'});
        var User_Details = realm.objects('User_Details');
        var mobile_number=User_Details[0].mobile_number;
        this.setState({
            local_mobile_no:User_Details[0].mobile_number,
        });

        this.readcontacts(User_Details[0].mobile_number);
}
    catch (e) {
        this.props.navigation.navigate('MobileNo',{rotateID:5});

}

}

    /*=======================
    * UI Started
    * =======================*/
    render() {

        return(
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left style={{paddingTop:'2%'}}>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />


                    </Left>
                    <Body style={{width:'100%'}}>
                     <Title style={styles.main_salon_name}>Dashboard </Title>

                    </Body>
                    <Right>

                        {this.state.total_cart_count!=0 ?
                                 <TouchableOpacity onPress={()=>this.onMyCart()}>
                                     <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                         <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                     </Badge>
                                        <Icon type="SimpleLineIcons" name="basket"
                                          style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                                </TouchableOpacity>
                                        :
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>
 }
                    </Right>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content  style={{padding:2}}>

                    {/*Section 2- About Us*/}

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Aboutus')}>
                        <View style={styles.aboutus_view}>

                            <View style={[styles.aboutus_text,]}>
                                <Text style={{fontSize:18,fontFamily:'Calibri Regular',marginLeft:'10%',
                                    color:'#D977AB',fontWeight:'bold',textAlign:'center',alignItems:'center'}}>
                                    Welcome to the COUCHES ..

                                </Text>
                                <Text style={{fontSize:16,fontFamily:'Calibri Regular', color:'#666666',lineHeight:22,textAlign:'justify'}}
                                      numberOfLines={5}>
                                    THE PATISSERIE, a Pune based online bakery!
                                    It all started by Anushka Jaju, who embarked on her career as a Chef from Institute of Hotel Management (IHM),Jaipur.
                                    With a passion for baking, she took her talent and creativity to the next level by baking a very finest range of customized cakes that can immediately bring a Joy to any event. Here we season each of our cake with the European and modern techniques to take you to the rich creamy world.

                                </Text>
                            </View>

                        </View>
                    </TouchableOpacity>
                    {/*Close Section 2- About Us*/}

                    {/*Section 1:- Cake Sliders*/}

                    {/*<Text style={{fontSize:20, fontWeight:'bold',textAlign:'center',paddingTop:2,color:'brown',marginTop:'1%',marginBottom:'2%'}}>
                            Cakes
                        </Text>*/}
                        <Carousel
                            data={datacarousel}
                            autoPlay={true}
                            playTime={2000}
                            height={220}
                            navigation={true}
                            navigationColor={'#000'}
                            navigationType={'dots'}
                            parallax={true}
                            onPress={()=>this.FunctionToOpenCategory()}
                        />
                    {/*Close Section 1:- Cake Sliders*/}


                    {/*Section Forth:-  refer and win*/}

                    <View style={styles.refer_win_style}>


                        <View style={{flexDirection:'row',marginTop:'3%'}}>
                            <View style={[styles.refer_win_second_view,{ borderRightColor:'#666', borderRightWidth:1,borderTopStyle:{padding:5}}]}>
                                <Text style={[styles.text_referearn,{color:'#1A406A'}]}>REFER </Text>
                                <Text style={[styles.text_referearn,{color:'#000'}]}> & </Text>
                                <Text style={[styles.text_referearn,{color:'#2074B7'}]}>EARN</Text>
                            </View>
                            <View style={[styles.refer_win_first_view,]}>
                                <Text style={styles.refer_win_option_style}
                                      onPress={()=>Linking
                                          .openURL('https://api.whatsapp.com/send?&text=I love Couches cake its dekicious. Use my referal link to get a discount now ! https://referallink/ZwdZxRE3 via @Couches' )}>
                                    1. WHATAPPS.
                                </Text>
                                <Text style={[styles.refer_win_option_style,{paddingTop:'5%'}]}
                                      /*onPress={()=>Linking.openURL(`sms:?addresses=null&body=Dummy SMS Text`)}*/
                                       onPress={()=>this.open_ReadContact()} >
                                    2. SMS.
                                </Text>
                            </View>
                        </View>

                        <Text style={styles.refer_text}>Refer your friend and earn 5% reward points on first order.</Text>






                        {/* <View style={styles.status_right}>
                            <Image source={require('../../images/cancel_icon.png')}
                                   style={{width:24,height:24}}/>
                        </View>*/}

                        {/*<Text style={styles.refer_win_satic_text}>
                            You will refer to your friend and earn moeny. you will choose one of the option.
                        </Text>
                        <View style={styles.refer_win_text_style}>
                            <Text style={styles.refer_win_option_style}
                                  onPress={()=>Linking
                                      .openURL('https://api.whatsapp.com/send?&text=Dummy SMS Text')}>
                               1. WHATAPPS.
                            </Text>
                            <Text style={styles.refer_win_option_style}
                                  onPress={()=>Linking.openURL(`sms:?addresses=null&body=Dummy SMS Text`)}  >
                                2. SMS.
                            </Text>
                        </View>*/}
                    </View>

                    {/*Close Section Forth:-  refer and win*/}


                    {/*Section 3: Offers*/}
                    {/*  <Text style={{fontSize:20, fontWeight:'bold',textAlign:'center',paddingTop:2,color:'brown',marginTop:'1%',marginBottom:'2%'}}>
                            Offers
                        </Text>
*/}

                 {/*   <Carousel
                        data={offercarousel}
                        autoPlay={true}
                        playTime={2000}
                        height={160}
                        width={'100%'}
                        navigation={true}
                        navigationColor={'#000'}
                        navigationType={'dots'}
                        parallax={true}
                        onPress={()=>this.FunctionToOpenOffer()}
                    />*/}
                {/*Close Section 3: Offers*/}


                </Content>



            </Container>
        );
    }
}

