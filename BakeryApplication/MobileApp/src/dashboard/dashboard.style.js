export default {
    header_style:{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65},
    main_salon_name:{fontSize:17,fontFamily:'Proxima Nova Reg',paddingTop:6,marginLeft:'40%',color:'#fff',fontWeight:'bold',textAlign:'center'},
    main_column_style:{
        flexDirection:'column',
        paddingLeft:'1%',
        margin:3,
        paddingBottom:'2%',
        backgroundColor:'#f2f2f2',

    },
    element_style:{
        width:'30%',
        height:'auto',
        marginTop:5,
        marginLeft:5,
        marginRight:5,
        marginBottom:5,

    },
    main_view:{
        marginTop:'3%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    row_one_style:{
        padding:10,
    },
    row_two_style:{
        padding:10,
        marginLeft:'5%',
        marginTop: '2%',
    },
    row_three_style_text:{
        padding:10,
    },
    row_four_style_text:{
        padding:10,
        marginLeft:'5%',

    },
    row_one_style_text: {
        fontSize: 16,
        textAlign:'center'

    },
   /* technician_cardstyle:{
        width:115,
        height:115,
        marginTop:10,
        marginLeft:10,
        marginRight:10,
        marginBottom:0,
        backgroundColor:'#fff',
    },
    technician_view:{
        flexDirection:'column',
        width:'100%'
    },
    service_name:{
        fontSize:14,
        fontFamily:'Calibri Regular',
        padding:3,
        fontWeight:'bold',
        color:'#333333',
        textAlign:'center',
        marginLeft:'10%'
    },*/
    main_headings:{
        marginTop:'0%',
        fontSize:20,
        textAlign:'center',
        color:'#474747',
        fontFamily:'Calibri Regular',
        fontWeight:'bold',

    },

    /* Mental Terms of Service*/
    payment_modal_style:{
        flex: 1,
        width: '100%',
        height: '100%',
        alignContent:'flex-start',
        justifyContent:'flex-start',
        alignItems:'flex-start',
        backgroundColor:'rgba(0,0,0,0.7)',
        padding:10,
        marginTop:'12%',


    },
    remove_item_card:{
        position: 'absolute',
        width: '100%',
        top:'5%',
        height: '100%',
        marginLeft:10,
        marginRight:10,
    },
    remove_text:{
        fontSize:18,
        fontFamily:'Calibri Regular',
        fontWeight:'bold',
        textAlign:'center',
        padding:5,
    },

    term_of_service:{
        fontSize:16,
        fontFamily:'Calibri Regular',
        textAlign:'center',
        padding:5,
    },

    remove_btn_style:{
        width:'50%',
        justifyContent:'center',
        alignItems:'center',

    },
    remove_btn_text_style:{
        textAlign:'center',
        fontSize:14,
        fontFamily:'Calibri Regular',
        fontWeight:'bold',
    },
    remove_description:{
        fontSize:16,
        fontFamily:'Calibri Regular',
        fontWeight:'bold',
        textAlign:'center',
        padding:5,
    },
    aboutus_view:{
        marginTop:'3%',
        flexDirection:'row',
        padding:5,
        marginLeft:5,
        marginRight:5,
        marginBottom:'5%',
        width:'95%',
    },
    aboutus_image:{
        width:'30%',
        padding:5,
        marginTop:'2%',
        marginLeft:'5%'

    },
    aboutus_text:{
        paddingTop:0,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:5,

    },
    refer_win_style:{


        alignItems:'center',
        width:'95%',
        borderRadius:10,
        borderWidth:0.5,
        borderColor:'#fff',
        backgroundColor:'#fff',
        flexDirection:'column',
        paddingBottom:2,
        margin:10,
        paddingLeft:5,
        paddingRight:5,

        marginBottom:'4%'
    },
    refer_win_text_style:
        {
            flexDirection:'row',
            paddingTop:10,
            paddingLeft:5,
            paddingRight:5,
            paddingBottom:5,
        },
    refer_win_option_style:
        {
            fontSize:16,
            fontWeight:'bold',
            paddingLeft:10,
            paddingTop:5,
            paddingRight:5,
            color:'#506290'
        },
    refer_win_satic_text:
        {
            fontSize:13,
            fontWeight:'bold',
            paddingTop:3,
            marginTop:1,
            paddingLeft:5,
            paddingRight:5,
            color:'#000'
        },
    refer_win_partone:{
        width:'70%',
        padding:5.
    },
    refer_win_parttwo:{
        width:'20%',
        padding:5 ,
    },

    status_right : {
        justifyContent:'flex-end',
        alignItems:'flex-end',
        textAlign:'right',
        alignSelf: 'flex-end',
    },
    status_right_text : {
        fontSize:13,
        color:'#fff',
        padding:3,
        backgroundColor:'green',
        fontFamily:'Calibri Regular',
    },
    refer_win_first_view:{
        flexDirection:'column',
        padding:5,
        justifyContent:'center',



    },
    refer_win_second_view:{
        flexDirection:'column',
        paddingTop:5,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:0,
        marginTop:5,
        width:'55%',

    },
    text_referearn:{
        fontSize:20,
        fontWeight:'bold',
        textAlign:'center',
        alignItems:'center',
        paddingTop:1,

        fontStyle:'italic',
        paddingBottom:1,
        paddingRight:5,
        paddingLeft:5,


    },
    refer_text:{

        padding:5,
        textAlign:'center',
        fontSize:12,
        color:"#666",
        fontStyle: 'italic',
    },
    technician_cardstyle:{ width:115,
        height:150,
        margin:10,
    },

    technician_name:{fontSize:12, fontWeight:'bold', fontFamily:'Calibri Regular',paddingTop:3,paddingLeft:3,paddingRight:3,paddingBottom:0,color:'#fff',textAlign:'center'},
    technician_view:{flexDirection:'column',backgroundColor:'#22292fd6',bottom:0,position:'absolute',width:'100%'},
    service_name:{
        fontSize:14,  fontFamily:'Lato-Bold',
        paddingTop:3,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:5,
        color:'#fff',
        textAlign:'center'
    },
};