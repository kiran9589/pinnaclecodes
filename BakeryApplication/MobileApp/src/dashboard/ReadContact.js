import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
    FlatList,
    PermissionsAndroid,
    Image,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator, TextInput
} from 'react-native';
import { Container, Header, Item, Input, Icon,Text, Button,Right,Content,Body,Title,Left } from 'native-base';
import { CheckBox } from 'react-native-elements'

import Contacts from 'react-native-contacts';
import Toast from "react-native-simple-toast";

type Props = {};


export default class ReadContact extends Component<Props> {
    state={
        contacts: null,
        sort_contact: null,
        userNameArray: [],
        phoneNoArray : [],
        contact_ActivityIndicator_Loading:false,
        isLoading:true,
        contactname:'',
        ErrorStatus_contactname : false,
        searchTerm:'',
        checked:[],
    }

 onEntercontactname = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({contactname : TextInputValues, ErrorStatus_contactname : false,}) ;
        }else{
            this.setState({contactname : TextInputValues, ErrorStatus_contactname : true}) ;
        }
    }
    componentDidMount(){
        if(Platform.OS === 'ios'){
            Contacts.getAll((err, contacts) => {

                if (err) {
                    throw err;
                    this.setState({
                        isLoading:false
                    })
                }
                // contacts returned
                if(contacts){
                    contacts = contacts.map((obj, index) => {
                        obj["isChecked"] = false
                        if(contacts.length-1 == index){
                            this.setState({contacts, isLoading:false});
                        }
                    });
                }

            })
        }else if(Platform.OS === 'android'){
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS ||  PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
                {
                    title: 'Contacts',
                    message: 'This app would like to view your contacts.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err === 'denied'){
                        // error
                        this.setState({
                            isLoading:false
                        })
                    }
                    else {
                        // contacts returned in Array
                        this.setState({contacts,
                            isLoading:false,
                        },
                            function(){
                                this.arrayholder = contacts;
// contacts returned

                            });
                        if(contacts){
                            contacts.map((obj, index) => {
                                obj["isChecked"] = false
                                if(contacts.length-1 == index){
                                    this.setState({contacts, isLoading:false});
                                }
                            });
                        }

                    }
                })
            })
        }
    }


/*========================
* Demo Functions
* ========================*/








    search = text => {
        console.log(text);
    };
    clear = () => {
        this.search.clear();
    };
    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function(item) {
            //applying filter for the inserted text in search bar
            const itemData = item.givenName ? item.givenName.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            contacts: newData,
            search:text,
        });
    }
/*============================
* Add Phone Number to array
* ============================*/

 addDatatoArray=(username,phone_number)=>{
     let obj = {
         "userName": username,
         "phoneNo": + phone_number,
      };
     this.state.userNameArray.push(obj.userName);
     this.state.phoneNoArray.push(obj.phoneNo);

    }

/*==================================
* Send Refer and Earn Message
* ==================================*/
sendMessage = () =>{
    this.setState({contact_ActivityIndicator_Loading: true}, () => {
        fetch('http://3.13.212.113:3000/message/newReferals',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        //sender: this.props.navigation.state.params.mobile_number,
                        sender: this.props.navigation.state.params.mobile_number,
                        receivers:this.state.phoneNoArray,
                        referalLink: ""



                    })

            }).then((response) => response.json()).then((response) => {

            if (response.message != "Refral links sent successfully") {
                this.setState({contact_ActivityIndicator_Loading: false});
                Toast.show("Message Dose Not Send", Toast.SHORT);
            } else {
                this.setState({contact_ActivityIndicator_Loading: false});
                Toast.show("Message Send Successfully", Toast.SHORT);
                this.props.navigation.navigate('MyDrawerNavigator');

            }


        }).catch((error) => {
            console.error(error);
            this.setState({contact_ActivityIndicator_Loading: false});
        });
    });
}
/*========================
* UI start
*========================= */
 render () {
     if (this.state.isLoading) {
         return (
             <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                 <ActivityIndicator size={'large'} color={'#000'}/>
                 <Text style={{color:'#000'}}>Please Wait .....</Text>
             </View>
         );
     }
        return (


            <Container style={{backgroundColor:'#FCECF4',}}>
                <Header  style={{backgroundColor:'#333333',padding:10,height:60}}>
                    <Left style={{flexDirection:'row',paddingTop:'2%'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                    <Body>
                        <Title style={{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:'5%',paddingLeft:'15%',color:'#fff',fontWeight:'bold',}}>Select Contact</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={this.state.empty_phone_no?styles.error_SectionStyle:styles.SectionStyle}>

                        <TextInput
                            editable={true}
                            style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                            placeholder="Search Contact Name"
                            keyboardType = 'default'
                            autoFocus={true}
                            underlineColorAndroid="transparent"
                            onChangeText={text => this.SearchFilterFunction(text)}
                            value={this.state.search}
                        />

                    </View>
                    {/*<Text style={{fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',padding:5,color:'green'}}> {JSON.stringify(this.state.userNameArray)}</Text>*/}





                    <FlatList
                        data={this.state.contacts}
                        renderItem={({ item,index }) => (

                         <View>
                           {/* <TouchableOpacity onPress={()=> this.addDatatoArray(item.givenName, item.phoneNumbers[0].number)}>*/}

                               <View style={{flexDirection:'row'}}>

                                   <View style={{alignItems:'center',justifyContent:'center'}}>
                                       <Icon type="EvilIcons" name="user"
                                             style={{fontSize: 35, color: '#000',paddingTop:'1%',paddingRight:'3%'}}/>
                                   </View>

                                    <View style={{width:'60%'}}>
                                        <Text style={styles.contact_details}>
                                           {`${item.givenName} `} {item.familyName}
                                        </Text>


                                        {
                                            item.phoneNumbers[0].number.length >= 13 ?
                                                <Text style={styles.phones}>{item.phoneNumbers[0].number.substring(3,13)}</Text>
                                                :
                                                <Text style={styles.phones}>{item.phoneNumbers[0].number}</Text>
                                        }

                                    </View>
                                   <View style={{padding:10,alignItems:'center'}}>
                                       <CheckBox
                                           checkedIcon='dot-circle-o'
                                           uncheckedIcon='circle-o'
                                           checked={item.isChecked}
                                           onPress={()=> {
                                               item.isChecked = !item.isChecked;
                                               let phoneNum = item.phoneNumbers[0].number;
                                               if(item.isChecked) {
                                                   if (this.state.phoneNoArray.indexOf(phoneNum) == -1) {
                                                       this.state.phoneNoArray.push(phoneNum.toString());
                                                   }
                                               } else {
                                                   let index = this.state.phoneNoArray.indexOf(phoneNum);
                                                   if (this.state.phoneNoArray.indexOf(phoneNum) > -1) {
                                                       this.state.phoneNoArray.splice(index, 1);
                                                   }
                                               }
                                               // this.addDatatoArray(item.givenName, item.phoneNumbers[0].number)
                                           }}
                                       />

                                   </View>
                               </View>
                         {/*   </TouchableOpacity>*/}

                             <View style={{width:'100%',backgroundColor:'#666',height:1,}}/>
                         </View>

                        )}
                        //Setting the number of column
                        numColumns={1}
                        keyExtractor={(item, index) => index}
                    />
                </Content>
                <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                    justifyContent:'center',alignItems:'center',textAlign:'center',}}
                         onPress={()=>this.sendMessage()}>


                    {
                        this.state.contact_ActivityIndicator_Loading ? <ActivityIndicator color='#fff' size='large'
                                                                                  style={styles.ActivityIndicatorStyle}/> :

                            <Text style={{
                                color: '#fff',
                                fontSize: 14,
                                fontWeight: 'bold',
                                fontFamily: 'Calibri Bold',
                            }}
                                  onPress={()=>this.sendMessage()}>Continue </Text>
                    }
                </Button>
            </Container>


           /* <View style={styles.container}>


                <FlatList
                    data={this.state.contacts}
                    renderItem={({ item }) => (
                        <View>
                            <Text style={styles.contact_details}>
                                Name: {`${item.givenName} `} Surname: {item.familyName}
                            </Text>
                           {/!* {item.phoneNumbers.map(phone => (
                                <Text style={styles.phones}>{phone.label} : {phone.number}</Text>
                            ))}
                       *!/}

                            <Text style={styles.phones}>{item.phoneNumbers[0].label} : {item.phoneNumbers[0].number}</Text>
                       </View>
                    )}
                    //Setting the number of column
                    numColumns={1}
                    keyExtractor={(item, index) => index}
                />
            </View>*/
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        marginTop:30,
    },
    phones: {
        fontSize: 12,
        textAlign: 'left',
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        color: '#666',
        fontFamily:'Calibri Bold',
        marginBottom: 5,
    },
    contact_details: {
        textAlign: 'left',
        color: '#666',
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 3,
        fontFamily:'Calibri Bold',
        fontSize:16,
        fontWeight: 'bold',
    },
    SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: '#666', height: 40, borderRadius: 5 , marginTop:0,marginLeft:10,marginRight:10,
        marginBottom:10,paddingLeft:10,},
});