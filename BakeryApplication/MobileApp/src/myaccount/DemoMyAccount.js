/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, StatusBar, ActivityIndicator,
    Modal, TouchableWithoutFeedback, Alert, ListView, TextInput,ScrollView,
} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button, Text,Right,Title, Subtitle,Body,Tab, Tabs,Left,Card, CardItem, } from 'native-base';
import styles from './myaccount.style';
import Toast from "react-native-simple-toast";
import CodeInput from 'react-native-confirmation-code-input';
import Snackbar from 'react-native-snackbar';

var Realm = require('realm');
let realm;


type Props = {};
export default class DemoMyAccount extends Component<Props> {

    constructor(props) {

        super(props);

   /*     try{
            console.warn("Under Try Block");
            realm = new Realm({ path: 'CakeDatabase.realm'});
            var User_Details = realm.objects('User_Details');
            var mobile_number=User_Details[0].mobile_number;
            console.warn("Under Try Block mobile_number ==== "+ mobile_number);
            var loading_status =true;

        }
        catch (e) {

            var mobile_number='';
            var loading_status =false;
            this.setState({
                user_name:'',
                user_email:'',
                user_mobile:'',
            })

        }
   */     this.state = {
     //       isLoading: loading_status,
            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,

       //     mobile_number:mobile_number,
            user_name:'',
            user_email:'',
            user_mobile:'',
            rewardPoints:'',
            id:null,
            direct_login:"1",
            alert_modal_value:true,
            loginModalVisible:false,
            otpModalVisible:false,
            signupModalVisible:false,
            // mobileNo: mobile_number,
            ErrorStatus_mobile: true,
            empty_phone_no:false,
            ActivityIndicator_Loading:false,
            empty_username:false,
            ErrorStatus_mail: true,
            ErrorStatus_username: true,
            ErrorStatus_password: true,
            ErrorStatus_name: true,
            phone: '',
            password: '',
            username: '',
            email: '',
            signupActivityIndicator:false,
            blankmodal:false



        };
    }
    /*==============================
    * Edit Profile Open
    * ==============================*/
    Function_togoto_EditActivity = (user_name,user_mobile,user_email,user_id) => {



        this.props.navigation.navigate('EditProfile',{user_name,user_mobile,user_email,user_id});
    }


    /*===============================
    * Maintain Login Modal
    * ===============================*/

    openLoginModal=()=>{
        this,this.setState({
            loginModalVisible:true,
            blankmodal:false,
        })
    }

    closeLoginModal=()=>{
        this.setState({
            loginModalVisible:false
        })
    }
    /*===============================
    * Maintain OTP Modal
    * ===============================*/

    openotpModal=()=>{
        this.setState({
            otpModalVisible:true
        })
    }

    closeotpModal=()=>{
        this.setState({
            otpModalVisible:false
        })
    }

    /*===============================
    * Maintain SIGNUP Modal
    * ===============================*/

    opensignupModal=()=>{
        this.setState({
            signupModalVisible:true
        })
    }

    closesignupModal=()=>{
        this.setState({
            signupModalVisible:false
        })
    }

    /*=================================
    * Genrate User Details
    * =================================*/
    refreshPage=()=>{

        console.log("this.state.mobile_number ==== "+this.state.mobile_number);
        console.warn("this.state.mobile_number ==== "+this.state.mobile_number);
        return fetch('http://3.13.212.113:3000/user/details',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },

                body: JSON.stringify(
                    {
                        mobile_no:this.state.mobile_number,

                    })

            }).then((response) => response.json()).then((response) =>
        {
            console.warn("Under user create API");
            this.setState({
                id:response.id,
                user_name:response.name,
                user_email:response.email,
                user_mobile:response.mobile_no,
                rewardPoints:response.rewardPoints,
                isLoading: false,
            });

            // console.log("The Data is = " + JSON.stringify(this.state.data).toString());
            //console.log("The weightPrice is = " + JSON.stringify(this.state.weightPrice).toString());

        }).catch((error) =>
        {
            console.warn("Error Under user create API");
            console.error(error);
            this.setState({
                isLoading: false,
            });

        });

    }


    /*==============================
    * SIgnup Mainatin Code
    * ==============================*/

    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(text) === false)
        {
            console.log("Email is Not Correct");
            this.setState({email:text})
            return false;
        }
        else {
            console.log("Email is Correct");
            this.setState({email : text, ErrorStatus_mail : true}) ;

            this.onEnteremail(text);
        }
    }
    onEnterusername = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({username : TextInputValues, ErrorStatus_username : false,empty_username:false,
            }) ;
        }else{
            this.setState({username : TextInputValues, ErrorStatus_username : true}) ;
        }
    }

    onEnteremail = (TextInputValues) =>{

        if(TextInputValues.trim() != 0){
            this.setState({email : TextInputValues, ErrorStatus_mail : false,empty_email:false,}) ;
        }

        else{

            this.setState({email : TextInputValues, ErrorStatus_mail : true}) ;
        }
    }

    validate_mobileno=(mobno)=>{
        if(mobno.length!=10)
        {
            console.log("Length Should be 10");
        }
        else
        {
            this.onEnterMobileno(mobno);
        }
    }

    /*========================
    * User Creation
    * ========================*/

    createUser = () => {
        this.setState({signupActivityIndicator:true})
        return fetch('http://3.13.212.113:3000/user/create',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        userdetails: {
                            name: this.state.username,
                            mobile: this.state.mobile_number,
                            email: this.state.email,
                            roleId: 1,

                        }
                    })

            }).then((response) => response.json()).then((response) =>
        {

            if(response.status!=false)
            {
                Toast.show(response.message, Toast.SHORT);
                console.warn("3::"+response.message);
                this.setState({signupActivityIndicator:false});
                this.closesignupModal();
                this.Function_TO_RequestOTP(this.state.mobile_number);
                this.openotpModal();

            }
            else
            {
                this.setState({signupActivityIndicator:false});
                Toast.show("User Not Created Successfully", Toast.SHORT);
            }
            // console.log("The Data is = " + JSON.stringify(this.state.data).toString());
            //console.log("The weightPrice is = " + JSON.stringify(this.state.weightPrice).toString());

        }).catch((error) =>
        {
            console.error(error);

        });

    }


    /*===============================
    * Save User Details to server
    * ===============================*/
    Function_to_open_LoginScreen = () => {


        if(this.state.ErrorStatus_username===true && this.state.ErrorStatus_mail==true && this.state.ErrorStatus_mobile==true)
        {
            this.setState({
                empty_username:true,
                empty_email:true,
                empty_phone_no:true,
            })
        }

        else  if(this.state.ErrorStatus_username===true)
        {
            this.setState({
                empty_username:true,
            })
        }
        else if(this.state.ErrorStatus_mail==true)
        {            realm = new Realm({ path: 'CakeDatabase.realm' });

            this.setState({
                empty_email:true,
            })
        }
        else if(this.state.ErrorStatus_mobile==true)
        {
            this.setState({
                empty_phone_no:true,
            })
        }
        else
        {
            /*  // Call User Created Function
              realm = new Realm({ path: 'CakeDatabase.realm' });

              realm.write(() => {

                  realm.create('User_Details', {
                      mobile_number:this.state.mobile_number,
                      login_value:1

                  });

              });
              console.warn("The Value in User Details Table are as follows"+JSON.stringify(realm.objects('User_Details')));
            */  this.createUser();
        }



    }


    /*==========================
    * ComponentDidMount
    * ==========================*/
    componentDidMount()
    {

        console.warn("line 363");
        try {
            console.warn("Under Try Block");
            realm = new Realm({ path: 'CakeDatabase.realm'});
            var User_Details = realm.objects('User_Details');
            var mobile_number=User_Details[0].mobile_number;
            console.warn("Under Try Block mobile_number ==== "+ mobile_number);
            var loading_status =true;
            this.setState({
                isLoading: loading_status,
                 mobile_number:mobile_number,
            })
            console.warn("Under Try Block mobile_number 2:: ==== "+ this.state.mobile_number);
        }
        catch (e) {

            var mobile_number='';
            var loading_status =false;
            this.setState({
                user_name:'',
                user_email:'',
                user_mobile:'',
                isLoading: loading_status,
                mobile_number:mobile_number,
            })
            console.warn("Under Catch Block mobile_number ==== "+ this.state.mobile_number);
        }

        console.warn("line 385");
        console.warn("this.props.navigation.state.params"+this.props.navigation.state.params);
        if(!this.state.mobile_number!= ' ' ||this.state.mobile_number!= null ||this.state.mobile_number!= undefined )
        {
            this.setState({
                blankmodal:true,
            })
        }
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                console.warn("Page Refresh");
                console.warn("this.props.navigation.state.params==="+this.props.navigation.state.params);
                console.warn("under will focus subsription ==== "+ this.state.mobile_no);
                if(this.state.mobile_number!= ' '||this.state.mobile_number!= null ||this.state.mobile_number!= undefined)
                {
                    this.refreshPage ();
                }


            }
        );
        // alert("The Correct Mobile Number is = "+ this.state.mobile_number);
        if(this.state.mobile_number!= ' '||this.state.mobile_number!= null ||this.state.mobile_number!= undefined)
        {
            this.refreshPage ();
        }


    }

    /*=========================
    * Demo Function
    *=========================*/


    myorder = (user_id) => {
        this.props.navigation.navigate('MyOrder',{user_id});
    }
    setPaymentModalVisible(visible) {

        this.setState({paymentmodal: visible});
    }


    /*===============================
    * Logout Function
    * ==============================*/
    openAlertMessage=()=>{
        Alert.alert(
            'Logout',
            'Are You Sure You want to logout from App. ',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
        );
    }


    /*=======================================
    * Maintain All Login Function here
    * =======================================*/
    onEnterMobileno = (TextInputValues) =>{

        if(TextInputValues.trim() != 0){
            this.setState({mobile_number : TextInputValues, ErrorStatus_mobile : false,empty_phone_no:false}) ;
        }else{
            this.setState({mobile_number : TextInputValues, ErrorStatus_mobile : true}) ;
        }
    }

    /*=======================================
    *Method Send Mobile Number to Server
    * =======================================*/

    Function_TO_RequestOTP = (mobile_number) =>
    {
        // this.props.navigation.navigate('Mycart');

        if(this.state.mobile_number!='')
        {

            console.log("Mobile Number Screen Called");
            console.log("Mobile Number 70 Send OTP Called ");
            this.setState({ActivityIndicator_Loading: true}, () => {
                console.log("Personal Details 214 Send OTP Called ");
                fetch('http://3.13.212.113:3000/message/sendotp',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                mobile_no: this.state.mobile_number,

                            })

                    }).then((response) => response.json()).then((response) => {


                    if (response.status != 'Fail') {
                        Toast.show(response.message, Toast.SHORT);
                        console.warn("1::"+response.message);
                        this.setState({
                            ActivityIndicator_Loading: false,
                        });
                        this.closeLoginModal();
                        this.openotpModal();
                    } else {
                        Toast.show(response.message, Toast.SHORT);
                        this.setState({ActivityIndicator_Loading: false});
                        console.warn("2::"+response.message);
                        this.closeLoginModal();
                        this.opensignupModal();

                    }


                }).catch((error) => {
                    console.error(error);

                    this.setState({ActivityIndicator_Loading: false});
                });
            });
        }

        else
        {
            this.setState({
                empty_phone_no:true,
            })
        }
    }


    /*==============================
    * Maintain OTP Code
    * ==============================*/

    _onFulfill(code) {
        // TODO: call API to check code here
        // If code does not match, clear input with: this.refs.codeInputRef1.clear()
        if(code.length<0)
        {
            Toast.show('OTP Should Not be Blank', Toast.SHORT);
        }
        else {

            this.setState({otp_ActivityIndicator_Loading: true}, () => {
                fetch('http://3.13.212.113:3000/message/verifyotp',
                    {
                        method: 'POST',
                        headers:
                            {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        body: JSON.stringify(
                            {
                                mobile_no: this.state.mobile_number,
                                otp: code,

                            })

                    }).then((response) => response.json()).then((response) => {


                    if (response.message != 'invalid otp') {

                        Snackbar.show({
                            title: 'Congrats,User Crate Successfully',
                            duration: Snackbar.LENGTH_INDEFINITE,
                            action: {
                                title: 'UNDO',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                            },
                        });
                        //this.getuserDetails();
                        // Call User Created Function
                        realm = new Realm({ path: 'CakeDatabase.realm' });

                        realm.write(() => {

                            realm.create('User_Details', {
                                mobile_number:this.state.mobile_number,
                                login_value:1

                            });

                        });
                        console.warn("The Value in User Details Table are as follows"+JSON.stringify(realm.objects('User_Details')));

                        this.closeotpModal();
                        this.refreshPage();

                    } else {
                        Toast.show('Please Enter Correct OTP', Toast.SHORT);

                    }

                    this.setState({otp_ActivityIndicator_Loading: false});

                }).catch((error) => {
                    console.error(error);

                    this.setState({otp_ActivityIndicator_Loading: false});
                });
            });
        }
    }
    /*Function TO CHeck OTP Code*/
    Insert_Data_Into_Verifyotp = (otpno) =>
    {
        if(otpno != '' || otpno != undefined ||otpno != null)
        {
            Toast.show('OTP Should Not be Blank', Toast.SHORT);
        }

    }

    /*============================
    * UI Started
    * ============================*/

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size={'large'} color={'#000'}/>
                    <Text style={{color: '#000'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#FCECF4'}}>

                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />

                    </Left>


                    <Body style={{padding:5,}}>
                    <Title style={{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'12%',color:'#fff',fontWeight:'bold',}}>
                        My Account</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>

                    {/*
                            {this.state.mobile_number!= '' ?
                               null :

                                <View style={{borderRadius:10,padding:5,backgroundColor:'rgba(0,0,0,0.37)',marginTop:'2%',marginBottom:'2%',
                                    marginRight:'2%',marginLeft:'2%'}}>
                                    <View style={{alignItems:'flex-end',justifyContent:'flex-end',paddingRight:0,paddingBotom:5,}}>
                                        <TouchableOpacity onPress={()=>alert('data called')}>
                                            <Icon type="EvilIcons" name="close-o"
                                                  style={{paddingRight:'5%',color:'#000', fontSize:24,paddingTop:'1%'}}/>
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={{color:'#000',fontSize:14,fontWeight:'bold',textAlign:'center'}}>You are not authorised user. please login to get better features</Text>
                                    <Text style={{color:'green',textAlign:'right',alignItems:'flex-end',fontWeight:'bold',fontSize:16,marginRight:'1%'}}
                                          onPress={()=>this.openLoginModal()}  >
                                        Login
                                    </Text>
                                </View>
                            }*/}

                    {/*Main Header Title*/}
                    <View style={{backgroundColor:'#D977AB',marginBottom:'3%'}}>
                        <View style={{alignItems:'flex-end',justifyContent:'flex-end',paddingRight:0,paddingBotom:5,}}>
                            <TouchableOpacity onPress={()=>this.Function_togoto_EditActivity(this.state.user_name,this.state.user_mobile,this.state.user_email,this.state.id)}>
                                <Icon type="Entypo" name="edit"
                                      style={{paddingRight:'5%',color:'#fff', fontSize:18,paddingTop:'1%'}}/>
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection:'column',paddingTop:0,paddingBottom:0,alignItems:'center',justifyContent:'center',}}>

                            <Image source={require('../../images/bekery_user.png')}
                                   style={{width: 95, height: 95, borderRadius: 95/2,}} />

                            <Text style={[styles.client_user_name,{paddingBottom:3,fontWeight:'bold'}]}>{this.state.user_name}</Text>
                            <Text style={[styles.client_user_name,{fontSize:14,marginTop:'3%',}]}> {this.state.user_mobile}</Text>

                            <Text style={[styles.client_user_name,{fontSize:14,marginTop:'2%',marginBottom:'1%'}]}>{this.state.user_email}</Text>

                        </View>
                    </View>

                    {/*Options List */}

                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                        <TouchableOpacity onPress={()=>this.myorder(this.state.id)}>

                            <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>My Orders</Text>
                                <View style={styles.list_order_seperator}/>
                                <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}>View All Orders</Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('RewardScreen')}>
                            <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Refer Points</Text>
                                <View style={styles.list_order_seperator}/>
                                <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}>View All Refer Point</Text>
                            </View>
                        </TouchableOpacity>
                    </View>



                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                        <TouchableOpacity onPress={()=>this.openAlertMessage()}>
                            <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Logout</Text>

                            </View>
                        </TouchableOpacity>
                    </View>

                </Content>

                {/*Login Modal*/}
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.loginModalVisible}>
                    <View style={{paddingTop:22,backgroundColor:'#FCECF4',flex:1,height:'100%'}}>
                        <View>
                            <View style={styles.image_container}>
                                <Image
                                    source={require('../../images/logo_app.png')}
                                    style={styles.logo}
                                />
                            </View>

                            <Text style={styles.text_name}>Enter Mobile Number</Text>
                            <View style={this.state.empty_phone_no?styles.error_SectionStyle:styles.SectionStyle}>

                                <TextInput
                                    ref='input_mobileno'
                                    maxLength={10}
                                    editable={true}
                                    style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                    placeholder="Enter Mobile Number"
                                    keyboardType = 'numeric'
                                    autoFocus={true}
                                    underlineColorAndroid="transparent"
                                    onChangeText = {(TextInputText) =>  this.onEnterMobileno(TextInputText)} />

                            </View>
                            {this.state.empty_phone_no?
                                <Text style={styles.error_message}>Phone Number is Mandetory</Text>:null}

                        </View>

                        <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,
                            bottom:-10,justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                 onPress={()=>this.Function_TO_RequestOTP(this.state.mobile_number)}>


                            {
                                this.state.ActivityIndicator_Loading ? <ActivityIndicator color='#fff' size='large'/> :

                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 14,
                                        fontWeight: 'bold',
                                        fontFamily: 'Calibri Bold',
                                    }}
                                          onPress={() => this.Function_TO_RequestOTP(this.state.mobile_number)}>Continue </Text>
                            }
                        </Button>
                    </View>

                </Modal>

                {/*OTP Modal*/}

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.otpModalVisible}>
                <View style={{paddingTop:22,backgroundColor:'#FCECF4',flex:1,height:'100%'}}>
                        <View>
                            <View style={styles.image_container}>
                                <Image
                                    source={require('../../images/logo_app.png')}
                                    style={styles.logo}
                                />
                            </View>

                            <Text style={styles.otp_display_text}>Enter OTP send to your Register Number.  </Text>
                            <CodeInput
                                ref="codeInputRef2"
                                codeLength={4}
                                secureTextEntry
                                className={'border-circle'}
                                activeColor='#E1A7C5'
                                inactiveColor='rgba(0,0,0, 1.3)'
                                ignoreCase={true}
                                inputPosition='center'
                                size={40}
                                keyboardType="numeric"
                                autoFocus={true}
                                containerStyle={{ marginTop: 30,marginBottom:100}}
                                codeInputStyle={{ borderWidth: 1.5,fontSize:14 }}
                                onFulfill={(code) => this._onFulfill(code)}/>

                            {

                                this.state.otp_ActivityIndicator_Loading ? <ActivityIndicator color='#009688' size='large'style={styles.ActivityIndicatorStyle} /> : null

                            }

                            <Button  style={{backgroundColor:'#333333',width:'90%',padding:10,bottom:0,
                                justifyContent:'center',alignItems:'center',textAlign:'center',marginLeft:'10%',marginRight:'10%'}}
                                     onPress={()=> this.Insert_Data_Into_Verifyotp()}>
                                <Text style={{color:'#fff',fontSize:14,fontFamily:'Calibri Bold',}}
                                      onPress={()=> this.Insert_Data_Into_Verifyotp()}  >Continue</Text>
                            </Button>

                        </View>
                    </View>
                </Modal>


                {/*Signup Code*/}


                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.signupModalVisible}>
                    <View style={{paddingTop:22,backgroundColor:'#FCECF4',flex:1,height:'100%'}}>
                        <View>
                            <ScrollView>
                                <View style={styles.image_container}>
                                    <Image
                                        source={require('../../images/logo_app.png')}
                                        style={styles.logo}
                                    />
                                </View>

                                <Text style={styles.otp_display_text}>Choose Type of Personal Account </Text>

                                <Text style={styles.displays_text}>Enter Username <Text style={[styles.displays_text,{color:'red',paddingLeft:5}]}>*</Text></Text>

                                <View style={this.state.empty_username?[styles.error_SectionStyle,{marginBottom:10}]:[styles.SectionStyle,{marginBottom:10}]}>
                                    <TextInput
                                        style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                        placeholder="Enter Username "
                                        underlineColorAndroid="transparent"
                                        returnKeyType = {"next"}
                                        autoFocus = {false}
                                        keyboardType='email-address'
                                        onChangeText = {(TextInputText) =>  this.onEnterusername(TextInputText)}
                                        onSubmitEditing={(event) => {
                                            this.refs.email.focus();

                                        }}/>


                                </View>

                                {this.state.empty_username?
                                    <Text style={styles.error_message}>Username is Mandetory</Text>:null}


                                <Text style={styles.displays_text}>Enter Email <Text style={[styles.displays_text,{color:'red',paddingLeft:5}]}>*</Text></Text>

                                <View style={this.state.empty_username?[styles.error_SectionStyle,{marginBottom:10}]:[styles.SectionStyle,{marginBottom:10}]}>

                                    <TextInput
                                        ref='email'
                                        style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                        placeholder="Enter  Email "
                                        underlineColorAndroid="transparent"
                                        returnKeyType = {"next"}
                                        keyboardType='email-address'
                                        onChangeText = {(TextInputText) => { this.validate(TextInputText)}}
                                        onSubmitEditing={(event) => {
                                            this.refs.phone.focus();

                                        }}/>

                                </View>

                                {this.state.empty_email?
                                    <Text style={styles.error_message}>Email is Mandetory</Text>:null}


                                <Text style={styles.displays_text}>Enter Phone Number <Text style={[styles.displays_text,{color:'red',paddingLeft:5}]}>*</Text></Text>

                                <View style={this.state.empty_username?[styles.error_SectionStyle,{marginBottom:10}]:[styles.SectionStyle,{marginBottom:10}]}>

                                    <TextInput
                                        ref='phone'
                                        style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                        placeholder={this.state.mobile_number}
                                        underlineColorAndroid="transparent"
                                        maxLength={10}
                                        editable={false}
                                        returnKeyType = {"done"}
                                        keyboardType='numeric'
                                        onChangeText = {(TextInputText) =>  this.validate_mobileno(TextInputText)}/>

                                </View>
                                {this.state.empty_phone_no?
                                    <Text style={styles.error_message}>Phone Number is Mandetory</Text>:null}


                            </ScrollView>

                            <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                                justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                     onPress={()=>this.Function_to_open_LoginScreen()}>
                                <Text style={{color:'#fff',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                      onPress={()=>this.Function_to_open_LoginScreen()}  >Register</Text>
                            </Button>

                        </View>
                    </View>
                </Modal>


                {/*Modal open when mobile number is blank*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.blankmodal}>
                    <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center',
                        backgroundColor:'rgba(0, 0, 0, 0.7)'}}>
                        <Card style={{width:'80%'}}>
                            <CardItem header>
                                <Text style={{color:'green',fontSize:14,fontWeight:'bold',paddingLeft:10}}>Conformation Message</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Text style={{color:'#666',fontSize:14,fontWeight:'bold',}}>
                                    You are not Authorised User pLease Sign In for Further Process.
                                </Text>
                                </Body>
                            </CardItem>
                            <CardItem footer style={{alignItems:'center',marginLeft:'40%'}}>
                                <Text style={{color:'green',fontSize:16,fontWeight:'bold',paddingLeft:10,textAlign:'center'}}
                                      onPress={()=>this.openLoginModal()}>
                                    Continue
                                </Text>
                            </CardItem>
                        </Card>
                    </View>

                </Modal>
            </Container>
        );
    }
}

