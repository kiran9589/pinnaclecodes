import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert, StatusBar, FlatList,
} from 'react-native';
import { Container, Header, Item, Input, Icon,Text, Button,Right,Content,Body,Title,Left } from 'native-base';
import  Rating from 'react-native-easy-rating';

import styles from './myorder.style';
import MyOrderDetails from "./MyOrderDetails";

export default class RewardScreen extends Component<> {

    constructor(props) {

        super(props);

        this.state = {
            mobileNo: '',
            userID: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,
            start:0,
            page_count:1,
            totalCount:0,
            data:[],
            isLoading: false,
            refreshing:false,
        };
    }
    writereview = () => {
        this.props.navigation.navigate('WriteReview');
    }

    pressback=()=>{
        this.props.navigation.navigate('MyDrawerNavigator');
    }

    /*=====================
    * UI Started
    * =====================*/
    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#000'}/>
                    <Text style={{color:'#000'}}>Please Wait .....</Text>
                </View>
            );
        }     return (
            <Container style={{backgroundColor: '#FCECF4',}}>
                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent onPress={() => this.pressback()} style={{top:10}}>
                            <Icon name='arrow-back'/>
                        </Button>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height: 60, width: 100,}}/>
                    </Left>

                    <Body>
                    <Title style={styles.main_salon_name}>My Orders</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding: 10}}>

                    <View style={{justifyContent:'center',alignItems:'center',textAlign:'center'}}>

                        <Text style={{textAlign:'center',color:'#000',fontSize:18,fontWeight:'bold'}}>
                            Data Will Be Comming Soon.....
                        </Text>

                    </View>


                    {/*


                    <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff'}}>
                            <View style={{flexDirection:'row',width:'100%'}}>
                                <View style={{padding:5}}>
                                    <Image source={require('../../images/image_one.png')}
                                           style={{width:80,height:80,}}/>
                                </View>
                                <View style={{flexDirection:'column',width:'76%',paddingLeft:'3%'}}>
                                    <View style={styles.status_right}>
                                        <Text style={[styles.status_right_text,]} >Completed</Text>
                                    </View>

                                    <Text style={styles.cake_name}>Cake 1 </Text>
                                    <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:0}}>
                                        <Icon type="FontAwesome" name="rupee"
                                              style={{fontSize: 18, color: '#E9C789',paddingTop:'1%',paddingRight:'2%'}}/>
                                        <Text style={styles.cake_price}>500.00</Text>


                                        <View style={{flexDirection:'row',paddingLeft:'20%',}}>
                                            <Icon type="FontAwesome5" name="weight"
                                                  style={{fontSize: 18, color: '#F4C9C7',paddingTop:'1%',paddingRight:'2%'}}/>
                                            <Text style={styles.cake_price}>1 Kg.</Text>

                                        </View>
                                    </View>

                                    <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:0}}>
                                        <Rating
                                            rating={2}
                                            max={5}
                                            editable={false}
                                            iconWidth={20}
                                            iconHeight={20}
                                            iconSelected={require('../../images/star_rate.png')}
                                            onRate={(rating) => this.setState({rating: rating})}/>

                                        <View style={{flexDirection:'row',paddingLeft:'20%',}} onPress={()=>this.writereview()}>

                                            <Text style={[styles.cake_price,{fontSize:14,marginTop:-4,color:'#000'}]}
                                                  onPress={()=>this.writereview()}>Write Review</Text>

                                        </View>
                                    </View>

                                    <View style={{flexDirection:'row',paddingBottom:'2%',paddingTop:0}}>
                                        <Icon type="MaterialCommunityIcons" name="bike"
                                              style={{fontSize: 18, color: '#87CEC7',paddingTop:'1%',paddingRight:'2%'}}/>
                                        <Text style={styles.cake_price}>15 min - 20 min</Text>
                                    </View>

                                </View>
                            </View>
                    </View>
*/}

                </Content>
            </Container>
        );
    }
}
