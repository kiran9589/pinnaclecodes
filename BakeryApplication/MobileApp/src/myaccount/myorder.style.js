export default {
    header_style:{backgroundColor:'#333333',padding:10,height:60},

    status_right : {
        justifyContent:'flex-end',
        alignItems:'flex-end',
        textAlign:'right',
        backgroundColor:'#17d90a',
        alignSelf: 'flex-end',

    },
    status_right_text : {
        fontSize:14,
        color:'#fff',
        paddingTop:1,
        fontWeight:'bold',
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        textAlign:'center',
        fontFamily:'Calibri Bold',

    },
    cake_name:{
        fontWeight:'bold',
        fontSize:16,
        color:'#282828',
        fontFamily:'Calibri Bold',
    },
    cake_desc:{
        fontWeight:'bold',
        fontSize:12,
        paddingTop:'2%',
        paddingBottom:'2%',
        color:'#8A8A8C',
        fontFamily:'Calibri Bold',
    },
    cake_price:{
        fontWeight:'bold',
        fontSize:12,
        paddingTop:'1%',
        paddingBottom:'2%',
        color:'#8A8A8C',
        fontFamily:'Calibri Bold',
    },
    underlineview:{

        backgroundColor:'#000',
        width:'100%',
        height:0.5,
        margin:10,
    },
    main_salon_name:{fontSize:16,fontFamily:'Calibri Bold',paddingTop:6,paddingLeft:'20%',color:'#fff'},

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:'30%',
    },
    empty_vart_text_one:{
        fontFamily:'Proxima Nova Reg',
    },


}