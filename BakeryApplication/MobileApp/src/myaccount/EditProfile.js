/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, StatusBar,
    Modal, TouchableWithoutFeedback, Alert, TextInput,PixelRatio,
} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button, Text,Right,Title, Subtitle,Body,Tab, Tabs,Left } from 'native-base';
import styles from './myaccount.style';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-simple-toast';


type Props = {};
export default class EditProfile extends Component<Props> {

    constructor(props) {

        super(props);

        this.state = {
            isLoading: true,
            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,
            ImageSource: null,

            full_name:this.props.navigation.state.params.user_name,
            new_full_name:this.props.navigation.state.params.user_name,
            email_id:this.props.navigation.state.params.user_email,
            mobile_number: this.props.navigation.state.params.user_mobile,
            new_email_id:this.props.navigation.state.params.user_email


        };
    }
    onEnterfullname = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({full_name : TextInputValues, ErrorStatus_full_name : false,}) ;
        }else{
            this.setState({full_name : TextInputValues, ErrorStatus_full_name : true}) ;
        }
    }
    onEnterEmail = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({email_id : TextInputValues, ErrorStatus_email : false,}) ;
        }else{
            this.setState({email_id : TextInputValues, ErrorStatus_email : true}) ;
        }
    }


/*=========================
* Update Details
* =========================*/

updateProfile=()=>{
    return fetch('http://3.13.212.113:3000/user/update',
        {
            method: 'POST',
            headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },

            body: JSON.stringify(
                {
                    name : this.state.full_name,  // mandatory
                    email: this.state.email_id,  // mandatory
                    id: this.props.navigation.state.params.user_id // mandatory

                })

        }).then((response) => response.json()).then((response) =>
    {
            if(response.status!='Success')
            {
                Toast.show('User details not updated successfully.', Toast.SHORT);

            }
            else {

                this.props.navigation.navigate('MyDrawerNavigator');
                Toast.show('User details has been updated successfully.', Toast.SHORT);


            }
    }).catch((error) =>
    {
        console.error(error);

    });

}
 /*===========================
 * Open Photo Choser
 * ===========================*/

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({

                    ImageSource: source

                });
            }
        });
    }


    render() {

        return (
            <Container style={{backgroundColor:'#FCECF4'}}>

                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>

                    </Left>


                    <Body style={{padding:5,}}>
                         <Title style={{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'12%',color:'#fff',fontWeight:'bold',}}>
                       Edit Profile</Title>
                    </Body>
                    {/*                    <Right>
                        <TouchableOpacity>
                            <Image source={require('../../images/white_logout.png')}
                                   style={{width:22,height:22,right:20,marginTop:2}}/>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Image source={require('../../images/black_cart.png')}
                                   style={{width:24,height:24,right:5,marginTop:2}}/>
                        </TouchableOpacity>
                    </Right>*/}

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>

                    {/*Main Header Title*/}
                    <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                        <View style={{flexDirection:'column',paddingTop:10,paddingBottom:0,alignItems:'center',justifyContent:'center',backgroundColor:'#D977AB',marginBottom:'2%'}}>

                            <Image source={require('../../images/bekery_user.png')}
                                   style={{width: 100, height: 100, borderRadius: 100/2,marginTop:'1%',marginBottom:'5%'}} />

                        </View>
                    </TouchableOpacity>

                    <Text style={[styles.client_user_name,{fontSize:14,marginTop:'3%',color:'#000',fontWeight:'bold'}]}> Full Name</Text>
                    <TextInput
                        ref='input_mobileno'
                        style={{flex:1,fontSize:14,paddingLeft:12,fontFamily:'Calibri Regular',fontWeight:'bold',color:'#000'}}
                        keyboardType = 'default'
                        underlineColorAndroid="transparent"
                        onChangeText = {(TextInputText) =>  this.onEnterfullname(TextInputText)}
                        value={this.state.full_name}/>

                    <View style={[styles.list_order_seperator,{marginTop:1}]}/>

                    <Text style={[styles.client_user_name,{fontSize:14,marginTop:'3%',color:'#000',fontWeight:'bold'}]}> Email ID</Text>
                    <TextInput
                        editable={true}
                        style={{flex:1,fontSize:14,paddingLeft:12,fontFamily:'Calibri Regular',fontWeight:'bold',color:'#000'}}
                        keyboardType = 'default'
                        underlineColorAndroid="transparent"
                        onChangeText = {(TextInputText) =>  this.onEnterEmail(TextInputText)}
                        value={this.state.email_id}/>



                    <View style={[styles.list_order_seperator,{marginTop:1}]}/>

                    <Text style={[styles.client_user_name,{fontSize:16,marginTop:'3%',color:'#D977AB',fontWeight:'bold',textAlign: 'center'}]}
                           onPress={()=>this.updateProfile()}>
                        Submit
                    </Text>

                    <Text style={[styles.client_user_name,{fontSize:14,marginTop:'3%',color:'#000',fontWeight:'bold'}]}> Mobile Number</Text>
                    <TextInput
                        maxLength={10}
                        editable={false}
                        style={{flex:1,fontSize:14,paddingLeft:12,fontFamily:'Calibri Regular',fontWeight:'bold',color:'#000'}}
                        placeholder={this.state.mobile_number}
                        keyboardType = 'default'
                        underlineColorAndroid="transparent"/>


                </Content>

            </Container>
        );
    }
}
