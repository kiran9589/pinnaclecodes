import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert, Text,StatusBar, FlatList,
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left } from 'native-base';
import  Rating from 'react-native-easy-rating';

import styles from './myorder.style';
import MyOrderDetails from "./MyOrderDetails";

export default class MyOrder extends Component<> {

    constructor(props) {

        super(props);

        this.state = {
            mobileNo: '',
            userID: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,
            start:0,
            page_count:1,
            totalCount:0,
            data:[],
            isLoading: true,
            refreshing:false,
        };
    }
    writereview = () => {
        this.props.navigation.navigate('WriteReview');
    }

pressback=()=>{
    this.props.navigation.navigate('MyDrawerNavigator');
}


/*==================================
* componentDidMount Method Start
* ==================================*/

componentDidMount(){

    this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {
            this.setState({ isLoading: true,start:0});
            this.fetchMyOrder();
        }
    );


//    this.fetchMyOrder();
}


/*======================
* My Order List
* ======================*/


fetchMyOrder=()=>
{
    return fetch('http://3.13.212.113:3000/order/list',
        {
            method: 'POST',
            headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },

            body: JSON.stringify(
                {
                    userId:this.props.navigation.state.params.user_id,
                    start: this.state.start,
                    limit: 10,

                })

        }).then((response) => response.json()).then((response) =>
    {

        var totalCount= response.totalCount;
        var orderlist= response.orders;

        this.setState({
            totalCount:totalCount,
            data:[...this.state.data , ...orderlist],
            isLoading: false,
            refreshing:false,

            //dataSource: response.data.items,
        });


    }).catch((error) =>
    {
        console.error(error);

    });

}

/*=====================
* FlatList Method
* =====================*/
    handleRefresh = () => {
        this.setState({

                start: 0,
                refreshing: true,
                data:[],
                page_count:1,
            },
            ()=>{
                this.fetchMyOrder();
            })
    }

    handleLoadMore = () =>{
        this.setState({
            start:this.state.start + 10,
            page_count:this.state.page_count+1,

        },()=>{
            var pagecount=this.state.totalCount/10;
            var total_page=Math.ceil(pagecount);

            if(this.state.page_count<=total_page)
            {
                this.fetchMyOrder();
            }
            else
            {
                console.log("Data Not Found");
            }

        })
    }


/*===========================
* Open Detail Page
* ===========================*/

openDetailPage=(order_id,user_id)=>{

        this.props.navigation.navigate('MyOrderDetails',{order_id,user_id})
}

/*=====================
* UI Started
* =====================*/
render() {
    if (this.state.isLoading) {
        return (
            <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator size={'large'} color={'#D977AB'}/>
                <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
            </View>
        );
    }     return (
            <Container style={{backgroundColor: '#FCECF4',}}>
                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent onPress={() => this.pressback()} style={{top:10}}>
                            <Icon name='arrow-back'/>
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                </Left>

                    <Body>
                         <Title style={styles.main_salon_name}>My Orders</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding: 10}}>

                    {this.state.data.length != 0 ?

                        <FlatList
                            data={this.state.data}
                            keyExtractor={(item, index) => index}
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.handleRefresh()}
                            onEndReached={() => this.handleLoadMore()}
                            renderItem={({item}) =>

                                <TouchableOpacity
                                    onPress={() => this.openDetailPage(item.orderId, this.props.navigation.state.params.user_id)}>
                                    <View style={{
                                        borderRadius: 5,
                                        height: 'auto',
                                        backgroundColor: '#fff',
                                        margin: 5,
                                        flexDirection: 'row'
                                    }}>

                                        {item.paymentStatus != "Success" ?
                                            <View style={{
                                                width: '25%',
                                                padding: 10,
                                                backgroundColor: '#EB3E52',
                                                justifyContent: 'center'
                                            }}>
                                                <Image source={require('../../images/attention.png')}
                                                       style={{
                                                           width: 50,
                                                           height: 50,
                                                           marginBottom: 10,
                                                           marginLeft: '12%'
                                                       }}/>
                                                <Text style={{
                                                    color: '#fff',
                                                    fontWeight: 'bold',
                                                    fontSize: 13,
                                                    textAlign: 'center'
                                                }}>
                                                    Failure
                                                </Text>

                                            </View>
                                            :

                                            <View style={{
                                                width: '25%',
                                                padding: 10,
                                                backgroundColor: '#4DAD4A',
                                                justifyContent: 'center'
                                            }}>
                                                <Image source={require('../../images/confirm.png')}
                                                       style={{
                                                           width: 50,
                                                           height: 50,
                                                           marginBottom: 10,
                                                           marginLeft: '12%'
                                                       }}/>
                                                <Text style={{
                                                    color: '#fff',
                                                    fontWeight: 'bold',
                                                    fontSize: 13,
                                                    textAlign: 'center'
                                                }}>
                                                    Success
                                                </Text>

                                            </View>
                                        }

                                        <View style={{width: '75%', paddingLeft: 10}}>

                                            <View style={styles.status_right}>
                                                <Text style={[styles.status_right_text,]}>{item.orderStatus}</Text>
                                            </View>
                                            <Text style={styles.cake_price}> <Text
                                                style={{fontWeight: 'bold', fontSize: 13}}>Transaction
                                                No</Text> {item.transactionNum}</Text>
                                            <Text style={styles.cake_price}> <Text
                                                style={{fontWeight: 'bold', fontSize: 13}}>Payment
                                                Mode</Text> {item.paymentMode}</Text>
                                            <Text style={styles.cake_price}> <Text style={{
                                                fontWeight: 'bold',
                                                fontSize: 13
                                            }}>Date</Text> {item.orderDate.substring(0, 10)}</Text>
                                            <Text style={styles.cake_price}> <Text
                                                style={{fontWeight: 'bold', fontSize: 13}}>Total
                                                Amount</Text> {item.totalAmount}</Text>

                                        </View>
                                    </View>

                                </TouchableOpacity>

                            }
                        /> :
                        <View style={styles.container}>
                            <Image
                                source={require('../../images/sad_smilee.png')}
                                style={{width: 100, height: 100}}/>

                            <Text style={[styles.empty_vart_text_one, {
                                fontSize: 22,
                                fontWeight: 'bold',
                                padding: 10,
                            }]}> No Order Available </Text>


                            <Button style={{
                                backgroundColor: '#CE3152',
                                width: '75%',
                                paddingTop: 0,
                                paddingBottom: 15,
                                bottom: 0,
                                marginLeft: '15%',
                                marginTop: '5%',
                                justifyContent: 'center',
                                alignItems: 'center',
                                textAlign: 'center',
                                height: 60
                            }}
                                    onPress={() => this.props.navigation.navigate('Category')}>

                                <Text style={{
                                    color: '#fff',
                                    fontSize: 14,
                                    fontWeight: 'bold',
                                    textAlign:'center',
                                    alignItems:'center',
                                    fontFamily: 'Calibri Bold',
                                }}
                                      onPress={() => this.props.navigation.navigate('Category')}>Order Now </Text>

                            </Button>


                        </View>
                    }

                        </Content>
            </Container>
        );
    }
}
