export default {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    service_count: {
        fontSize: 20,
        fontFamily:'Calibri Bold',
        textAlign: 'center',
        marginTop: 5,
    },
    edit_profile: {
        fontSize:12,
        padding:10,
        textAlign: 'center',
        color: '#333333',
        fontFamily:'Calibri Bold',

        marginBottom: 5,
    },
    display_text:{ fontSize: 14,
        fontFamily:'Calibri Bold',
        textAlign: 'center',
        marginTop:0,
    },
    client_user_name:{
        fontSize:24,
        fontFamily:'Calibri Bold',
        paddingLeft:10,
        //color:'#143F45',
        color:'#fff',

    },
    list_name_style:{
        fontSize:16,
        fontFamily:'Calibri Bold',
        paddingLeft:10,
        //color:'#143F45',
        color:'#000',
    },
    lineseperator:{
        width:'100%',
        backgroundColor:'gray',
        height:0.5,
        marginTop:5,
        marginBottom:5,
    },
    list_order_seperator:{
        width:'108%',
        backgroundColor:'#ccc',
        height:0.4,
        marginTop:'5%',
        marginBottom:'5%',
    },
    btn_service_name:{
        fontSize:14,
        fontFamily:'Calibri Bold',

    },
    myordertext:{

        width:'100%',
        marginTop:'5%',
        marginBottom:'3%',
        backgroundColor:'#333333',
        height:40,
        justifyContent:'center',
        alignItems:'center'
    },
    myorder_text:{
        color:'#fff',
        fontSize:16,
        paddingTop:10,
        marginBottom:'2%',
        paddingBottom:10,

        fontFamily:'Calibri Bold',
        textAlign:'center',
    },
    payment_modal_style:{
        flex: 1,
        width: '100%',
        height: 'auto',
        alignContent:'flex-start',
        justifyContent:'flex-start',
        alignItems:'flex-start',
        backgroundColor:'rgba(0,0,0,0.7)',
        padding:10,
        marginTop:'12%'

    },
    Payment_Card_Style:{
        position: 'absolute',
        width: '95%',
        marginTop:65,
        height: 'auto',
        marginLeft:10,
        marginRight:10,
        backgroundColor:'#fff',
    },
    payment_option_text:{
        fontSize:15,
        fontFamily:'Calibri Bold'

    },
    payment_main_text:{
        fontSize:16,
        fontFamily:'Calibri Bold',
        textAlign:'center',
        margin:5,
        padding:10,
    },
    main_text_view:{
        width:'100%',
        paddingTop:'5%',
        paddingBottom:10,
        paddingLeft:15,
        paddingRight:10,
        height:'auto',
    },
    account_text:{
      color:'#143F45',
        fontSize:20 ,
        fontWeight:'bold',
        alignItems:'center',

    },
    sub_text_Style:{

        padding:2,
        color:'#666',
        fontSize:12,
    },
    underline_view_new:{
      backgroundColor:'#333',
      width:'100%',
      height:0.5,
      marginTop:5,
      marginBottom:5,

    },
    divide_section:{
        backgroundColor:'#D977AB',
        width:'100%',
        height:5,
        marginTop:5,
        marginBottom:5,

    },

/*===============================
* Login Style
* ==============================*/
    SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: '#000', height: 40, borderRadius: 5 , marginTop:0,marginLeft:10,marginRight:10,
        marginBottom:450,paddingLeft:10,},
    error_SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: 'red', height: 40, borderRadius: 5 , marginTop:0,marginLeft:10,marginRight:10,
        marginBottom:450,paddingLeft:10,},
    number_SectionStyle:{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff',
        borderWidth: .5, borderColor: '#000', height: 40, borderRadius: 5 , marginTop:20,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:5,width:'18%'},
    input_number_SectionStyle:{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff',
        borderWidth: .5, borderColor: '#000', height: 40, borderRadius: 5 , marginTop:20,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:5,width:'65%'},
    ImageStyle: {padding: 5, margin: 5, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'},
    logo: {width: 120, height:120,},
    image_container: {justifyContent: 'center', alignItems: 'center',marginTop:20,marginBottom:10
    },
    error_message:{
        color:'red',
        paddingLeft:5,
        paddingRight:5,
        fontFamily:'Calibri Bold',
        paddingTop:3,
        paddingBottom:3,
        fontSize:14,
    },
    text_name:{fontSize:12,fontFamily:'Calbri -Bold',paddingTop:5,paddingLeft:10,paddingRight:5,paddingBottom:5,
        color:'gray'},
    no_error_text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#CE3152',},
    error_text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#CE3152',},


/*============================
* OTP Style
* ============================*/
    otp_display_text:{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:10,color:'#000sss',fontWeight:'bold',},

/*SIgnup Style*/
    displays_text:{
        color:'#000',
        paddingTop:10,
        paddingLeft:5,
        paddingRight:5,
        fontFamily:'Calibri Bold',
        padding:5,
        fontSize:14,
    },

}