import Toast from "react-native-simple-toast";

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
AccountWithoutLogin
import React, {Component} from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, StatusBar,ActivityIndicator,
    Modal, TouchableWithoutFeedback, Alert, ListView,Text,
} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button,Right,Title, Subtitle,Body,Tab, Tabs,Left } from 'native-base';
import styles from './myaccount.style';
import Snackbar from 'react-native-snackbar';

var Realm = require('realm');
let realm;


type Props = {};
export default class AccountWithoutLogin  extends Component<Props> {

    constructor(props) {

        super(props);


        this.state = {

            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,
            user_name:'',
            user_email:'',
            user_mobile:'',
            rewardPoints:'',
            id:null,
            //       isLoading: loading_status,
            //     mobile_number:mobile_number,
            direct_login:"1",
            alert_modal_value:true,
            loginModalVisible:false,
            otpModalVisible:false,
            signupModalVisible:false,
            // mobileNo: mobile_number,
            ErrorStatus_mobile: true,
            empty_phone_no:false,
            ActivityIndicator_Loading:false,
            empty_username:false,
            ErrorStatus_mail: true,
            ErrorStatus_username: true,
            ErrorStatus_password: true,
            ErrorStatus_name: true,
            phone: '',
            password: '',
            username: '',
            email: '',
            signupActivityIndicator:false,
            blankmodal:false,
            return_id:false,
            address_screen:false,
            isLogin:false,
            logoutmodal:false,

        };
    }
    Function_togoto_EditActivity = (user_name,user_mobile,user_email,user_id) => {

        this.props.navigation.navigate('EditProfile',{user_name,user_mobile,user_email,user_id});
    }


    myorder = (user_id,click_status) => {
        this.props.navigation.navigate('MyOrder',{user_id});
        this.setState({
            click_status:click_status,
        })
    }
    setPaymentModalVisible(visible) {

        this.setState({paymentmodal: visible});
    }

    openAlertMessage=()=>{
        Alert.alert(
            'Logout',
            'Are You Sure You want to logout. ',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => this.logoutFunction()},
            ],
            {cancelable: false},
        );
    }

/*===========================
* Logout Function
* ===========================*/
logoutFunction =() =>{
    realm.write(() => {

       /* realm.delete(realm.objects('Cart_Details'));
        realm.delete(realm.objects('User_Details'));*/
       realm.deleteAll();
        Toast.show('Logout Successfully', Toast.SHORT);
        this.props.navigation.navigate('MobileNo',{rotateID:4});
        this.setState({
            user_name:'',
            user_email:'',
            user_mobile:'',
            logoutmodal:false

        })

    });

}
/*=============================
* Load User Data
* =============================*/
 refreshPage=(mobile_number)=>{

        return fetch('http://3.13.212.113:3000/user/details',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },

                body: JSON.stringify(
                    {
                        mobile_no:mobile_number,

                    })

            }).then((response) => response.json()).then((response) =>
        {
            this.setState({
                id:response.id,
                user_name:response.name,
                user_email:response.email,
                user_mobile:response.mobile_no,
                rewardPoints:response.rewardPoints,
                isLoading: false,
            });
            this.getRewardPoint(response.id);
            this.getAddress(response.id);



        }).catch((error) =>
        {
            console.error(error);
            this.setState({
                isLoading: false,
            });

        });

 }

/*==============================
* ComponentDidMount
* ==============================*/
componentDidMount(){
    console.warn("172");
    this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {

            try {
                console.warn("178");
                realm = new Realm({ path: 'CakeDatabase.realm'});
                var User_Details = realm.objects('User_Details');
                var mobile_number=User_Details[0].mobile_number;
                var loading_status =true;
                this.setState({
                    blankmodal:false,
                    isLoading: loading_status,
                    mobile_number:mobile_number,
                    address_screen:false,

                })
                this.refreshPage (mobile_number);



            }
            catch (e) {
                console.warn("194");
                var mobile_number='';
                var loading_status =false;

            if(this.state.return_id != true)
            {
                console.warn("200");
                this.setState({
                    blankmodal:true,
                    user_name:'',
                    user_email:'',
                    user_mobile:'',
                    isLoading: loading_status,
                    mobile_number:mobile_number,
                    address_screen:false
                });
            }
            else {
                console.warn("211");
                this.setState({
                    blankmodal:false,
                    user_name:'',
                    user_email:'',
                    user_mobile:'',
                    isLoading: loading_status,
                    mobile_number:mobile_number,
                    return_id:false,
                    address_screen:true,
                });
            }

            }
        }
    );

  //  this.getAddress();
//    this.getRewardPoint();
}


/*=============================
* componentWillUnMount
* ============================*/
componentWillUnmount(){
    this.setState({
        return_id:false,
    })
}


    /*========================
    * Get Reward Point
    * ========================*/
getRewardPoint=(response_id)=>{
    fetch('http://3.13.212.113:3000/user/rewardPoints/'+response_id)
        .then((response) => response.json())
        .then((responseJson) => {
            var rewardPoint=responseJson.rewardPoints;
            this.setState({
                rewardPoint:rewardPoint,

            }, function(){

            });
        })
        .catch((error) =>{
            console.error(error);
        });
}
/*=============================
* Get User Address
* =============================*/

    getAddress = (response_id) => {
       // fetch('http://3.13.212.113:3000/user/address/' + user_id)
       return fetch('http://3.13.212.113:3000/user/address/'+response_id)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    address_data:responseJson
                }, function () {
                    this.setState({
                        array_length:this.state.address_data.length-1
                    })
                    this.state.address_data.map((item, index) => {
                        if (item.is_default == true) {
                            // this.setState({sel_id:item.id});
                            this.setState({
                                sel_index: index,
                                address_id: item.id,
                                user_address: item.address,
                                user_city: item.city,
                                user_state: item.state,
                                user_pincode: item.pincode.toString(),
                                total_len:this.state.address_data.length-1
                            });
                        }

                    });
                })
                    .catch((error) => {
                        console.error(error);
                    });
            });
    }
/*===========================
* Login Navigate
* ==========================*/
loginNavigate = () => {
    this.props.navigation.navigate('MobileNo',{rotateID:2});
}
    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#FCECF4'}}>

                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />

                    </Left>


                    <Body style={{padding:5,}}>
                        <Title style={{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'12%',color:'#fff',fontWeight:'bold',}}>
                            My Account</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>

                    {/*Main Header Title*/}
                    <View style={{backgroundColor:'#D977AB',marginBottom:'3%'}}>
                        {this.state.address_screen === true ? null :
                        <View style={{alignItems:'flex-end',justifyContent:'flex-end',paddingRight:0,paddingBotom:5,}}>
                            <TouchableOpacity onPress={()=>this.Function_togoto_EditActivity(this.state.user_name,this.state.user_mobile,this.state.user_email,this.state.id)}>
                                <Icon type="Entypo" name="edit"
                                      style={{paddingRight:'5%',color:'#fff', fontSize:18,paddingTop:'1%'}}/>
                            </TouchableOpacity>
                        </View> }

                        <View style={{flexDirection:'column',paddingTop:0,paddingBottom:0,alignItems:'center',justifyContent:'center',}}>

                            <Image source={require('../../images/bekery_user.png')}
                                   style={{width: 95, height: 95, borderRadius: 95/2,}} />

                            <Text style={[styles.client_user_name,{paddingBottom:3,fontWeight:'bold'}]}>{this.state.user_name}</Text>
                            <Text style={[styles.client_user_name,{fontSize:14,marginTop:'3%',}]}> {this.state.user_mobile}</Text>

                            <Text style={[styles.client_user_name,{fontSize:14,marginTop:'2%',marginBottom:'1%'}]}>{this.state.user_email}</Text>

                        </View>
                    </View>
                    {/*Options List */}
                    {this.state.address_screen === true ?


                        <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                            <TouchableOpacity onPress={()=>{ this.props.navigation.navigate('MobileNo',{rotateID:9})}}>
                                <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                    <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Login</Text>

                                </View>
                            </TouchableOpacity>
                        </View>


                        :
                        <View>
                            <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>

                                <TouchableOpacity onPress={()=> {this.myorder(this.state.id,"order_click"),
                                    this.setState({ return_id:true})}}>
                                    <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                        <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>My Orders</Text>
                                        <View style={styles.list_order_seperator}/>
                                        <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}>View All Orders</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>


                            <View style={{borderRadius:5,backgroundColor:'#fff',margin:5,height:  50}}>

                                <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                    <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Reward Points</Text>

                                    <Text style={{width:80,color:"#000",fontSize:14,fontWeight:'bold',
                                        textAlign:'right',marginLeft:'92%',top:-30,
                                        paddingTop:5,paddingBottom:5,}}>
                                        {this.state.rewardPoint}
                                    </Text>

                                </View>

                            </View>

                            <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                                <TouchableOpacity
                                    onPress={()=>this.props.navigation.navigate('GetAllAddress',{userID:this.state.id,screenID:1,return_id:true})}>
                                    <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                        <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>My Addresses</Text>

                                        <Text style={styles.list_name_style} > {this.state.user_address} {this.state.user_city} {this.state.user_state} {this.state.user_pincode}</Text>

                                        <View style={[styles.list_order_seperator,{height:1}]}/>

                                        {this.state.total_len === 0 ?
                                            <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}
                                                  onPress={()=>this.props.navigation.navigate('GetAllAddress',{userID:this.state.id,screenID:1})}>
                                                View Address
                                            </Text> :
                                            <Text style={[styles.list_name_style,{paddingBottom:5,color:'#D977AB',textAlign:'right',fontSize:14,fontWeight:'bold'}]}
                                                  onPress={()=>this.props.navigation.navigate('GetAllAddress',{userID:this.state.id,screenID:1})}>
                                                View {this.state.total_len} Address
                                            </Text>
                                        }
                                    </View>
                                </TouchableOpacity>

                            </View>

                            <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                                <TouchableOpacity onPress={()=>this.setState({logoutmodal:true})}>
                                    <View style={{flexDirection:'column',width:'90%',padding:10,}}>
                                        <Text style={[styles.list_name_style,{paddingBottom:5,fontWeight:'bold'}]}>Logout</Text>

                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View> }
                </Content>

                {/*Register Pop Up Modal*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.blankmodal}>
                    <View style={{ backgroundColor:'rgba(0, 0, 0, 0.8)',paddingTop:'10%',flex:1,
                        justifyContent:'center',alignItems:'center' }}>

                        <View style={{backgroundColor:'#fff',borderRadius:0,margin:20}}>
                            <Text style={{textAlign:'center',fontSize:15,paddingTop:20,paddingBottom:20,paddingLeft:'12%',paddingRight:'12%',
                                fontWeight:'bold',fontFamily:'Calibri Bold',}}>
                                You are not registered. Please Register to continue
                            </Text>
                            <View style={{flexDirection:'row'}}>

                                <View style={{flexDirection:'row',}}>

                                    <Button  style={{backgroundColor:'#E1A7C5',width:'50%',padding:10,bottom:0,
                                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>{ this.props.navigation.navigate('MobileNo',{rotateID:9}),
                                                 this.setState({blankmodal:false,return_id:true})}}>
                                        <Text style={{color:'#000',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>{ this.props.navigation.navigate('MobileNo',{rotateID:9}),
                                                  this.setState({blankmodal:false,return_id:true})}}>
                                            Register </Text>
                                    </Button>

                                    <Button  style={{backgroundColor:'#333333',width:'50%',padding:10,bottom:0, justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>this.setState({blankmodal:false,address_screen:true})}>

                                        <Text style={{color:'#fff',fontSize:12,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>this.setState({blankmodal:false,address_screen:true})}>Cancel</Text>

                                    </Button>
                                </View>



                            </View>
                        </View>
                    </View>

                </Modal>


                {/*Logout Pop Up Modal*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.logoutmodal}>
                    <View style={{ backgroundColor:'rgba(0, 0, 0, 0.8)',paddingTop:'10%',flex:1,
                        justifyContent:'center',alignItems:'center' }}>

                        <View style={{backgroundColor:'#fff',borderRadius:0,margin:20}}>
                            <Text style={{textAlign:'center',fontSize:15,paddingTop:20,paddingBottom:20,paddingLeft:'12%',paddingRight:'12%',
                                fontWeight:'bold',fontFamily:'Calibri Bold',}}>
                                Are You Sure You want to logout.
                            </Text>
                            <View style={{flexDirection:'row'}}>

                                <View style={{flexDirection:'row',}}>

                                    <Button  style={{backgroundColor:'#E1A7C5',width:'50%',padding:10,bottom:0,
                                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>{this.logoutFunction()}}>
                                        <Text style={{color:'#000',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>{this.logoutFunction() }}>
                                            Ok </Text>
                                    </Button>

                                    <Button  style={{backgroundColor:'#333333',width:'50%',padding:10,bottom:0, justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>this.setState({logoutmodal:false,})}>

                                        <Text style={{color:'#fff',fontSize:12,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>this.setState({logoutmodal:false,})}>Cancel</Text>

                                    </Button>
                                </View>



                            </View>
                        </View>
                    </View>

                </Modal>


            </Container>
        );
    }
}
