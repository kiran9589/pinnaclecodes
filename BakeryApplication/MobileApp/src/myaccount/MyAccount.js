/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, StyleSheet, View, TouchableOpacity, Image, StatusBar,
    Modal, TouchableWithoutFeedback, Alert
} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button, Text,Right,Title, Subtitle,Body,Tab, Tabs,Left } from 'native-base';
import styles from './myaccount.style';


type Props = {};
export default class MyAccount extends Component<Props> {

    constructor(props) {

        super(props);

        this.state = {
            isLoading: true,
            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,


        };
    }
    Function_togoto_EditActivity = () => {

        this.props.navigation.navigate('Edit_Profile');
    }
    myorder = () => {

        this.props.navigation.navigate('MyOrder');
    }
    setPaymentModalVisible(visible) {

        this.setState({paymentmodal: visible});
    }

    openAlertMessage=()=>{
        Alert.alert(
            'Logout',
            'Are You Sure You want to logout from App. ',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
        );
    }

    render() {
        return (
            <Container style={{backgroundColor:'#FCECF4'}}>

                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />

                    </Left>


                    <Body style={{padding:5,}}>
                   {/* <Title style={{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:10,color:'#fff',fontWeight:'bold',}}>
                        My Account</Title>*/}
                    </Body>
{/*                    <Right>
                        <TouchableOpacity>
                            <Image source={require('../../images/white_logout.png')}
                                   style={{width:22,height:22,right:20,marginTop:2}}/>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Image source={require('../../images/black_cart.png')}
                                   style={{width:24,height:24,right:5,marginTop:2}}/>
                        </TouchableOpacity>
                    </Right>*/}
                    
                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>
                    <View style={{flexDirection:'row',padding:10}}>
                            <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}}
                                   style={{width: 75, height: 75, borderRadius: 75/2,marginTop:'1%'}} />
                        <View style={{flexDirection:'column',marginTop:'10%'}}>
                            <Text style={[styles.client_user_name,{paddingBottom:10,fontWeight:'bold'}]}>Arjun Dalal</Text>
                            <Text style={[styles.client_user_name,{fontSize:16}]}>Mob No. : 8793694590</Text>

                        </View>

                        <TouchableOpacity onPress={()=>alert("Edit Will Comming Soon")}>
                        <Icon type="Entypo" name="edit"
                              style={{paddingRight:'10%',color:'#143F45', fontSize:18,marginTop:'15%',marginLeft:'30%'}}/>
                        </TouchableOpacity>

                    </View>
                    <Text style={[styles.client_user_name,{fontSize:16}]}>Address : Plot No 4, Rushikesh Appt. Trimurti Nagar , Nagpur</Text>
{/*
                            <Button bordered style={{borderColor:'#E1A7C5',marginTop:10,marginLeft:'15%',height:35,padding:5,width:'80%',
                                right:'10%',justifyContent:'center',alignItems:'center'}}
                                    onPress={()=>this.setPaymentModalVisible(true)}>
                                <Text style={styles.edit_profile}
                                      onPress={()=>this.setPaymentModalVisible(true)}>Edit Address</Text>
                            </Button>

                    <View style={styles.myordertext}>
                        <Text style={styles.myorder_text}>My Orders</Text>
                    </View>
*/}
                    <View style={styles.divide_section}/>
                    <TouchableOpacity onPress={()=>this.myorder()}>

                    <View style={styles.main_text_view}>
                        <Text style={styles.account_text}>
                            <Icon type="Foundation" name="dollar-bill"
                                  style={{paddingRight:'10%',color:'#143F45', fontSize:18}}/> My Order & Billing</Text>
                        <Text style={styles.sub_text_Style}>View your orders and billing details</Text>
                    </View>
                    </TouchableOpacity>

                    <View style={styles.underline_view_new}/>

                   {/* <View style={styles.main_text_view}>
                        <Text style={styles.account_text}>
                            <Icon type="MaterialCommunityIcons" name="face-profile"
                                  style={{paddingRight:'10%',color:'#143F45', fontSize:22}}/> Edit Profile</Text>
                        <Text style={styles.sub_text_Style}>Click Here to edit Profile</Text>
                    </View>
*/}


                  <TouchableOpacity onPress={()=>this.openAlertMessage()}>
                        <View style={styles.main_text_view}>
                            <Text style={styles.account_text}>
                                <Icon type="AntDesign" name="logout"
                                      style={{paddingRight:'10%',color:'#143F45', fontSize:22}}/> Logout</Text>
                            <Text style={styles.sub_text_Style}>Click Here to Logout from Profile</Text>
                        </View>
                  </TouchableOpacity>



                </Content>
                <Modal transparent = {true}
                       visible = {this.state.paymentmodal}
                       onRequestClose={() => this.setPaymentModalVisible(false)}
                       onBackdropPress={()=>this.setPaymentModalVisible(false)}>

                    <TouchableWithoutFeedback onPress={() => this.setPaymentModalVisible(false)}>
                        <View style={styles.payment_modal_style}>

                            <View
                                style={styles.Payment_Card_Style}>
                                <Text style={[styles.payment_main_text,{fontSize:18,color:'brown'}]}>Enter New Address </Text>
                                <Text style={styles.payment_main_text}>The Details address are shown here. this address are demo
                                        address all content are demo content.</Text>
                                <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                                    justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                         onPress={()=> this.setPaymentModalVisible(false)}>
                                    <Text style={{color:'#fff',fontSize:14,fontFamily:'Calibri Bold',}}
                                          onPress={()=> this.setPaymentModalVisible(false)} >Save Address</Text>
                                </Button>


                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

            </Container>
        );
    }
}
