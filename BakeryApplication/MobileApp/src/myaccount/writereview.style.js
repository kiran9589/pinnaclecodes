export default {
    client_user_name:{
        fontSize:20,
        fontFamily:'Calibri Bold',
        paddingLeft:10,
        color:'#143F45',
        bottom:20,
    },
    divide_section:{
        backgroundColor:'#000',
        width:'100%',
        height:2,
        marginTop:5,
        marginBottom:5,

    },
    textareaContainer:{
        backgroundColor: '#fff',
        borderWidth: .5,
        width:'90%',
        borderColor: '#000',
        height: 150,
        borderRadius: 5 ,
        marginTop:10,
        marginLeft:10,
        marginRight:10,
        marginBottom:5,
        paddingLeft:10,
    },
    textarea:{
        fontSize:14,
    },
    error_message:{
        color:'red',
        paddingLeft:10,
        paddingRight:5,
        fontFamily:'Calibri Bold',
        paddingTop:3,
        paddingBottom:3,
        fontSize:14,
        marginBottom:10,
    },

}