import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert, StatusBar, FlatList,
} from 'react-native';
import { Container, Header, Item, Input, Icon,Text, Button,Right,Content,Body,Title,Left } from 'native-base';
import  Rating from 'react-native-easy-rating';

import styles from './myorder.style';

export default class MyOrderDetails extends Component<> {

    constructor(props) {

        super(props);

        this.state = {
            mobileNo: '',
            userID: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,
            start:0,
            page_count:1,
            totalCount:0,
            data:[],
            isLoading: true,
            refreshing:false,
        };
    }
    writereview = (user_id,product_id,pro_img,pro_name) => {
        this.props.navigation.navigate('WriteReview',{user_id,product_id,pro_img,pro_name});
    }

    pressback=()=>{
        this.props.navigation.navigate('MyOrder');
    }


    /*==================================
    * componentDidMount Method Start
    * ==================================*/

    componentDidMount(){


        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                console.warn("Page 11 Refresh");
                this.setState({ isLoading: true,start:0});
                this.fetchMyOrder();
            }
        );

        }


    /*======================
    * My Order List
    * ======================*/


    fetchMyOrder=()=>
    {
        fetch('http://3.13.212.113:3000/order/'+this.props.navigation.state.params.order_id+'/'+this.props.navigation.state.params.user_id)
            .then((response) => response.json())
            .then((responseJson) => {
                var listData=responseJson;
                this.setState({
                    isLoading:false,
                    data: responseJson.products,
                    refreshing:false,

                }, function(){

                });
            })
            .catch((error) =>{
                console.error(error);
            });

    }

    /*=====================
    * FlatList Method
    * =====================*/
    handleRefresh = () => {
        this.setState({

                start: 0,
                refreshing: true,
                data:[],
                page_count:1,
            },
            ()=>{
                this.fetchMyOrder();
            })
    }

    handleLoadMore = () =>{
        this.setState({
            start:this.state.start + 10,
            page_count:this.state.page_count+1,

        },()=>{
            var pagecount=this.state.totalCount/10;
            var total_page=Math.ceil(pagecount);

            if(this.state.page_count<=total_page)
            {
                this.fetchMyOrder();
            }
            else
            {
                console.log("Data Not Found");
            }

        })
    }





    /*=====================
    * UI Started
    * =====================*/
    render() {
        if (this.state.isLoading) {
            return (

                    <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                        <ActivityIndicator size={'large'} color={'#D977AB'}/>
                        <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                    </View>
            );
        }
        return (
            <Container style={{backgroundColor: '#FCECF4',}}>
                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent onPress={() => this.pressback()} style={{top:10}}>
                            <Icon name='arrow-back'/>
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>

                    </Left>

                    <Body>
                    <Title style={styles.main_salon_name}>My Orders</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding: 10}}>



                    <FlatList
                        data={ this.state.data}
                        keyExtractor={(item, index) => index}
                        refreshing={this.state.refreshing}
                        onRefresh={()=> this.handleRefresh()}
                        onEndReached={()=>this.handleLoadMore()}
                        renderItem={({item}) =>

                            <View style={{
                                borderRadius: 5,
                                height: 'auto',
                                backgroundColor: '#fff',
                                margin: 5,
                                flexDirection: 'column'
                            }}>
                                <View style={{flexDirection: 'row'}}>
                                    <View style={{padding: 5}}>
                                        <Image source={{uri: 'http://3.13.212.113:3000' + item.imagePath}}
                                               style={{width: 80, height: 80,}}/>
                                    </View>
                                    <View style={{flexDirection: 'column', paddingLeft: 10, paddingTop: 5}}>
                                        <Text style={[styles.cake_name, {fontSize: 14,color:'#666'}]}
                                              numberOfLines={1}>{item.productName} </Text>
                                        <View style={{flexDirection: 'row',marginTop:3}}>
                                            <View style={{flexDirection: 'row', paddingTop: 0}}>
                                                <Icon type="FontAwesome" name="rupee"
                                                      style={{
                                                          fontSize: 18,
                                                          color: '#E9C789',
                                                          paddingTop: '1%',
                                                          paddingRight: '2%'
                                                      }}/>
                                                <Text style={styles.cake_price}>{item.price}</Text>


                                                <View style={{flexDirection: 'row', paddingLeft: '20%',}}>
                                                    <Icon type="FontAwesome5" name="weight"
                                                          style={{
                                                              fontSize: 18,
                                                              color: '#F4C9C7',
                                                              paddingTop: '1%',
                                                              paddingRight: '2%'
                                                          }}/>
                                                    <Text style={styles.cake_price}>{item.weight}</Text>

                                                </View>

                                            </View>
                                        </View>
                                        <Text style={[styles.cake_name, {fontSize: 14,color:'#666'}]} numberOfLines={1}>Delivery
                                            Date {item.deliveryDate.substring(0, 10)} </Text>

                                    </View>
                                </View>

                                <View style={{backgroundColor:'#666',width:'100%',height:0.8,marginTop:2,marginBottom:2}}/>
                                <View style={{flexDirection:'row',}}>

                                    <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:1,paddingLeft:'10%'}}>
                                        <Rating
                                            rating={2}
                                            max={5}
                                            editable={false}
                                            iconWidth={20}
                                            iconHeight={20}
                                            iconSelected={require('../../images/star_rate.png')}
                                            onRate={(rating) => this.setState({rating: rating})}/>

                                        {item.isAlreadyReview != false ? null :
                                            <View style={{flexDirection: 'row', paddingLeft: '35%',}}
                                                  onPress={() => this.writereview(this.props.navigation.state.params.user_id)}>

                                                <Text style={[styles.cake_price, {
                                                    fontSize: 14,
                                                    marginTop: -4,
                                                    color: '#000'
                                                }]}
                                                      onPress={() => this.writereview(this.props.navigation.state.params.user_id, item.producId, 'http://3.13.212.113:3000' + item.imagePath, item.productName)}>Write
                                                    Review</Text>

                                            </View>
                                        }
                                    </View>
                                </View>

                            </View>
                        }/>





















{/*

                            <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                                <View style={{flexDirection:'row',width:'100%'}}>
                                    <View style={{padding:5}}>
                                        <Image source={{uri:'http://3.13.212.113:3000'+item.imagePath}}
                                               style={{width:80,height:80,}}/>
                                    </View>
                                    <View style={{flexDirection:'column',width:'76%',paddingLeft:'3%'}}>
                                        <View style={styles.status_right}>
                                            <Text style={[styles.status_right_text,]} >{item.paymentStatus}</Text>
                                        </View>

                                        <Text style={styles.cake_name} numberOfLines={1}>{item.productName} </Text>
                                        <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:0}}>
                                            <Icon type="FontAwesome" name="rupee"
                                                  style={{fontSize: 18, color: '#E9C789',paddingTop:'1%',paddingRight:'2%'}}/>
                                            <Text style={styles.cake_price}>{item.price}</Text>


                                            <View style={{flexDirection:'row',paddingLeft:'20%',}}>
                                                <Icon type="FontAwesome5" name="weight"
                                                      style={{fontSize: 18, color: '#F4C9C7',paddingTop:'1%',paddingRight:'2%'}}/>
                                                <Text style={styles.cake_price}>{item.weight}</Text>

                                            </View>
                                        </View>


                                        <View style={{flexDirection:'row',paddingBottom:'2%',paddingTop:0}}>
                                        <Icon type="MaterialCommunityIcons" name="bike"
                                              style={{fontSize: 18, color: '#87CEC7',paddingTop:'1%',paddingRight:'2%'}}/>
                                        <Text style={styles.cake_price}>{item.quantity}</Text>
                                    </View>
                                        <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:1}}>
                                            <Rating
                                                rating={2}
                                                max={5}
                                                editable={false}
                                                iconWidth={20}
                                                iconHeight={20}
                                                iconSelected={require('../../images/star_rate.png')}
                                                onRate={(rating) => this.setState({rating: rating})}/>

                                            <View style={{flexDirection:'row',paddingLeft:'20%',}}
                                                  onPress={()=>this.writereview(this.props.navigation.state.params.user_id)}>

                                                <Text style={[styles.cake_price,{fontSize:14,marginTop:-4,color:'#000'}]}
                                                      onPress={()=>this.writereview(this.props.navigation.state.params.user_id,item.producId,'http://3.13.212.113:3000'+item.imagePath,item.productName)}>Write Review</Text>

                                            </View>
                                        </View>
                                    </View>
                                </View>


                            </View>
                        }
                    />
*/}




                   {/* <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff'}}>
                        <View style={{flexDirection:'row',width:'100%'}}>
                            <View style={{padding:5}}>
                                <Image source={require('../../images/image_one.png')}
                                       style={{width:80,height:80,}}/>
                            </View>
                            <View style={{flexDirection:'column',width:'76%',paddingLeft:'3%'}}>
                                <View style={styles.status_right}>
                                    <Text style={[styles.status_right_text,]} >Completed</Text>
                                </View>

                                <Text style={styles.cake_name}>Cake 1 </Text>
                                <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:0}}>
                                    <Icon type="FontAwesome" name="rupee"
                                          style={{fontSize: 18, color: '#E9C789',paddingTop:'1%',paddingRight:'2%'}}/>
                                    <Text style={styles.cake_price}>500.00</Text>


                                    <View style={{flexDirection:'row',paddingLeft:'20%',}}>
                                        <Icon type="FontAwesome5" name="weight"
                                              style={{fontSize: 18, color: '#F4C9C7',paddingTop:'1%',paddingRight:'2%'}}/>
                                        <Text style={styles.cake_price}>1 Kg.</Text>

                                    </View>
                                </View>

                                <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:0}}>
                                    <Rating
                                        rating={2}
                                        max={5}
                                        editable={false}
                                        iconWidth={20}
                                        iconHeight={20}
                                        iconSelected={require('../../images/star_rate.png')}
                                        onRate={(rating) => this.setState({rating: rating})}/>

                                    <View style={{flexDirection:'row',paddingLeft:'20%',}}
                                          onPress={()=>this.writereview(this.props.navigation.state.params.user_id)}>

                                        <Text style={[styles.cake_price,{fontSize:14,marginTop:-4,color:'#000'}]}
                                              onPress={()=>this.writereview(this.props.navigation.state.params.user_id)}>Write Review</Text>

                                    </View>
                                </View>*/}

                                {/*<View style={{flexDirection:'row',paddingBottom:'2%',paddingTop:0}}>
                                        <Icon type="MaterialCommunityIcons" name="bike"
                                              style={{fontSize: 18, color: '#87CEC7',paddingTop:'1%',paddingRight:'2%'}}/>
                                        <Text style={styles.cake_price}>15 min - 20 min</Text>
                                    </View>*/}

                            {/*</View>
                        </View>
                    </View>
*/}
                </Content>
            </Container>
        );
    }
}
