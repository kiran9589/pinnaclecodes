export default {
    header_style:
        {
            backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65
        },
    main_salon_name:
        {
            fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'20%',color:'#fff',fontWeight:'bold',
        },
    text_name:
        {
            fontSize:12,
            fontFamily:'Calibri Bold',
            padding:5,
            color:'gray'
        },
    no_error_text_name:
        {
            fontSize:12,
            fontFamily:'Calibri Bold',
            padding:5,
            color:'#CE3152',
        },
    error_text_name:
        {
            fontSize:12,
            fontFamily:'Calibri Bold',
            padding:5,
            color:'#CE3152',
        },
    SectionStyle:
        {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff',
            borderWidth: .5,
            borderColor: '#000',
            height: 40,
            borderRadius: 5 ,
            marginTop:5,
            marginLeft:10,
            marginRight:10,
            marginBottom:3,
            paddingLeft:10,

        },
    error_SectionStyle:
        {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff',
            borderWidth: .5,
            borderColor: 'red',
            height: 40,
            borderRadius: 5 ,
            marginTop:5,
            marginLeft:10,
            marginRight:10,
            marginBottom:3,
            paddingLeft:10,

        },
    number_SectionStyle:
        {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff',
            borderWidth: .5,
            borderColor: '#000',
            height: 30,
            borderRadius: 5 ,
            marginTop:20,
            marginLeft:10,
            marginRight:10,
            marginBottom:5,
            paddingLeft:5,
            width:'18%'
        },
    input_number_SectionStyle:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#000',
        height: 30,
        borderRadius: 5 ,
        marginTop:20,
        marginLeft:10,
        marginRight:10,
        marginBottom:5,
        paddingLeft:5,
        width:'65%'
    },
    ImageStyle:
        {
            padding: 5,
            margin: 5,
            height: 25,
            width: 25,
            resizeMode : 'stretch',
            alignItems: 'center'
        },
    logo:
        {
            width: 200,
            height:80,
        },
    image_container:
        {
            justifyContent: 'center',
            alignItems: 'center',
            marginTop:10,
        },
    otp_display_text:
        {
            fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:10,color:'#000',fontWeight:'bold',paddingBottom:10,textAlign:'center',
        },
    displays_text:{
      color:'#000',
      paddingTop:10,
      paddingLeft:5,
      paddingRight:5,
      fontFamily:'Calibri Bold',
      padding:5,
      fontSize:14,
    },
    error_message:{
      color:'red',
      paddingLeft:10,
      paddingRight:5,
      fontFamily:'Calibri Bold',
      paddingTop:3,
      paddingBottom:3,
      fontSize:14,
    },
    new_btn_style:
        {
            width:'48%',
            padding:10,
            justifyContent:'center',
            alignItems:'center',
            textAlign:'center',
            marginTop:15,
            borderColor:'#ccc',
            borderWidth:0.2

        },
    click_new_btn_style:
        {
            width:'90%',
            padding:10,
            justifyContent:'center',
            alignItems:'center',
            textAlign:'center',
            marginLeft:15,
            marginRight:10,
            marginTop:15,
            borderColor:'#ccc',
            borderWidth:0.2 ,
            backgroundColor:'#F82E56'
        },
    btn_type_text_style:
        {
            color:'#fff',
            fontSize:12,
            fontFamily:'Calibri Regular',
            fontWeight:'bold',
            textAlign:'center',
        },
    click_btn_text_style:
        {
            color:'#fff',
            fontSize:13,
            fontFamily:'Calibri regular',
        },
    status_right : {
        justifyContent:'flex-end',
        alignItems:'flex-end',
        textAlign:'right',
        alignSelf: 'flex-end',
        bottom:20,
        right:10,
        position:'absolute'
    },
    status_right_text : {
        fontSize:18,
        color:'#F82E56',
        padding:10,
        fontFamily:'Calibri Bold',
    },
    new_text_input_style:
        {
            flex:1,
            fontSize:12,
            padding:0,
            fontFamily:'Calibri Regular',
            width:'90%'
        },

}