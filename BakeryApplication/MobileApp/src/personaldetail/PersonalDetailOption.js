import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, ListView, TouchableWithoutFeedback, Linking, Alert,StatusBar
} from 'react-native';
import {Container, Header, Item, Input, Icon, Text, Button, Right, Content, Body, Title, Left} from 'native-base';
import { CheckBox } from 'react-native-elements'
import Toast from 'react-native-simple-toast';

import styles from './personaldetail.style';
var Realm = require('realm');
let realm;


export default class PersonalDetailOption extends Component<> {

    constructor(props) {

        super(props);
        global.signupcount=0,
        this.state = {
            selectedButton: null,

            mobileNo: '',
            phone: '',
            password: '',
            username: '',
            user_name: '',
            email: '',
            ErrorStatus_mobile: true,
            ErrorStatus_mail: true,
            ErrorStatus_username: true,
            ErrorStatus_password: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,
            ActivityIndicator:false,
            fb:false,twitter:false,linkedin:false,instagram:false,
            empty_username:false,
            empty_email:false,
            empty_phone_no:false,
        };
        this.selectionOnPress = this.selectionOnPress.bind(this);
    }
    selectionOnPress(userType) {
        this.setState({ selectedButton: userType });
    }
    onEnterMobileNo = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({mobileNo : TextInputValues, ErrorStatus_mobile : false,empty_phone_no:false}) ;
        }else{
            this.setState({mobileNo : TextInputValues, ErrorStatus_mobile : true}) ;
        }
    }
    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(text) === false)
        {
            console.log("Email is Not Correct");
            this.setState({email:text})
            return false;
        }
        else {
            console.log("Email is Correct");
            this.setState({email : text, ErrorStatus_mail : true}) ;

            this.onEnteremail(text);
        }
    }
    onEnterusername = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({username : TextInputValues, ErrorStatus_username : false,empty_username:false,
            }) ;
        }else{
            this.setState({username : TextInputValues, ErrorStatus_username : true}) ;
        }
    }

    onEnteremail = (TextInputValues) =>{

        if(TextInputValues.trim() != 0){
            this.setState({email : TextInputValues, ErrorStatus_mail : false,empty_email:false,}) ;
        }

        else{

            this.setState({email : TextInputValues, ErrorStatus_mail : true}) ;
        }
    }

validate_mobileno=(mobno)=>{
        if(mobno.length!=10)
        {
           console.log("Length Should be 10");
        }
        else
        {
            this.onEnterMobileNo(mobno);
        }
}



    Function_to_open_SplashScreen = (btntypes) => {

        this.selectionOnPress(btntypes)

    }


/*========================
* User Creation
* ========================*/

createUser = () => {
    this.setState({ActivityIndicator:true})
    return fetch('http://3.13.212.113:3000/user/create',
        {
            method: 'POST',
            headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            body: JSON.stringify(
                {
                        userdetails: {
                            name: this.state.username,
                            mobile: this.props.navigation.state.params.mobile_no,
                            email: this.state.email,
                            roleId: 1,

                        }
                })

        }).then((response) => response.json()).then((response) =>
    {

        if(response.status!=false)
        {
            Toast.show(response.message, Toast.SHORT);
            this.setState({ActivityIndicator:false});
            this.Function_TO_SendOTP(this.props.navigation.state.params.mobile_no);

        }
        else
        {
            this.setState({ActivityIndicator:false});
            Toast.show("User Not Created Successfully", Toast.SHORT);
        }
        // console.log("The Data is = " + JSON.stringify(this.state.data).toString());
        //console.log("The weightPrice is = " + JSON.stringify(this.state.weightPrice).toString());

    }).catch((error) =>
    {
        console.error(error);

    });

}


/*===============================
* Save User Details to server
* ===============================*/
    Function_to_open_LoginScreen = () => {


        if(this.state.ErrorStatus_username===true && this.state.ErrorStatus_mail==true )
        {
                this.setState({
                    empty_username:true,
                    empty_email:true,
                })
        }

       else  if(this.state.ErrorStatus_username===true)
        {
            this.setState({
                empty_username:true,
            })
        }
        else if(this.state.ErrorStatus_mail==true)
        {            realm = new Realm({ path: 'CakeDatabase.realm' });

            this.setState({
                empty_email:true,
            })
        }
        else
        {
        /*    // Call User Created Function
            realm = new Realm({ path: 'CakeDatabase.realm' });

            realm.write(() => {

                realm.create('User_Details', {
                    mobile_number:this.state.mobileNo,
                    login_value:1

                });

            });
            console.warn("The Value in User Details Table are as follows"+JSON.stringify(realm.objects('User_Details')));
        */    this.createUser();
        }



    }

handleBackPress = () => {
        this.props.navigation.navigate('DescriptionScreen');
}


/*====================================
* Send OTP From Signup Screen
* ====================================*/

    Function_TO_SendOTP = (mobile_number) =>
    {
        console.log("Personal Details 214 Send OTP Called " );
        this.setState({ ActivityIndicator_Loading : true }, () =>
        {
            fetch('http://3.13.212.113:3000/message/sendotp',
                {
                    method: 'POST',
                    headers:
                        {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                    body: JSON.stringify(
                        {
                            mobile_no :mobile_number,

                        })

                }).then((response) => response.json()).then((response) =>
            {
                if(response.status!='Fail') {
                    Toast.show(response.message, Toast.SHORT);
                    this.setState({ActivityIndicator_Loading: false});
                    this.props.navigation.navigate('Verification_Screen', {mobile_number:mobile_number});
                }
                else
                {
                    Toast.show(response.message, Toast.SHORT);
                    this.setState({ ActivityIndicator_Loading : false});
                }


            }).catch((error) =>
            {
                console.error(error);

                this.setState({ ActivityIndicator_Loading : false});
            });
        });
    }
/*========================
* UI Design
* =======================*/

render() {

        return(
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>

                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=>this.props.navigation.navigate('MobileNo')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>


                    <Body>
                     <Title style={styles.main_salon_name}>Sign Up</Title>

                    </Body>
                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <ScrollView>
                        <View style={styles.image_container}>
                            <Image
                                source={require('../../images/logo_small.png')}
                                style={styles.logo}
                            />
                        </View>

                        <Text style={styles.otp_display_text}>Enter details to register </Text>

                        <Text style={styles.displays_text}>Enter Username <Text style={[styles.displays_text,{color:'red',paddingLeft:5}]}>*</Text></Text>

                        <View style={this.state.empty_username?styles.error_SectionStyle:styles.SectionStyle}>
                            <TextInput
                                style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                placeholder="Enter Username "
                                underlineColorAndroid="transparent"
                                returnKeyType = {"next"}
                                autoFocus = {false}
                                keyboardType='email-address'
                                onChangeText = {(TextInputText) =>  this.onEnterusername(TextInputText)}
                                onSubmitEditing={(event) => {
                                    this.refs.email.focus();

                                }}/>


                        </View>

                        {this.state.empty_username?
                        <Text style={styles.error_message}>Username is Mandetory</Text>:null}


                        <Text style={styles.displays_text}>Enter Email <Text style={[styles.displays_text,{color:'red',paddingLeft:5}]}>*</Text></Text>

                        <View style={this.state.empty_email?styles.error_SectionStyle:styles.SectionStyle}>

                            <TextInput
                                ref='email'
                                style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                placeholder="Enter  Email "
                                underlineColorAndroid="transparent"
                                returnKeyType = {"next"}
                                keyboardType='email-address'
                                onChangeText = {(TextInputText) => { this.validate(TextInputText)}}
                                onSubmitEditing={(event) => {
                                    this.refs.phone.focus();

                                }}/>

                        </View>

                        {this.state.empty_email?
                        <Text style={styles.error_message}>Email is Mandetory</Text>:null}


                        <Text style={styles.displays_text}>Enter Phone Number <Text style={[styles.displays_text,{color:'red',paddingLeft:5}]}>*</Text></Text>

                       {/* <View style={this.state.empty_phone_no?styles.error_SectionStyle:styles.SectionStyle}>
                       */}
                       <View style={styles.SectionStyle}>

                            <TextInput
                                ref='phone'
                                style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                                placeholder={this.props.navigation.state.params.mobile_no}
                                underlineColorAndroid="transparent"
                                editable={false}
                                maxLength={10}
                                returnKeyType = {"done"}
                                keyboardType='numeric'
                                onChangeText = {(TextInputText) =>  this.validate_mobileno(TextInputText)}/>

                        </View>
{/*
                        {this.state.empty_phone_no?
                        <Text style={styles.error_message}>Phone Number is Mandetory</Text>:null}
*/}


                    </ScrollView>

                </Content>
                <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                    justifyContent:'center',alignItems:'center',textAlign:'center',}}
                         onPress={()=>this.Function_to_open_LoginScreen()}>
                    <Text style={{color:'#fff',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                          onPress={()=>this.Function_to_open_LoginScreen()}  >Register</Text>
                </Button>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.ActivityIndicator}>
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center',
                                backgroundColor:'#333333'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>

            </Modal>
            </Container>


        );
    }
}

