/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar, PermissionsAndroid, ListView, Image,TouchableOpacity} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text, Title, Left, Button, Icon, Right} from 'native-base';

import styles from './status.style';

type Props = {};
export default class SuccessScreen extends Component<Props> {

      render() {
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.main_salon_name}>Order Success</Title>
                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={styles.container}>
                        <Image source={require('../../images/confirm.png')}
                               style={{width:128,height:128}} />

                        <Text style={[styles.empty_vart_text_one,{fontSize:22,fontWeight:'bold',padding:10,}]}>
                                 Order placed successfully.
                        </Text>
                        <Text style={[styles.empty_vart_text_one,{fontSize:16,padding:5}]}> Order ID  {this.props.navigation.state.params.orderId} </Text>





                    </View>

                </Content>
                <Button  style={{backgroundColor:'#CE3152',width:'100%',padding:10,bottom:0,
                    justifyContent:'center',alignItems:'center',textAlign:'center',}}
                         onPress={()=> this.props.navigation.navigate('Category')}>

                    <Text style={{
                        color: '#fff',
                        fontSize: 14,
                        fontWeight: 'bold',
                        fontFamily: 'Calibri Bold',
                    }}
                          onPress={()=> this.props.navigation.navigate('Category')}>Continue Shopping </Text>

                </Button>

            </Container>

        );
    }
}

