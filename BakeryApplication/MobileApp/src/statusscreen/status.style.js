export default {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:'50%',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
        lineHeight:22,
    },
    header_style:{backgroundColor:'#333333',padding:10,height:60},
    main_salon_name:{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'20%',color:'#fff',fontWeight:'bold',},
    shopnowbtn:{
        textAlign:'center',
        fontSize:14,
        fontFamily:'Calibri Bold',
        fontWeight:'bold',
    },
    empty_vart_text_one:{
        fontFamily:'Proxima Nova Reg',
    },
}