import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, TouchableWithoutFeedback, Linking, Alert,Text,StatusBar,FlatList
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left, Card, CardItem,Badge} from 'native-base';
import styles from "../address/address.style";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import { Divider } from 'react-native-elements';
import Overlay from 'react-native-modal-overlay';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

import AddAddress from "./AddAddress";
import Snackbar from "react-native-snackbar";

var Realm = require('realm');
let realm ;

export default class TPGetAddress extends Component<> {
    constructor(props) {

        super(props);
        this.state = {
            modalVisible: false,
            isLoading:true,
            address_data:[],
            address_ActivityIndicator_Loading:false
        };

    }
    /*============================
    * OnSelect Function
    * ============================*/

    onSelect(index, value) {
        this.setState({
            text: `${value}`,
        });
        const [name, street, unit, city,address_type,address_id] = `${value}`.split(',');
        console.warn('address_id'+address_id);
        this.setState({
            address_id:address_id
        })
        console.warn('Valuss array' + `${value}`);
    }

    /*========================
    * componentdidmount
    *========================*/

    componentDidMount(){
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.getAllAddress();

            }
        );
        this.getAllAddress();


        //alert(this.props.navigation.state.params.userId);
    }


    /*========================
    * Fetch Address
    * ========================*/

    getAllAddress = () => {

        //fetch('http://3.13.212.113:3000/user/address/'+this.props.navigation.state.params.userId)
        fetch('http://3.13.212.113:3000/user/address/18')
            .then((response) => response.json())
            .then((responseJson) => {
                console.warn("Address array"+JSON.stringify(responseJson));
                this.setState({
                    isLoading:false,
                    address_data:responseJson,


                }, function(){
                    this.state.address_data.map((item, index)=>{
                        if(item.is_default == true)
                        {
                            // this.setState({sel_id:item.id});
                            console.warn("index : "+index);
                            this.setState({sel_index:index,
                                address_id:item.id});
                        }


                    });

                })
                    .catch((error) =>{
                        console.error(error);
                        this.setState({
                            isLoading:false,
                        })
                    });
            });
    }

    /*===========================
   *  * Edit Address Details
   * ==========================*/

    onEdit = (address,city,state,pincode,userID,item_id,btn_value,contact_no) => {
        //alert("getAllAddress userID::"+userID);
        this.props.navigation.navigate ('AddAddress',{address,city,state,pincode,userID,item_id,btn_value,contact_no});

    }
    /*================================
    * Delete the present address
    * ================================*/
    ondelete_address = (item_id) => {
        console.warn("Delete ID"+item_id);
        console.warn("delete_id"+this.state.delete_id);
        fetch('http://3.13.212.113:3000/user/address/delete/'+item_id,
            {
                method: 'DELETE',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
            }).then((response) => response.json()).then((response) => {


            if (response.status != 'Fail') {
                Snackbar.show({
                    title: response.message,
                    duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                    color:'#fff',
                    action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                    },
                });
                this.setState({modalVisible:false});
                this.getAllAddress();

            } else {
                Snackbar.show({
                    title: response.message,
                    duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                    color:'#fff',
                    action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                    },
                });

            }


        }).catch((error) => {
            console.error(error);

            this.setState({ActivityIndicator_Loading: false});
        });
    }
    /*=========================================
    * APi Callled for Deliver Address
    * =========================================*/
    deliveraddress= () =>{

        console.warn("this.state.text"+this.state.text);

        const [name, street, unit, city,address_type,address_id] = this.state.text.split(',');
        console.warn('address_id 22'+address_id);
        console.warn('name'+name);
        console.warn('street'+street);
        console.warn('unit'+unit);
        console.warn('city'+city);
        console.warn('address_type'+address_type);

        this.setState({address_ActivityIndicator_Loading: true}, () => {
            console.log("Personal Details 214 Send OTP Called ");
            fetch('http://3.13.212.113:3000/user/changeDefault',
                {
                    method: 'POST',
                    headers:
                        {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    body: JSON.stringify(
                        {
                            //  userId: this.props.navigation.state.params.userId,
                            id: address_id,
                            isDefault: true
                        })

                }).then((response) => response.json()).then((response) => {


                if (response.status != 'Fail') {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({address_ActivityIndicator_Loading: false});
                    this.props.navigation.navigate('Mycart');
                } else {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({address_ActivityIndicator_Loading: false});

                }


            }).catch((error) => {
                console.error(error);

                this.setState({ActivityIndicator_Loading: false});
            });
        });
    }
    /*=============================
    * Show DropDown Menu
    * =============================*/
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };


    /*======================
    * UI Start
    * =====================*/
    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#000'}/>
                    <Text style={{color:'#000'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#f3e3eb',}}>

                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=> this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />
                    </Left>
                    <Body style={{flexDirection:'row'}}>
                    <Title style={styles.main_salon_name}> Select Address</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <Button  style={{backgroundColor:'#E1A7C5',width:'100%',padding:10,marginBottom:10,
                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                             onPress={()=>this.onEdit('','','','',this.props.navigation.state.params.userID,'',0,'')}>

                        <Text style={{
                            color: '#000',
                            fontSize: 14,
                            fontWeight: 'bold',
                            fontFamily: 'Calibri Bold',
                        }}
                              onPress={()=>this.onEdit('','','','',this.props.navigation.state.params.userID,'',0,'')}>

                            Add new address </Text>

                    </Button>

                    <View style={{backgroundColor:'#fff',borderRadius:5,padding:5,width:'100%'}}>
                        <RadioGroup
                            size={24}
                            thickness={2}
                            color='#E1A7C5'
                            selectedIndex={this.state.sel_index}
                            style={{marginBottom:10,}}
                            onSelect = {(index, value) => this.onSelect(index, value)}>
                            {this.state.address_data.map((item) => {
                                return <RadioButton
                                    value={item.address + ',' + item.city + ',' + item.state + ',' + item.pincode + ',' + item.address_type + "," + item.id + "," + item.contact_no}
                                    color='#E1A7C5'
                                    style={{alignItems: 'center'}}>

                                    <TouchableOpacity>
                                        <View style={{alignItems:'flex-end',justifyContent:'flex-end'}}>
                                            <Menu
                                                ref={this.setMenuRef}
                                                button={<Text onPress={this.showMenu}>Show menu</Text>}>
                                                <MenuItem onPress={this.hideMenu}>Edit</MenuItem>
                                                <MenuItem onPress={this.hideMenu}>Remove</MenuItem>
                                            </Menu>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{backgroundColor:'#fff',borderRadius:5,padding:5,width:'100%'}}>
                                        <View style={{flexDirection:'row',width:'100%',paddingLeft:'5%',paddingBottom:10}}>
                                            <View style={{width:'60%',}} >
                                                <Text style={{lineHeight: 20,fontSize:14,}}>
                                                    {item.address}
                                                </Text>
                                                <Text style={{lineHeight: 20,fontSize:14,}}>
                                                    {item.city} {item.state}
                                                </Text>
                                                <Text style={{lineHeight: 20,fontSize:14,}}>
                                                    {item.pincode}
                                                </Text>

                                            </View>
                                            <View style={{width:'22%'}}>

                                                {item.address_type!='' ?
                                                    <Badge style={{backgroundColor:'#edb2d1',borderRadius:3,height:18,
                                                        alignItems:'center'}}>
                                                        <Text style={{color:'#666',fontWeight:'bold',
                                                            textAlign: 'center', fontSize:10}}>{item.address_type.toUpperCase()}</Text>
                                                    </Badge>
                                                    :null}

                                            </View>
                                        </View>
                                    </View>

                                </RadioButton>
                            })}
                        </RadioGroup>

                    </View>


























                    <RadioGroup
                        size={24}
                        thickness={2}
                        color='#E1A7C5'
                        selectedIndex={this.state.sel_index}
                        style={{marginBottom:10,}}
                        onSelect = {(index, value) => this.onSelect(index, value)}>

                        {this.state.address_data.map((item) =>{
                            var data=item.address+','+item.city+','+item.state+','+item.pincode;
                            return <RadioButton
                                value= {item.address + ',' + item.city + ','+item.state+','+item.pincode+','+item.address_type+","+item.id+","+item.contact_no}
                                color='#E1A7C5'
                                style={{alignItems:'center'}}
                            >
                                <View style={{backgroundColor:'#fff',borderRadius:5,padding:5,width:'100%'}}>
                                    <View style={{flexDirection:'row',width:'100%',paddingLeft:'5%',paddingBottom:10}}>
                                        <View style={{width:'60%',}} >
                                            <Text style={{lineHeight: 20,fontSize:14,}}>
                                                {item.address}
                                            </Text>
                                            <Text style={{lineHeight: 20,fontSize:14,}}>
                                                {item.city} {item.state}
                                            </Text>
                                            <Text style={{lineHeight: 20,fontSize:14,}}>
                                                {item.pincode}
                                            </Text>

                                        </View>
                                        <View style={{width:'22%'}}>

                                            {item.address_type!='' ?
                                                <Badge style={{backgroundColor:'#edb2d1',borderRadius:3,height:18,
                                                    alignItems:'center'}}>
                                                    <Text style={{color:'#666',fontWeight:'bold',
                                                        textAlign: 'center', fontSize:10}}>{item.address_type.toUpperCase()}</Text>
                                                </Badge>
                                                :null}

                                        </View>
                                        {this.props.navigation.state.params.screenID!=1 ?
                                            null :
                                            this.state.address_id!=item.id ?
                                                <View style={{alignItems:'center',justifyContent:'center'}}>

                                                    <TouchableOpacity
                                                        onPress={()=> this.setState({modalVisible:true,delete_id:item.id})}>
                                                        <Icon type="MaterialIcons" name="delete"
                                                              style={{fontSize: 24, color: '#E1A7C5',paddingLeft:'2%',paddingBottom:'2%'}}/>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                        onPress={()=> this.onEdit(item.address,item.city,item.state,item.pincode,this.props.navigation.state.params.userId,item.id,1,item.contact_no)}>
                                                        <Icon type="EvilIcons" name="pencil"
                                                              style={{fontSize: 30, color: '#E1A7C5',paddingLeft:'2%'}}/>
                                                    </TouchableOpacity>

                                                </View> :

                                                <View style={{alignItems:'center',justifyContent:'center'}}>
                                                    <TouchableOpacity
                                                        onPress={()=> this.onEdit(item.address,item.city,item.state,item.pincode,this.props.navigation.state.params.userId,item.id,1,item.contact_no)}>
                                                        <Icon type="EvilIcons" name="pencil"
                                                              style={{fontSize: 30, color: '#E1A7C5',}}/>
                                                    </TouchableOpacity>
                                                </View>

                                            /*
                                                                                    :

                                                                                    <TouchableOpacity
                                                                                    onPress={()=> this.onEdit(item.address,item.city,item.state,item.pincode,this.props.navigation.state.params.userId,item.id,1)}>
                                                                                    <Icon type="EvilIcons" name="pencil"
                                                                                    style={{fontSize: 30, color: '#E1A7C5',}}/>
                                                                                    </TouchableOpacity>
                                            */
                                        }
                                        {/* <TouchableOpacity
                                        onPress={()=> this.onEdit(item.address,item.city,item.state,item.pincode,this.props.navigation.state.params.userId)}>
                                        <Icon type="EvilIcons" name="pencil"
                                              style={{fontSize: 30, color: '#E1A7C5',}}/>
                                    </TouchableOpacity>
*/}
                                        {/* <TouchableOpacity
                                        onPress={()=> this.ondelete_address(item.id)}>
                                        <Icon type="MaterialIcons" name="delete"
                                              style={{fontSize: 30, color: '#E1A7C5',}}/>
                                    </TouchableOpacity>
*/}
                                        {/* <Text style={{color:'#EC429C', borderRadius:2,borderColor:'#EC429C',borderWidth:1,
                                        pading:10,height:20,width:30,textAlign: 'center', fontSize:12}}
                                    onPress={()=> this.props.navigation.navigate('AddAddress')}>Edit</Text>
*/}

                                    </View>
                                </View>


                            </RadioButton>
                        })}
                    </RadioGroup>


                </Content>

                {this.props.navigation.state.params.screenID!=1 ?
                    <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,bottom:0,
                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                             onPress={()=> this.deliveraddress()}>
                        {
                            this.state.address_ActivityIndicator_Loading ?  <ActivityIndicator size={'large'} color={'#fff'}/> :
                                <Text style={{
                                    color: '#fff', fontSize: 14, fontWeight: 'bold', fontFamily: 'Calibri Bold',
                                }}
                                      onPress={() => this.deliveraddress()}>
                                    Deliver here
                                </Text>
                        }

                    </Button> : null }

                <Modal transparent = {false}
                       animated={'slide'}
                       visible = {this.state.modalVisible}
                       onRequestClose={() => this.setState({modalVisible:false})} >

                    <View style={{backgroundColor:'rgba(0,0,0,0.7)',paddingTop:'10%',flex:1,
                        justifyContent:'center',alignItems:'center' }}>

                        <View style={{backgroundColor:'#fff',borderRadius:0,margin:20}}>
                            <Text style={{textAlign:'center',fontSize:15,paddingTop:20,paddingBottom:20,paddingLeft:'12%',paddingRight:'12%',
                                fontWeight:'bold',fontFamily:'Calibri Bold',}}>
                                Are you sure you want to remove this item?
                            </Text>
                            <View style={{flexDirection:'row'}}>

                                <View style={{flexDirection:'row',}}>

                                    <Button  style={{backgroundColor:'#E1A7C5',width:'50%',padding:10,bottom:0,
                                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>this.ondelete_address(this.state.delete_id)}>
                                        <Text style={{color:'#000',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>this.ondelete_address(this.state.delete_id)}>
                                            OK
                                        </Text>
                                    </Button>

                                    <Button  style={{backgroundColor:'#333333',width:'50%',padding:10,bottom:0,
                                        marginRight:'3%',justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>this.setState({modalVisible:false})}>

                                        <Text style={{color:'#fff',fontSize:12,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>this.setState({modalVisible:false})}>Cancle</Text>

                                    </Button>
                                </View>



                            </View>
                        </View>
                    </View>
                </Modal>

            </Container>
        )
    }
}
