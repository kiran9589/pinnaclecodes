import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, TouchableWithoutFeedback, Linking, Alert,Text,StatusBar,FlatList
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left, Card, CardItem,Badge} from 'native-base';
import styles from "../address/address.style";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import { Divider } from 'react-native-elements';

import AddAddress from "./AddAddress";
import Snackbar from "react-native-snackbar";

var Realm = require('realm');
let realm ;

export default class BlankAddressBlankAddress extends Component<> {
    constructor(props) {

        super(props);
        this.state = {
            modalVisible: false,
            isLoading:true,
            address_data:[],
            address_ActivityIndicator_Loading:false
        };

    }
    /*============================
    * OnSelect Function
    * ============================*/

    onSelect(index, value) {
        this.setState({
            text: `${value}`,
        });
        const [name, street, unit, city,address_type,address_id] = `${value}`.split(',');
        this.setState({
            address_id:address_id
        })
    }

    /*========================
    * componentdidmount
    *========================*/

    componentDidMount(){
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {


            }
        );



    }


    /*========================
    * Fetch Address
    * ========================*/



    /*===========================
   *  * Edit Address Details
   * ==========================*/

    onEdit = (address,city,state,pincode,userID,item_id,btn_value,contact_no) => {
        alert(userID);

        if(this.props.navigation.state.params.screenID!=1 )
        {
            this.props.navigation.navigate ('AddAddress',{address,city,state,pincode,userID,item_id,btn_value,contact_no});
        }
        else
        {
            this.props.navigation.navigate ('AddContactByDrawer',{address,city,state,pincode,userID,item_id,btn_value,contact_no})
        }
    }
    /*================================
    * Delete the present address
    * ================================*/
    ondelete_address = (item_id) => {
        fetch('http://3.13.212.113:3000/user/address/delete/'+item_id,
            {
                method: 'DELETE',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
            }).then((response) => response.json()).then((response) => {


            if (response.status != 'Fail') {
                Snackbar.show({
                    title: response.message,
                    duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                    color:'#fff',
                    action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                    },
                });
                this.setState({modalVisible:false});


            } else {
                Snackbar.show({
                    title: response.message,
                    duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                    color:'#fff',
                    action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                    },
                });

            }


        }).catch((error) => {
            console.error(error);

            this.setState({ActivityIndicator_Loading: false});
        });
    }
    /*=========================================
    * APi Callled for Deliver Address
    * =========================================*/
    deliveraddress= () =>{


        const [name, street, unit, city,address_type,address_id] = this.state.text.split(',');

        this.setState({address_ActivityIndicator_Loading: true}, () => {
            fetch('http://3.13.212.113:3000/user/changeDefault',
                {
                    method: 'POST',
                    headers:
                        {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    body: JSON.stringify(
                        {
                            //  userId: this.props.navigation.state.params.userId,
                            id: address_id,
                            isDefault: true
                        })

                }).then((response) => response.json()).then((response) => {


                if (response.status != 'Fail') {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({address_ActivityIndicator_Loading: false});
                    {
                        this.props.navigation.state.params.screenID!=1 ?
                            this.props.navigation.navigate('Mycart'):
                            this.props.navigation.navigate('MyDrawerNavigator')}
                } else {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({address_ActivityIndicator_Loading: false});

                }


            }).catch((error) => {
                console.error(error);

                this.setState({ActivityIndicator_Loading: false});
            });
        });
    }
    /*=============================
    * Show DropDown Menu
    * =============================*/
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };


    /*======================
    * UI Start
    * =====================*/
    render() {
        return (
            <Container style={{backgroundColor:'#f3e3eb',}}>

                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=> this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />
                    </Left>
                    <Body style={{flexDirection:'row'}}>
                        <Title style={styles.main_salon_name}> Select Address</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>

                    <Text style={{fontSize:20,fontWeight:'bold',textAlign:'center',marginTop:'20%'}}> No Address Present Please Add.</Text>

                </Content>




            </Container>
        )
    }
}
