import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, TouchableWithoutFeedback, Linking, Alert,Text,StatusBar,FlatList
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left, Card, CardItem,Badge} from 'native-base';
import styles from "../address/address.style";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Toast from "react-native-simple-toast";
import Snackbar from 'react-native-snackbar';

var Realm = require('realm');
let realm ;

export default class AddAddress extends Component<> {
    constructor(props) {

        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            address_data: [],
            ActivityIndicator_Loading:false,
            flatname:'',
            city:'Pune',
            pincode:'411002',
            state:'Maharashta',
            country:'',
            ErrorStatus_flatname : true,
            ErrorStatus_city : true,
            ErrorStatus_pincode : true,
            ErrorStatus_state : true,
            ErrorStatus_country : true,
            address_btn_type:'',
            mobileno:'',
            ErrorStatus_mobileno:true,
            btn_value:0,
            empty_address:false,
            empty_city:false,
            empty_state:false,
            empty_pincode:false,
            empty_address_type:false,
            empty_phonenumber:false

        };

    }


    /*============================
    * TextInput Functions
    * ============================*/

    onEnterflatname = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({flatname : TextInputValues, ErrorStatus_flatname : false,empty_address:false}) ;
        }else{
            this.setState({flatname : TextInputValues, ErrorStatus_flatname: true}) ;
        }
    }
    onEntercity = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({city : TextInputValues, ErrorStatus_city : false,empty_city:false}) ;
        }else{
            this.setState({city : TextInputValues, ErrorStatus_city: true}) ;
        }
    }
    validate_pincode=(pincode)=>{
        if(pincode.length!=6)
        {
            this.setState({
                empty_pincode:true,
                message:'Length should equal than 6'
            })
        }
        else
        {
            this.setState({
                message:'Pin Code is Mandetory'
            })
            this.onEnterpincode(pincode);
        }
    }

    onEnterpincode = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({pincode : TextInputValues, ErrorStatus_pincode : false,empty_pincode:false,}) ;
        }else{
            this.setState({pincode : TextInputValues, ErrorStatus_pincode: true,message:'Pin Code is Mandetory'}) ;
        }
    }
    onEnterstate = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({state : TextInputValues, ErrorStatus_state : false,empty_state:false}) ;
        }else{
            this.setState({state : TextInputValues, ErrorStatus_state: true}) ;
        }
    }
    onEntercountry = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({country : TextInputValues, ErrorStatus_country : false,}) ;
        }else{
            this.setState({country : TextInputValues, ErrorStatus_country: true}) ;
        }
    }

    validate_mobileno=(mobno)=>{
        if(mobno.length!=10)
        {
            this.setState({
                empty_phonenumber:true,
                phone_message:'Length should equal than 10'
            })
        }
        else
        {

            this.onEnterphonenumber(mobno);
        }
    }

    onEnterphonenumber = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({mobileno : TextInputValues, ErrorStatus_mobileno : false,empty_phonenumber:false}) ;
        }else{
            this.setState({mobileno : TextInputValues, ErrorStatus_mobileno: true}) ;
        }
    }

    /*=====================================
    * To check Empty Textinput value
     * ====================================*/
    validate_textinput = () =>{
        if(this.state.ErrorStatus_flatname === true )
        {
            this.setState({
                empty_address:true,
                empty_address_type:true,
                empty_phonenumber:true,
                message:'Pin Code is Mandetory',
                phone_message:'Phone Number is Mandetory'

            })
        }
        else if(this.state.ErrorStatus_flatname === true )
        {
            this.setState({
                empty_address:true
            })
        }
        else if(this.state.ErrorStatus_mobileno === true )
        {
            this.setState({
                empty_phonenumber:true
            })
        }
        else {
            {this.props.navigation.state.params.btn_value != 1 ? this.addAddress() : this.updatepdAddress() }
        }
    }
    /*=========================
    * componentDidMount
    * =========================*/
    componentDidMount(){
        this.setState({
            placeholder_id:this.props.navigation.state.params.item_id,
            placeholder_address:this.props.navigation.state.params.address,
            placeholder_city:this.props.navigation.state.params.city,
            placeholder_state:this.props.navigation.state.params.state,
            placeholder_pincode:this.props.navigation.state.params.pincode.toString(),
            placeholder_contact_no:this.props.navigation.state.params.contact_no.toString(),
            city:'Pune',
            pincode:'411002',
            state:'Maharashta',
        });

        if(this.state.placeholder_address!='' && this.state.placeholder_city!= '' && this.state.placeholder_state!= '' && this.state.placeholder_pincode!='' &&this.state.placeholder_contact_no!='' )
        {
            if(this.props.navigation.state.params.btn_value != 1) {
                this.setState({
                    flatname: '',
                    city:'Pune',
                    pincode:'411002',
                    state:'Maharashta',
                });
            }
            else
            {
                this.setState({
                    flatname: this.props.navigation.state.params.address,
                    city: this.props.navigation.state.params.city,
                    state: this.props.navigation.state.params.state,
                    pincode: this.props.navigation.state.params.pincode.toString(),
                    mobileno:this.props.navigation.state.params.contact_no.toString(),

                });
            }
        }

    }

    /*======================
    * Update Address
    * =====================*/
    updatepdAddress = () =>{
        this.setState({ActivityIndicator_Loading: true}, () => {
//                this.state.city+"::"+this.state.state+"::"+this.state.pincode+"::"+this.state.address_btn_type);

            fetch('http://3.13.212.113:3000/user/address/update',
                {
                    method: 'POST',
                    headers:
                        {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    body: JSON.stringify(
                        {
                            id: this.state.placeholder_id,
                            userId: this.props.navigation.state.params.userID,
                            address: this.state.placeholder_address,
                            city: this.state.placeholder_city,
                            state: this.state.placeholder_state,
                            country: 'India',
                            pincode: this.state.placeholder_pincode,
                            contact_no: this.state.placeholder_contact_no,
                            addressType: this.state.address_btn_type

                        })

                }).then((response) => response.json()).then((response) => {


                if (response.status != 'Fail') {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({ActivityIndicator_Loading: false});
                    this.props.navigation.navigate('GetAllAddress');
                } else {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({ActivityIndicator_Loading: false});

                }


            }).catch((error) => {
                console.error(error);

                this.setState({ActivityIndicator_Loading: false});
            });
        });
    }

    /*==============================
    * Add Address To Database
    * ==============================*/

    addAddress = () =>{
        this.setState({ActivityIndicator_Loading: true}, () => {
              fetch('http://3.13.212.113:3000/user/add/address',
                {
                    method: 'POST',
                    headers:
                        {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    body: JSON.stringify(
                        {
                            userId: this.props.navigation.state.params.userID,
                            address: this.state.flatname,
                            city: this.state.city,
                            state: this.state.state,
                            country: 'India',
                            pincode: this.state.pincode,
                            contact_no: this.state.mobileno,
                            addressType: this.state.address_btn_type,
                            isDefault:true,

                        })

                }).then((response) => response.json()).then((response) => {


                if (response.status != 'Fail') {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({ActivityIndicator_Loading: false});
                    this.navigateFunction();
                } else {
                    Snackbar.show({
                        title: response.message,
                        duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                    this.setState({ActivityIndicator_Loading: false});

                }


            }).catch((error) => {
                console.error(error);

                this.setState({ActivityIndicator_Loading: false});
            });
        });
    }
    /*=================================
    * After ADd or Update Navigate
    * ===============================*/

    mycartNavigate = () => {
        this.props.navigation.navigate('Mycart')

    }
    drawernavigation = () => {
        this.props.navigation.navigate('MyDrawerNavigator')

    }
    navigateFunction = () => {
        {
            this.props.navigation.state.params.screenID != 1 ? this.mycartNavigate()
                : this.drawernavigation()
        }
    }
    render() {
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>
                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=> this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flexDirection:'row'}}>
                    {this.props.navigation.state.params.btn_value != 1 ?
                        <Title style={styles.main_salon_name}>
                            Add Address
                        </Title> :
                        <Title style={styles.main_salon_name}>
                            Edit Address
                        </Title>
                    }

                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    {this.props.navigation.state.params.btn_value != 1 ?
                    <Text style={{padding:5,fontWeight:'bold',fontSize:16,textAlign:'center',fontFamily:'Calibri Bold'}}>
                        Add New Address
                    </Text>:
                    <Text style={{padding:5,fontWeight:'bold',fontSize:16,textAlign:'center',fontFamily:'Calibri Bold'}}>
                            Edit Address
                    </Text>}

                    <Text style={styles.text_name}>Enter FlatNo , Flat Name , Area</Text>
                    <View style={this.state.empty_address?styles.error_SectionStyle:styles.address_SectionStyle}>
                        {
                            this.props.navigation.state.params.btn_value != 1 ?
                                <TextInput
                                    ref='flatno'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter address"
                                    keyboardType='default'
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.onEnterflatname(TextInputText)}
                                    onSubmitEditing={(event) => {
                                        this.refs.city.focus();
                                    }}
                                /> :
                                <TextInput
                                    ref='flatno'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter address"
                                    keyboardType='default'
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({placeholder_address:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.city.focus();
                                    }}

                                    value={this.state.placeholder_address}
                                />
                        }

                    </View>

                    {this.state.empty_address?
                        <Text style={styles.error_message}>Flat No, Name is Mandetory</Text>:null}


                    <Text style={styles.text_name}>Enter City</Text>
                    <View style={this.state.empty_city?styles.error_SectionStyle:styles.address_SectionStyle}>
                        {
                            this.props.navigation.state.params.btn_value != 1 ?
                                <TextInput
                                    ref='city'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter City"
                                    keyboardType='default'
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({city:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.state.focus();}}
                                    value={this.state.city}    /> :
                                <TextInput
                                    ref='city'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter City"
                                    keyboardType='default'
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({placeholder_city:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.state.focus();
                                    }}
                                    value={this.state.placeholder_city}/>
                        }

                    </View>
                    {this.state.empty_city?
                        <Text style={styles.error_message}>City is Mandetory</Text>:null}

                    <Text style={styles.text_name}>Enter State</Text>
                    <View style={this.state.empty_state?styles.error_SectionStyle:styles.address_SectionStyle}>

                        {
                            this.props.navigation.state.params.btn_value != 1 ?
                                <TextInput
                                    ref='state'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter State"
                                    keyboardType='default'
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({state:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.country.focus();}}
                                     value={this.state.state}/>
                                :
                                <TextInput
                                    ref='state'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter State"
                                    keyboardType='default'
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({placeholder_state:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.country.focus();
                                    }}
                                    value={this.state.placeholder_state}/>
                        }

                    </View>
                    {this.state.empty_state?
                        <Text style={styles.error_message}>State is Mandetory</Text>:null}


                    <Text style={styles.text_name}>Enter Country</Text>
                    <View style={styles.address_SectionStyle}>

                        <TextInput
                            ref='country'
                            editable={false}
                            style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                            placeholder="India"
                            keyboardType = 'default'
                            returnKeyType = {"next"}
                            underlineColorAndroid="transparent"
                            onChangeText = {(TextInputText) =>  this.onEntercountry(TextInputText)}
                            onSubmitEditing={(event) => {this.refs.pincode.focus();}}/>

                    </View>

                    <Text style={styles.text_name}>Enter Pin Code</Text>
                    <View style={this.state.empty_pincode?styles.error_SectionStyle:styles.address_SectionStyle}>
                        {
                            this.props.navigation.state.params.btn_value != 1 ?

                                <TextInput
                                    ref='pincode'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter Pin Code"
                                    keyboardType='numeric'
                                    maxLength={6}
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({pincode:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.phoneno.focus();
                                    }}
                                value={this.state.pincode}/>
                                :
                                <TextInput
                                    ref='pincode'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter Pin Code"
                                    keyboardType='numeric'
                                    maxLength={6}
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({placeholder_pincode:TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.phoneno.focus();
                                    }}
                                    value={this.state.placeholder_pincode}/>
                        }

                    </View>
                    {this.state.empty_pincode?
                        <Text style={styles.error_message}>{this.state.message}</Text>:null}


                    <Text style={styles.text_name}>Enter Mobile Number</Text>
                    <View style={this.state.empty_phonenumber?styles.error_SectionStyle:styles.address_SectionStyle}>
                        {
                            this.props.navigation.state.params.btn_value != 1 ?

                                <TextInput
                                    ref='phoneno'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter Mobile Number"
                                    keyboardType='numeric'
                                    maxLength={10}
                                    returnKeyType={"done"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.validate_mobileno(TextInputText)}/>
                                :

                                <TextInput
                                    ref='phoneno'
                                    editable={true}
                                    style={{flex: 1, fontSize: 12, padding: 0, fontFamily: 'Calibri Regular',}}
                                    placeholder="Enter Mobile Number"
                                    keyboardType='numeric'
                                    maxLength={10}
                                    returnKeyType={"next"}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(TextInputText) => this.setState({placeholder_contact_no: TextInputText})}
                                    onSubmitEditing={(event) => {
                                        this.refs.phoneno.focus();
                                    }}
                                    value={this.state.placeholder_contact_no}/>
                        }
                    </View>
                    {this.state.empty_phonenumber?
                        <Text style={styles.error_message}>{this.state.phone_message}</Text>:null}



                    <View style={{marginBottom:10}}>
                        <Text style={styles.text_name}>Select Address Type</Text>
                        <ScrollView horizontal={true} vertical={false} showsHorizontalScrollIndicator={false} >

                            <TouchableOpacity onPress={()=> this.setState({view_one:true,view_two:false,view_three:false,address_btn_type:'home'})}>
                                <View

                                    style={this.state.view_one?[styles.selected_technician_cardstyle,{height:35}]:[styles.technician_cardstyle,{height:35}]}>
                                    <View style={styles.technician_view}>
                                        <Text style={this.state.view_one?styles.sel_service_name:styles.service_name}>Home</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>


                            <TouchableOpacity onPress={()=> this.setState({view_one:false,view_two:true,view_three:false,address_btn_type:'office'})}>
                                <View

                                    style={this.state.view_two?[styles.selected_technician_cardstyle,{height:35}]:[styles.technician_cardstyle,{height:35}]}>
                                    <View style={styles.technician_view}>
                                        <Text style={this.state.view_two?styles.sel_service_name:styles.service_name}> Office </Text>
                                    </View>
                                </View>
                            </TouchableOpacity>


                            <TouchableOpacity onPress={()=> this.setState({view_one:false,view_two:false,view_three:true,address_btn_type:'others'})}>
                                <View

                                    style={this.state.view_three?[styles.selected_technician_cardstyle,{height:35}]:[styles.technician_cardstyle,{height:35}]}>
                                    <View style={styles.technician_view}>
                                        <Text style={this.state.view_three?styles.sel_service_name:styles.service_name}> Others </Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>


                </Content>
                <Button style={{
                    backgroundColor: '#333333', width: '100%', padding: 10, bottom: 0,
                    justifyContent: 'center', alignItems: 'center', textAlign: 'center',
                }}
                        onPress={() =>this.props.navigation.state.params.btn_value != 1 ? this.validate_textinput():this.updatepdAddress()}>


                    {
                        this.state.ActivityIndicator_Loading ? <ActivityIndicator color='#fff' size='large'
                                                                                  style={styles.ActivityIndicatorStyle}/> :
                            this.props.navigation.state.params.btn_value != 1 ?
                            <Text style={{
                                color: '#fff',
                                fontSize: 14,
                                fontWeight: 'bold',
                                fontFamily: 'Calibri Bold',
                            }}
                                  onPress={() =>this.props.navigation.state.params.btn_value != 1 ? this.validate_textinput():this.updatepdAddress()}>
                                Save Address </Text> :
                                <Text style={{
                                    color: '#fff',
                                    fontSize: 14,
                                    fontWeight: 'bold',
                                    fontFamily: 'Calibri Bold',
                                }}
                                      onPress={() =>this.props.navigation.state.params.btn_value != 1 ? this.validate_textinput():this.updatepdAddress()}>
                                     Update Address</Text>

                    }
                </Button>

            </Container>
        )
    }
}
