export default {
    header_style:{
        backgroundColor:'#333333',
        paddingTop:5,
        paddingLeft:10,
        paddingRight:10,
        height:65
    },
    main_salon_name:{
        fontSize:18,
        fontFamily:'Proxima Nova Reg',
        paddingTop:6,
        paddingLeft:'12%',
        color:'#fff',
        fontWeight:'bold',
    },

    address_SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: '#000', height: 40, borderRadius: 5 , marginTop:0,marginLeft:10,marginRight:10,
        marginBottom:10,paddingLeft:10,},

    text_name:{fontSize:12,fontFamily:'Calbri -Bold',padding:5,color:'gray'},
    service_name:{
        fontSize:14,
        fontFamily:'Lato-Medium',
        paddingTop:0,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        color:'#000',
        textAlign:'center'
    },
    sel_service_name:{
        fontSize:14,
        fontFamily:'Lato-Medium',
        paddingTop:0,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        color:'#fff',
        textAlign:'center'
    },

    technician_cardstyle:{
        width:80,
        height:55,
        margin:10,
        borderColor:'#000',
        borderRadius:5,
        borderWidth:1,
        alignItems:'center',
        paddingTop:5,
        paddingBotom:5,
    },
    selected_technician_cardstyle:{
        width:80,
        height:55,
        margin:10,
        borderColor:'#EF50A4',
        borderRadius:5,
        borderWidth:1,
        alignItems:'center',
        paddingTop:5,
        paddingBotom:5,
        backgroundColor:'#EF50A4',
    },
    error_message:{
        color:'red',
        paddingLeft:10,
        paddingRight:5,
        fontFamily:'Calibri Bold',
        paddingTop:0,
        paddingBottom:0,
        fontSize:14,
    },
    error_SectionStyle:
        {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff',
            borderWidth: .5,
            borderColor: 'red',
            height: 40,
            borderRadius: 5 ,
            marginTop:3,
            marginLeft:10,
            marginRight:10,
            marginBottom:3,
            paddingLeft:10,

        },
}
