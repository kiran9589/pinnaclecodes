/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
    StatusBar,
    PermissionsAndroid, TouchableOpacity,
    ListView,
    Image,FlatList,ActivityIndicator,
    Modal,
    TouchableWithoutFeedback, TextInput
} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text, Title, Left, Button, Icon, Right} from 'native-base';
import styles from "../verifyotp/otp.style";


type Props = {};
export default class Offers extends Component<Props> {

    constructor(props) {

        super(props);

        this.state = {
            isLoading: true,
            offermodal:false,
            offer_data:[],
            start:0,
            refreshing:false,
            totalCount:0,
            sel_coupon_code:'',
            sel_coupon_des:'',


        };
        this.closeoffermodal = this.closeoffermodal.bind(this);
    }
    closeoffermodal(){
        this.setState({
            offermodal:false,
        })
    }
    openoffermodal(coupon_code,coupon_Detail){
        this.setState({
            offermodal:true,
            sel_coupon_code:coupon_code,
            sel_coupon_des:coupon_Detail,
        })
    }
componentDidMount(){
        this.offerDetails();

}


/*============================================
* Load Offer Details in Flatlist
* ============================================*/

offerDetails = () =>{

    this.setState({offer_data:[]});
    return fetch('http://3.13.212.113:3000/offer/list',
        {
            method: 'POST',
            headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            body: JSON.stringify(
                {
                    start: 0,
                    limit: 10
                })

        }).then((response) => response.json()).then((response) =>
    {

            var totalCount =response.totalCount;
            var offer_record=response.offers;
            console.log("The Data Total Count = " + totalCount);
            this.setState({
                totalCount:totalCount,

                offer_data:[...this.state.offer_data , ...offer_record],
                isLoading: false,
                refreshing:false,
                //dataSource: response.data.items,
            });
            console.log("The Data is = " + JSON.stringify(this.state.offer_data).toString());

    }).catch((error) =>
    {
        console.error(error);

    });

}


/*==============================
* Flast List Event
* ==============================*/

    handleRefresh = () => {
        this.setState({

               start:0,
                refreshing: true,
                offer_data:[]
            },
            ()=>{
                this.offerDetails();
            })
    }

    handleLoadMore = () =>{
        this.setState({
            start:this.state.start + 10,

        },()=>{

                    this.offerDetails();

        })

    }



    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                        <Body>
                       <Title style={styles.main_salon_name}>Offers</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:5}}>


                    <FlatList

                        data={ this.state.offer_data}
                        renderItem={({item}) =>

                            <Card style={styles.main_card_style}>
                                <CardItem>
                                    <Body style={styles.card_body_style}>
                                        <View style={styles.promo_code}>
                                            <View style={styles.second_offer}>
                                                <Text style={styles.promo_code_text_details}>{item.offerCode}</Text>
                                            </View>

                                        </View>
                                        <Text style={styles.main_offer_style}>{item.offerDesc}</Text>
                                        <Text style={styles.submain_offer_style}>Use code {item.offerCode}  & {item.offerDesc}</Text>
                                        <Text style={styles.sub_offer_style}>Maximum Discount: {item.offerValue} {item.offerType}</Text>
                                        <Text style={styles.more_btn} onPress={()=>this.openoffermodal(item.offerCode,item.offerDesc)}>More Details</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        }
                        />



                        {/*<Card style={styles.main_card_style}>
                            <CardItem>
                                <Body style={styles.card_body_style}>
                                <View style={styles.promo_code}>
                                    <View style={styles.second_offer}>
                                        <Text style={styles.promo_code_text_details}>ADCSRRFDTY</Text>
                                    </View>

                                </View>
                                <Text style={styles.main_offer_style}>Get 50% OFF on Your first Cake order</Text>
                                <Text style={styles.submain_offer_style}>Use code ABCXYZ  & get 50% off on your first cake order</Text>
                                <Text style={styles.sub_offer_style}>Maximum Discount: Rs 100</Text>
                                <Text style={styles.more_btn} onPress={()=>this.openoffermodal()}>More Details</Text>
                                </Body>
                            </CardItem>
                        </Card>



                    <Card style={styles.main_card_style}>
                        <CardItem>
                            <Body style={styles.card_body_style}>

                            <View style={styles.promo_code}>
                                <View style={styles.second_offer}>
                                       <Text style={styles.promo_code_text_details}>ADCSRRFDTY</Text>
                                </View>

                            </View>

                            <Text style={styles.main_offer_style}>Get 50% OFF on Your first Cake order</Text>
                            <Text style={styles.submain_offer_style}>Use code ABCXYZ  & get 50% off on your first cake order</Text>
                            <Text style={styles.sub_offer_style}>Maximum Discount: Rs 100</Text>
                            <Text style={styles.more_btn} onPress={()=>this.openoffermodal()}>More Details</Text>
                            </Body>
                        </CardItem>
                    </Card>*/}

                    {/*
                    <Card style={{borderRadius:10}}>
                        <CardItem>
                            <Body style={{textAlign:'center',justifyContent:'center',
                                alignItems:'center'}}>
                            <Text style={styles.offer_description}>
                                Get 30 % Discount on Your First Order use Code :-
                            </Text>
                            <Text style={{fontSize:18,fontFamily:'Lato-Bold',padding:5,color:'green'}}>
                                GET90
                            </Text>
                            </Body>
                        </CardItem>
                    </Card>*/}
                </Content>




                <Modal transparent = {false}
                       visible = {this.state.offermodal}
                       onRequestClose={() => this.closeoffermodal(false)}
                       onBackdropPress={()=>this.closeoffermodal(false)}>

                    <TouchableWithoutFeedback onPress={() => this.closeoffermodal(false)}>
                        <View style={styles.offer_modal_style}>

                                <View style={styles.offer_Card_Style}>

                                    <View style={{alignItems:'flex-end',justifyContent:'flex-end',paddingRight:0,paddingBotom:5,}}>
                                        <TouchableOpacity onPress={()=>this.closeoffermodal()}>
                                            <Icon type="EvilIcons" name="close-o"
                                                  style={{paddingRight:'5%',color:'red', fontSize:24,paddingTop:'2%'}}/>
                                        </TouchableOpacity>
                                    </View>


                                    <Text style={{fontSize:18,fontFamily:'Lato-Bold',color:'#000',textAlign:'center',padding:10,fontWeight:'bold'}}> Coupons Details</Text>
                                    <Text style={{fontSize:16,fontFamily:'Lato-Bold',color:'#333333',textAlign:'center',margin:'2%'}}> Coupon Code:
                                        <Text style={{fontSize:16,fontFamily:'Lato-Bold',color:'#000',fontWeight:'bold',textAlign:'center',marginLeft:10}}>{"  "}{this.state.sel_coupon_code}</Text></Text>
                                    <Text style={{fontSize:16,fontFamily:'Lato-Bold',color:'#333333',textAlign:'left',paddingTop:5,paddingLeft:10,paddingBottom:5,}}> Details:-</Text>
                                    <Text style={{fontSize:14,fontFamily:'Lato-Semibold',color:'#666666',paddingTop:5,paddingBottom:5,paddingRight:10,paddingLeft:10, textAlign:'justify'}}>
                                        1. Use code {this.state.sel_coupon_code}  & {this.state.sel_coupon_des}
                                    </Text>

                                </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

            </Container>
                );
    }
}


