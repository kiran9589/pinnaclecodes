export default {
    header_style:{backgroundColor:'#E1A7C5',padding:10,height:49},
    main_salon_name:{fontSize:16,fontFamily:'Calibri Bold',paddingTop:6,paddingLeft:0,color:'#000'},

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    offer_description:{

        color:'#000',
        fontFamily:'Calibri Bold',
        padding:10,
        fontSize:18,
        textAlign:'center'

    },
    offer_modal_style:{
        flex: 1,
        width: '100%',
        height: 'auto',
        alignContent:'flex-start',
        justifyContent:'flex-start',
        alignItems:'flex-start',
        backgroundColor:'rgba(0,0,0,0.7)',
        padding:10,
        marginTop:'30%',

    },
    offer_Card_Style:{
        width: '90%',
        marginTop:'20%',
        height: '100%',
        backgroundColor:'#666',
        marginLeft:10,
        marginRight:10,
        marginBottom:'3%',

    },
    payment_main_text:{
        fontSize:16,
        fontFamily:'Calibri Bold',
        textAlign:'center',
        margin:5,
        padding:10,
    },
}