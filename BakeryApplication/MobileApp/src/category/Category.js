import React, {Component} from 'react';
import {ImageBackground, StatusBar,AsyncStorage,StyleSheet,  View,Image,TextInput,
    Modal,ScrollView,Dimensions,ActivityIndicator,TouchableOpacity,FlatList,} from 'react-native';
import { Container, Header, Content, Button, Text,Icon,Body,Left,Right,Title,Badge
} from 'native-base';
import styles from './category.style';
var Realm = require('realm');
let realm ;
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class Category extends Component {

    constructor() {
        super();

        this.state = {

            ActivityIndicator_Loading: false,
            modalVisibleThree: false,
            dataSource:{},
            isLoading:true,
            total_cart_count:0,

        }
    }

  loadDescription=(item_id,item_name)=>{
        this.props.navigation.navigate('ItemList',{item_id,item_name});
  }

    onMyCart=()=>{
        this.props.navigation.navigate('Mycart');
    }



/*===================
* componentDidMount
* ===================*/
componentDidMount(){

        this.loadCategory();


    this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {
            try{
                realm = new Realm({ path: 'CakeDatabase.realm'});
                var cart_count = realm.objects('Cart_Details').length;
                this.setState({total_cart_count:cart_count})
            }
            catch (e) {
                this.setState({total_cart_count:0})
            }


        }
    );



}

/*======================
* Load CAtegory
* ======================*/

loadCategory=()=>{
        fetch('http://3.13.212.113:3000/master/categories')
            .then((response) => response.json())
            .then((responseJson) => {
                var listData=responseJson;
                this.setState({
                    isLoading:false,
                    data:listData,
                    dataSource: listData,


                }, function(){

                });
            })
            .catch((error) =>{
                console.error(error);
            });
}


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>
            );
        }

        return (
            <Container style={{backgroundColor: '#FCECF4'}}>
                <Header
                    style={{backgroundColor: '#333333', paddingTop: 5, paddingLeft: 10, paddingRight: 10, height: 65}}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                    <Body>
                        <Text style={{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:3,paddingLeft:'50%',color:'#fff',fontWeight:'bold',}}>Category</Text>


                    </Body>
                    <Right>

                        {this.state.total_cart_count!=0 ?
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>

                            :
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>
 }

                    </Right>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{paddingTop:'12%',paddingLeft:'8%',paddingRight:'5%',paddingBottom:'5%'}}>

                    <FlatList
                            data={this.state.dataSource}
                            renderItem={({ item }) => (
                              <View style={{alignItems:'center',justifyContent:'center',marginTop:'8%',alignSelf: 'center',marginLeft:wp('1%')
                              }}>
                                <TouchableOpacity onPress={()=>this.loadDescription(item.id,item.name)}>
                                    <View
                                        style={styles.technician_cardstyle}>
                                        <ImageBackground
                                            source = {{uri:'http://3.13.212.113:3000'+item.imagePath}}
                                            style={{width:wp('35%'),height:hp('22%'),backgroundColor:'#666'}}>
                                            <View style={styles.technician_view}>
                                                <Text style={styles.service_name}> {item.name}</Text>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                </TouchableOpacity>
                              </View>
                            )}
                            //Setting the number of column
                            numColumns={2}
                            keyExtractor={(item, index) => index}
                        />




                </Content>
            </Container>
        );
    }
}