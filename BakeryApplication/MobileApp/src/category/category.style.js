import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default {

    view_style:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        marginTop:'5%',

    },
    technician_cardstyle:{
        width:wp('35%'),
        height:hp('22%'),
        margin:10,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5,
    },

    technician_view:{
        flexDirection:'column',
        backgroundColor:'#22292fd6',
        bottom:0,
        position:'absolute',
        width:'100%'
    },
    service_name:{
        fontSize:13,
        fontFamily:'Lato-Bold',
        padding:3,
        color:'#fff',
        textAlign:'center'
    },

}