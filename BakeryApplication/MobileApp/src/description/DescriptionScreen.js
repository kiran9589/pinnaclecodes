import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, TouchableWithoutFeedback, Linking, Alert,StatusBar,ListView,
} from 'react-native';
import { Container, Header, Item, Input, Icon,Text, Button,Right,Content,Body,Title,Left,Segment,Badge } from 'native-base';
import  Rating from 'react-native-easy-rating';
import Toast from 'react-native-simple-toast';
import DatePicker from 'react-native-datepicker';

import Snackbar from 'react-native-snackbar';

import styles from './description.style';
import ReviewPage from "../reviewpage/ReviewPage";
var Realm = require('realm');
let realm ;
//productId": 1,"weight":null,"quantity":1,"price": "20.00

export default class DescriptionScreen extends Component<> {

    constructor(props) {

        super(props);
        global.cart_count=0,
        global.signupcount=null;
        this.state = {
            mobileNo: '',
            userID: '',
            user_name: '',
            ErrorStatus_mobile: true,
            ErrorStatus_name: true,
            length: 0,
            ontransfer: false,
            value:0.5,
            view_one:false, view_two:false, view_three:false, view_four:false,
            cakename:'',
            ErrorStatus_cakename:true,
            empty_cake_name:false,
            productName:'',
            weightPrice:[],
            parse_weightPrice:[],
            imagePath:'',
            ActivityIndicator_Loading: false,
            dataSource:[],
            selected_weight:0,
            selected_price:0,
            cart_productId:'',
            cart_weight:'',
            cart_quantity:'',
            cart_price:'',
            isLoading:true,
            total_cart_count:0,
            occassion_name:'',
            blankmodal:false,
            empty_price:false,
            empty_date:false,
            date:'',
            chosenDate:'',

        };

        /*realm = new Realm({
            path: 'CakeDatabase.realm',
            schema: [{name: 'Cart_Details',
                properties:
                    {
                        cart_cake_id: {type: 'int',   default: 0},
                        stack_id: {type: 'int',   default: 0},
                        cart_image:'string',
                        cart_name:'string',
                        cart_productId: 'string',
                        cart_weight: 'string',
                        cart_quantity: 'string',
                        cart_price: 'string',
                        Base_price: 'string',
                    }},
                {name: 'User_Details',
                    properties:
                        {
                            mobile_number:'string',
                            login_value:{type: 'int',   default: 0},

                        }}]
        });*/


}

/*=================
* Signup Page
* =================*/
signupFunction = () =>{
    //this.props.navigation.navigate('PersonalDetailOption');
    this.props.navigation.navigate('MobileNo');

}
/*=======================
* Add Data To Realm
* =======================*/
    add_Record=(click_btn)=>{

        this.setState({blankmodal:false})

        if(this.state.selected_price === 0 && this.state.ErrorStatus_cakename === true && this.state.date === '' )
        {
            this.setState({empty_price:true,empty_cake_name:true,empty_date:true})
        }

        else if(this.state.ErrorStatus_cakename === true && this.state.date === '' )
        {
            this.setState({empty_cake_name:true,empty_date:true})
        }

       else  if(this.state.selected_price === 0)
        {
            this.setState({empty_price:true,})
        }
        else if(this.state.ErrorStatus_cakename === true)
        {
                this.setState({
                    empty_cake_name:true,
                })
        }
        else if(this.state.date === '')
        {
            this.setState({empty_date:true})

        }
        else {


            if (realm.objects('Cart_Details').filtered('cart_productId =' +this.props.navigation.state.params.product_id).length > 0) {
                //Toast.show('Item is Already Present in cart', Toast.SHORT);
                Snackbar.show({
                    title: 'Item is Already Present in cart',
                    duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                    color:'#fff',
                    action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                    },
                });
            }
            else {
                realm = new Realm({path: 'CakeDatabase.realm'});
                realm.write(() => {

                    var ID = realm.objects('Cart_Details').length + 1;

                    realm.create('Cart_Details', {
                        cart_cake_id: ID,
                        stack_id: 1,
                        cart_image: this.state.imagePath,
                        cart_name: this.state.productName,
                        cart_productId: this.props.navigation.state.params.product_id,
                        cart_weight: JSON.stringify(this.state.selected_weight),
                        cart_quantity: '1',
                        cart_price: JSON.stringify(this.state.selected_price),
                        Base_price: JSON.stringify(this.state.selected_price),
                        nameoncake:this.state.cakename,
                        delivery_date:this.state.date,
                        occassion:this.state.occassion_name,


                    });

                });
                this.setState({total_cart_count: realm.objects('Cart_Details').length})
             //   Toast.show('Item Successfully Added into The Cart', Toast.SHORT);
                Snackbar.show({
                    title: 'Item Successfully Added into The Cart',
                    duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                    color:'#fff',
                    action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                    },
                });



                if (click_btn != "0") {
                    if (this.state.ErrorStatus_cakename != true) {


                        /*Getting LOgin Value from Server*/

                        try {
                            realm = new Realm({path: 'CakeDatabase.realm'});
                            var User_Details = realm.objects('User_Details');
                           // var login_value = User_Details[0].login_value;
                            if (User_Details && User_Details[0].login_value != 1) {
                               // this.signupFunction();
                                this.props.navigation.navigate('MobileNo',{rotateID:1});
                                //this.props.navigation.navigate('Mycart');
                            } else {
                               // this.props.navigation.navigate('MobileNo');
                                this.props.navigation.navigate('Mycart');
                            }

                        } catch (e) {
                            //this.props.navigation.navigate('PersonalDetailOption');
                           // this.signupFunction();
                            this.props.navigation.navigate('MobileNo',{rotateID:1});
                        }
                    } else {
                        this.setState({
                            empty_cake_name: true
                        })
                    }

                    //Toast.show('Buy Now Button Click', Toast.SHORT);
                } else {

                    if (this.state.ErrorStatus_cakename != true) {

                        /*realm = new Realm({path: 'CakeDatabase.realm'});
                        realm.write(() => {

                            var ID = realm.objects('Cart_Details').length + 1;

                            realm.create('Cart_Details', {
                                cart_cake_id: ID,
                                stack_id: 1,
                                cart_image: JSON.stringify(this.state.imagePath),
                                cart_name: JSON.stringify(this.state.productName),
                                cart_productId: this.props.navigation.state.params.product_id,
                                cart_weight: JSON.stringify(this.state.selected_weight),
                                cart_quantity: '1',
                                cart_price: JSON.stringify(this.state.selected_price),
                                Base_price: JSON.stringify(this.state.selected_price),

                            });


                        });
                        this.setState({total_cart_count: realm.objects('Cart_Details').length})
                        Toast.show('Item Successfully Added into The Cart', Toast.SHORT);
                        console.warn("The Presented Item In Logal Data Base ===== " + JSON.stringify(realm.objects('Cart_Details')));
                        console.warn("The Total Count In Logal Data Base ===== " + realm.objects('Cart_Details').length);*/
                    } else {
                        this.setState({
                            empty_cake_name: true
                        })

                    }
                }
            }
        }
      //  global.cart_count=realm.objects('Cart_Details').length;

    }

/*========================
* End Realm Code
* ========================*/



    /*  FunctionToOpenOTP_Screen =(otpID) => {
          this.props.navigation.navigate('OTP_Screen',{otpID});
      }*/
    FunctionToOpenMycart =() => {

        if(this.state.ErrorStatus_cakename!=true)
        {
            //this.props.navigation.navigate('PersonalDetailOption');
            this.signupFunction();
            this.props.navigation.navigate('MobileNo',{rotateID:1}
            );

        }
        else
        {
            this.setState({
                empty_cake_name:true
            })
        }

    }
    handleBackPress = () => {
        this.props.navigation.navigate('ItemList');
    }
    review_page = (product_id,item_overallRating) => {
        this.props.navigation.navigate('ReviewPage',{product_id,item_overallRating});
    }
    onSelect(index, value){
        this.setState({
            text: `${value}`
        })
    }
    writereview = () => {
        this.props.navigation.navigate('WriteReview');
    }


    onEntercakename = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({cakename : TextInputValues, ErrorStatus_cakename : false,empty_cake_name:false}) ;
        }else{
            this.setState({cakename : TextInputValues, ErrorStatus_cakename : true}) ;
        }
    }

    onMyCart=()=>{
        this.props.navigation.navigate('Mycart');
    }

componentDidMount(){
        console.warn("under component did");
    //global.cart_count=realm.objects('Cart_Details').length
        this.loadCompanyData();
        this.ShowCurrentDate();
        this.getTotalReviewcount();

    try{
        realm = new Realm({ path: 'CakeDatabase.realm'});
        var cart_count = realm.objects('Cart_Details').length;
        this.setState({total_cart_count:cart_count})
    }
    catch (e) {
        this.setState({total_cart_count:0})
    }



}

/*======================
* get Total Review count
* ======================*/
getTotalReviewcount = () => {
    fetch('http://3.13.212.113:3000/product/'+this.props.navigation.state.params.product_id)

        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                total_length:responseJson.reviews.length,
                overallRating:responseJson.overallRating,
            }, function(){
        })
        .catch((error) =>{
            console.error(error);
        });
});
}
    /*========================
    * Load Product Data
    * ========================*/

loadCompanyData=()=>{
         fetch('http://3.13.212.113:3000/product/'+this.props.navigation.state.params.product_id)

            .then((response) => response.json())
            .then((responseJson) => {
                //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

                this.setState({
                    isLoading:false,
                    productName:responseJson.productName,
                    imagePath: responseJson.imagePath,
                    weightPrice:responseJson.weightPrice,
                 //   dataSource: ds.cloneWithRows(responseJson.weightPrice),

                }, function(){

                       console.warn("responseJson.weightPrice"+responseJson.weightPrice.length);
                       if(responseJson.weightPrice.length === 1) {

                           console.warn("aa"+JSON.parse(responseJson.weightPrice).weight);
                            this.selectweightprice(JSON.parse(responseJson.weightPrice).weight,JSON.parse(responseJson.weightPrice).price);

                           console.warn("Total Price::"+this.state.selected_weight + "=="+this.state.selected_price);
                       }
                });
                //console.log("datasouce"+this.state.dataSource);
            })
            .catch((error) =>{
                console.error(error);
            });
}

/*===========================
* Add Data To WeightPrice
* ===========================*/
selectweightprice = (sel_weight,sel_price) =>{

    this.setState({
            selected_weight: sel_weight,
            selected_price: sel_price,
            view_one:true,
            empty_price:false,
        });

}

/*=======================
* get current date
* =======================*/
ShowCurrentDate=()=>{
    console.warn("Under Zero Console");
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        var sec = new Date().getSeconds(); //Current Seconds

            console.warn("Under Third Console");
            if(hours<19)
            {
                var date = new Date().getDate()+1;
                this.setState({today_date:year+'-'+month+'-'+date,})

            }
            else
            {
                var date = new Date().getDate()+2;
                this.setState({today_date:year+'-'+month+'-'+date,})

            }



}

/*=========================
* Set Date
* ========================*/
 setDate(newDate) {
        this.setState({ chosenDate: newDate });
 }
/*=====================
* UI Code Start
* =====================*/
    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>
            );
        }
        return(
            <Container style={{backgroundColor:'#FCECF4',}}>
                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}
                          onPress={()=>this.props.navigation.navigate('ItemList')}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=>this.props.navigation.navigate('ItemList')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.main_salon_name}>Description</Title>

                    </Body>
                    <Right>

                        {this.state.total_cart_count!=0 ?
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>
 }

                    </Right>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{paddingTop:10,paddingLeft:10,paddingBottom:10,paddingRight:5,}}>
                    <View style={styles.image_container}>
                        <Image
                            style={styles.logo}
                           /* source={require('../../images/image_one.png')}*/
                            source = {{uri:'http://3.13.212.113:3000'+this.state.imagePath}}
                        />

                    </View>
                    <Text style={[styles.text_name,{fontSize:16,fontWeight:'bold',textAlign:'center'}]}>{this.state.productName}</Text>


                    <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'row',marginTop:'2%'}}>
                            <Text style={[styles.text_name,{fontSize:15,fontWeight:'bold'}]}>Rating</Text>



                            <Rating
                                rating={this.props.navigation.state.params.item_overallRating}
                                max={5}
                                iconWidth={24}
                                iconHeight={24}
                                editable={false}
                                iconSelected={require('../../images/star_rate.png')}
                                onRate={(rating) => this.setState({rating: rating})}/>
                        </View>

                        {this.state.total_length != 0 ?
                            <Button transparent style={styles.review_btn_style}
                                    onPress={() => this.review_page(this.props.navigation.state.params.product_id,this.props.navigation.state.params.item_overallRating)}>
                                <Text style={styles.review_text_style}
                                      onPress={() => this.review_page(this.props.navigation.state.params.product_id,this.props.navigation.state.params.item_overallRating)}>Review </Text>
                                <Badge style={{backgroundColor: '#EF50A4', marginTop: -3, marginLeft:-10,width:25,height:25}}>
                                    <Text style={{fontSize: 12}}>{this.state.total_length}</Text>
                                </Badge>

                            </Button> :

                            <Button transparent style={styles.review_btn_style}>
                                <Text style={styles.review_text_style}> No Review </Text>
                            </Button>
                        }
                            </View>

                    <Text style={[styles.text_name,{fontSize:14,fontWeight:'bold',alignItems:'flex-start',justifyContent:'flex-start',textAlign:'left',
                        marginTop:'5%'}]}>Weight & Price </Text>


                    <ScrollView horizontal={true} vertical={false} showsHorizontalScrollIndicator={false} >




                       {this.state.weightPrice.map((item) =>{
                            return <TouchableOpacity onPress={()=> this.selectweightprice(JSON.parse(item).weight,JSON.parse(item).price)}>
                                    {JSON.parse(item).weight > 0 ?
                                <View

                                    style={this.state.view_one?styles.selected_technician_cardstyle:styles.technician_cardstyle}>
                                    <View style={styles.technician_view}>
                                        <Text style={this.state.view_one?styles.sel_service_name:styles.service_name}> {JSON.parse(item).weight} KG </Text>

                                        <Text style={this.state.view_one?styles.sel_service_name:styles.service_name}> {'₹'} {JSON.parse(item).price} </Text>
                                    </View>
                                </View> :
                                        <View

                                            style={this.state.view_one?[styles.selected_technician_cardstyle,{height:35}]:[styles.technician_cardstyle,{height:35}]}>
                                            <View style={styles.technician_view}>
                                                <Text style={this.state.view_one?styles.sel_service_name:styles.service_name}> {'₹'} {JSON.parse(item).price} </Text>
                                            </View>
                                        </View>}
                            </TouchableOpacity>

                        })}


                    </ScrollView>
                    {this.state.empty_price?
                        <Text style={styles.error_message}>Price is Mandatory</Text>:null}

                <View style={styles.note_style}>


                    <Text style={styles.term_text}> Note:- Cake Ordered before 7 PM, will deliver next day.</Text>
                    <Text style={styles.term_text}> All Cake are Eggless.</Text>
                </View>
                    <View style={{backgroundColor:'#333',width:'100%',marginBottom:'1%',height:0.5,marginTop:'1%'}}/>
                    <View style={this.state.empty_cake_name?styles.error_SectionStyle:styles.SectionStyle}>

                        <TextInput
                            ref='input_mobileno'
                            maxLength={10}
                            style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                            placeholder="Enter Name on Cake *"
                            keyboardType = 'text'
                            underlineColorAndroid="transparent"
                            onChangeText = {(TextInputText) =>  this.onEntercakename(TextInputText)} />

                    </View>

                    {this.state.empty_cake_name?
                        <Text style={styles.error_message}>Name on Cake is Mandatory</Text>:null}

                    <View style={styles.SectionStyle}>

                        <TextInput
                            ref='input_mobileno'
                            maxLength={10}
                            style={{flex:1,fontSize:12,padding:0,fontFamily:'Calibri Regular',}}
                            placeholder="Enter Occassion"
                            keyboardType = 'text'
                            underlineColorAndroid="transparent"
                            onChangeText = {(TextInputText) =>  this.setState({occassion_name:TextInputText})} />

                    </View>


                   <View style={this.state.chosenDate? {flexDirection:'row',marginBottom:5}:{flexDirection:'row',marginBottom:20}}>
                       <Text style={[styles.text_name,{fontSize:14,fontWeight:'bold',alignItems:'flex-start',justifyContent:'flex-start',textAlign:'left',}]}>
                           Delivery Date :
                       </Text>

                       <DatePicker
                           style={{width: 200}}
                           date={this.state.date} //initial date from state
                           mode="date" //The enum of date, datetime and time
                           format="YYYY-MM-DD"
                           placeholder="Select Date"
                           minDate={this.state.today_date}
                           maxDate="7020-12-31"
                           confirmBtnText="Confirm"
                           cancelBtnText="Cancel"
                           customStyles={{
                               dateIcon: {
                                   position: 'absolute',
                                   left: 0,
                                   top: 4,
                                   marginLeft: 0
                               },
                               dateInput: {
                                   marginLeft: 36
                               }
                           }}
                           onDateChange={(date) => {this.setState({date: date,empty_date:false})}}
                       />

                     {/*  <DatePicker
                           defaultDate={parseFloat(this.state.today_date)}
                           minimumDate={parseFloat(this.state.today_date)}
                           maximumDate={new Date(7020, 12, 31)}
                           locale={"en"}
                           timeZoneOffsetInMinutes={undefined}
                           modalTransparent={false}
                           animationType={"fade"}
                           androidMode={"default"}
                           placeHolderText="Select date"
                           textStyle={{ color: "green" }}
                           placeHolderTextStyle={{ color: "#d3d3d3" }}
                           onDateChange={this.setDate}
                           disabled={false}
                       />
*/}
                   </View>
                    {this.state.empty_date?
                        <Text style={styles.error_message}>Date is Mandatory</Text>:null}
                        </Content>

                <View style={{flexDirection:'row',}}>

                    <Button  style={{backgroundColor:'#E1A7C5',width:'50%',padding:10,bottom:0,
                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                             onPress={()=>this.add_Record("1")}>
                        <Text style={{color:'#000',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                              onPress={()=>this.add_Record("1")}> Buy Now </Text>
                    </Button>

                    <Button  style={{backgroundColor:'#333333',width:'50%',padding:10,bottom:0, justifyContent:'center',alignItems:'center',textAlign:'center',}}
                             onPress={()=>this.add_Record("0")}>

                        <Text style={{color:'#fff',fontSize:12,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                              onPress={()=>this.add_Record("0")}>Add to Cart</Text>

                    </Button>
                </View>




            </Container>
        );
    }
}
