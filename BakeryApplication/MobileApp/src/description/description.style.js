export default {
    header_style:{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65},
    main_salon_name:{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'45%',color:'#fff',fontWeight:'bold',},
    text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#000',marginBottom:'1%'},
    no_error_text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#E1A7C5',},
    error_text_name:{fontSize:12,fontFamily:'Calibri Bold',padding:5,color:'#E1A7C5',},
    SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: '#000', height: 40, borderRadius: 5 , marginTop:'2%',marginLeft:10,marginRight:10,
        marginBottom:15,paddingLeft:10,},
    error_SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: 'red', height: 40, borderRadius: 5 , marginTop:'1%',marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:10,},
    number_SectionStyle:{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff',
        borderWidth: .5, borderColor: '#000', height: 40, borderRadius: 5 , marginTop:20,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:5,width:'18%'},
    input_number_SectionStyle:{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff',
        borderWidth: .5, borderColor: '#000', height: 40, borderRadius: 5 , marginTop:20,marginLeft:10,marginRight:10,
        marginBottom:5,paddingLeft:5,width:'65%'},
    ImageStyle: {padding: 5, margin: 5, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'},
    logo: {width: 200, height:200,},
    image_container: {justifyContent: 'center', alignItems: 'center',marginTop:20,marginBottom:10
    },
    term_text:{
        fontSize:12,
        fontFamily:'Calibri Bold',
        color:'#666',
        marginTop:'3%',
        alignItems:'center',
        justifyContent:'center',
        textAlign: 'center',
        margin:5,
    },
    review_text_style:{
        fontSize:13,
        color:'#000',
        fontFamily:'Calibri Bold',
        fontWeight:'bold'
    },
    review_btn_style:
        {

            height:30,
            marginTop:'1%',
            marginLeft:'18%',
            flexDirection:'row',

        },
    review_count:{
        margin:0
    },
    weight_btn_style:{


        borderRadius:5,
        width:'30%',
        height:80,
        margin:5,
        borderWidth:1,
        borderColor:'#666',
        padding:5,
        flexDirection:'column',

    },
    weight_kg:{
        fontFamily:'Calibri Bold',
        fontSize:12,
        color:'#666',
        paddding:2,
        textAlign:'center',
        fontWeight:'bold',


    },
    weight_rupees:{
        fontFamily:'Calibri Bold',
        fontSize:12,
        color:'#666',
        paddding:2,
        textAlign:'center',
        fontWeight:'bold',

    },
    technician_cardstyle:{
        width:80,
        height:55,
        margin:10,
        borderColor:'#000',
        borderRadius:5,
        borderWidth:1,
        alignItems:'center',
        paddingTop:5,
        paddingBotom:5,
    },
    selected_technician_cardstyle:{
        width:80,
        height:55,
        margin:10,
        borderColor:'#EF50A4',
        borderRadius:5,
        borderWidth:1,
        alignItems:'center',
        paddingTop:5,
        paddingBotom:5,
        backgroundColor:'#EF50A4',
    },

    technician_name:{fontSize:12,  fontFamily:'Lato-Bold',paddingTop:3,paddingLeft:3,paddingRight:3,paddingBottom:0,color:'#fff',textAlign:'center'},
    technician_salon_name:{fontSize:10,  fontFamily:'Lato-Semibold',padding:0,color:'#fff',textAlign:'center'},
    technician_view:{flexDirection:'column',width:'100%'},
    btn_style_new:{
        flexDirection:'column',
        padding:10,
    },
    service_name:{
        fontSize:14,
        fontFamily:'Lato-Medium',
        paddingTop:0,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        color:'#000',
        textAlign:'center'
    },
    sel_service_name:{
        fontSize:14,
        fontFamily:'Lato-Medium',
        paddingTop:0,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        color:'#fff',
        textAlign:'center'
    },
    note_style:{
        padding:3,
        justifyContent:'center',
        alignItems:'center',

    },
    error_message:{
        color:'red',
        paddingLeft:10,
        paddingRight:5,
        fontFamily:'Calibri Bold',
        paddingBottom:3,
        fontSize:14,
        marginBottom:10,
    },
}