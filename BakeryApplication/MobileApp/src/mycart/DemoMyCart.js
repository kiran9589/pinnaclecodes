import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, TouchableWithoutFeedback, Linking, Alert,Text,StatusBar,FlatList
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left } from 'native-base';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import RazorpayCheckout from 'react-native-razorpay';
import Textarea from 'react-native-textarea';

import styles from './mycart.style';
import Thankyou from "../thankyou/Thankyou";
import Toast from "react-native-simple-toast";

var Realm = require('realm');
let realm ;

export default class DemoMyCart extends Component<> {

    constructor(props) {

        super(props);
        realm = new Realm({ path: 'CakeDatabase.realm'});
        var Cart_Details = realm.objects('Cart_Details');
        var total=0;
        console.log("realm.objects('Cart_Details')",+JSON.stringify(realm.objects('Cart_Details')));
        for (let i = 0; i < Cart_Details.length; i++) {
            total= total+parseInt(Cart_Details[i].cart_price)
        }

        console.warn("Total=="+total);

        this.state = {
            isLoading: true,
            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,
            value:1,
            //dataSource: ds.cloneWithRows(Cart_Details),
            dataSource:Cart_Details,
            total_price:total,
            address: '',
            ErrorStatus_address: true,



        };
        console.log("Cart Item",+this.state.dataSource);
        console.warn("Cart Item",+this.state.dataSource);
    }

    onEnteraddress = (TextInputValue) =>{
        if(TextInputValue.trim() != 0){
            this.setState({address : TextInputValue, ErrorStatus_address : false,}) ;
        }else{
            this.setState({address : TextInputValue, ErrorStatus_address : true}) ;
        }
    }

    //this.selection_promo_btn_press("promo_press");


    placeorder=()=>{
        if(this.state.ErrorStatus_address===true)
        {
            alert("Enter Valid Address");
        }
        else {
            this.callforPaymentgateway();
        }
    }

    /*=============================
    * Update Quantity
    * =============================*/


    plusupdateQuantity=(cart_id)=>
    {

        realm.write(() => {

            var obj = realm
                .objects('Cart_Details')
                .filtered('cart_cake_id =' + cart_id);

            if (obj.length > 0) {
                obj[0].cart_quantity=(parseInt(obj[0].cart_quantity)+1).toString();
                obj[0].cart_price=(parseInt(obj[0].Base_price)*parseInt(obj[0].cart_quantity)).toString();

            }
        });
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });


        var Cart_Details = realm.objects('Cart_Details');
        var total=0;
        for (let i = 0; i < Cart_Details.length; i++) {
            total= total+parseInt(Cart_Details[i].cart_price)
        }
        this.setState({
            dataSource: ds.cloneWithRows(Cart_Details),
            total_price:total,
        })
    }

    minus_update_quantity=(cart_id)=>{

        realm.write(() => {

            var obj = realm
                .objects('Cart_Details')
                .filtered('cart_cake_id =' + cart_id);

            if (obj.length > 0) {
                if(parseInt(obj[0].cart_quantity)>1)
                {
                    obj[0].cart_quantity=(parseInt(obj[0].cart_quantity)-1).toString();
                    obj[0].cart_price=(parseInt(obj[0].Base_price)*parseInt(obj[0].cart_quantity)).toString();
                }
                else
                {

                    Toast.show("Product Quantity Should Not Be Zero", Toast.SHORT);
                }

            }
        });
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });


        var Cart_Details = realm.objects('Cart_Details');
        var total=0;
        for (let i = 0; i < Cart_Details.length; i++) {
            total= total+parseInt(Cart_Details[i].cart_price)
        }
        this.setState({
            dataSource: ds.cloneWithRows(Cart_Details),
            total_price:total,
        })
    }


    /*==============================
    * Start Payment Gateway
    *==============================*/
    callforPaymentgateway=()=>{
        var options = {
            description: 'Demo Payment Gateway',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_IJ4OY7kZ9f3S0O',
            amount: '100',
            name: 'Arjun',
            prefill: {
                email: 'arjundalal38@gmail.com',
                contact: '918793694590',
                name: 'Bekary App'
            },
            theme: {color: '#F37254'}
        }
        RazorpayCheckout.open(options).then((data) => {
            // handle success
            // alert(`Success: ${data.razorpay_payment_id}`);
            console.log(`Success: ${data.razorpay_payment_id}`);
            this.props.navigation.navigate('Thankyou');

        }).catch((error) => {
            // handle failure
            alert(`Error: ${error.code} | ${error.description}`);
        });
    }
    selectionOnPress(userType) {
        this.setState({ selected_payment_btn: userType });
    }


    selection_promo_btn_press() {
        this.props.navigation.navigate('MyCartOffer');
        this.setState({ selected_promo_btn: "promo_press" });
    }


    // End Payment Gateway
    onSelect(index, value){
        this.setState({
            text: `${value}`
        })
        this.setPaymentModalVisible(false)
        this.selectionOnPress("press");
    }
    setPaymentModalVisible(visible) {

        this.setState({paymentmodal: visible});
    }
    FunctionToOpenMobileNo = () => {

        alert("The Data WIll comming soon .....");
    }

    handleBackPress = () => {
        this.props.navigation.navigate('DescriptionScreen');
    }
    navigateScreen=()=>{
        this.setPaymentModalVisible(false),
            this.props.navigation.navigate('Thankyou');
    }
    render() {
        return(

            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}>
                            <Icon name='arrow-back' />
                        </Button>
                        <Image source={require('../../images/logo_app.png')}
                               style={{height:60,width:100,}}  />
                    </Left>
                    <Body style={{flexDirection:'row'}}>
                    <Title style={styles.main_salon_name}> My Cart</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content>
                    <ScrollView>

                        <FlatList
                            dataSource={this.state.dataSource}
                            keyExtractor={(item, index) => index}
                            renderItem={({ item }) =>
                                <View style={[styles.cardstyle,{borderRadius:10}]}>

                                    <View style={{flexDirection:'row',paddingTop:0,paddingLeft:10,paddingRight:10,paddingBottom:10}}>
                                        <Image source={{uri:'http://3.13.212.113:3000'+item.cart_image}}
                                               style={{height:65,width:65,padding:5}} />
                                        <View style={{flex:1,flexDirection:'column'}}>
                                            <Text style={styles.textname} numberOfLines={1}> {item.cart_name} </Text>


                                            <View style={{flexDirection:'row',}}>
                                                <View style={{marginLeft:'10%',top:5}}>
                                                    {/*    <TouchableOpacity style={{borderColor:'green',borderRight:1}}>
                                           <Image source={require('../../images/styllon_new_minus.png')}
                                                  style={{width:20,height:20,bottom:2,}} />
                                       </TouchableOpacity>
                                       <Text style={{bottom:3,right:2}}> 1</Text>
                                       <TouchableOpacity>
                                           <Image source={require('../../images/styllon_new_plus.png')}
                                                  style={{width:20,height:20,bottom:2,left:3,borderLeftWidth:1}} />
                                       </TouchableOpacity>*/}


                                                    <View style={{flexDirection:'row',padding:2,width:'100%',height:30,borderRadius:3,borderColor:'#000',
                                                        borderWidth: .5,backgroundColor:'#fff',alignItems:'center',}}>

                                                        <TouchableOpacity onPress={()=>this.minus_update_quantity(item.cart_cake_id)}>

                                                            <Icon type="Entypo" name="minus"
                                                                  style={{fontSize: 20, color: 'green',paddingRight:'1%',width:30,paddingLeft:'2%',}}/>
                                                        </TouchableOpacity>

                                                        <Text syle={{color:'#ccc',fontWeight:'bold',fontSize:14,paddingRight:'1%'}}>{item.cart_quantity}</Text>

                                                        <TouchableOpacity onPress={()=>this.plusupdateQuantity(item.cart_cake_id)}>

                                                            <Icon type="Entypo" name="plus"
                                                                  style={{fontSize: 20, color: 'green',paddingRight:'1%',width:30,paddingLeft:'2%',}}/>
                                                        </TouchableOpacity>
                                                    </View>

                                                </View>


                                                <View style={{flexDirection:'row',paddingLeft:0,paddingTop:'3%',marginLeft:'27%'}}>
                                                    <Image source={require('../../images/styllon_black_rupee.png')}
                                                           style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                                    <Text style={styles.service_subprice}>{item.cart_price}</Text>
                                                </View>

                                            </View>


                                        </View>
                                    </View>

                                    <View style={styles.seperator_lines}></View>
                                </View>

                            }
                        />


                        {/* <View style={[styles.cardstyle,{borderRadius:10}]}>

                           <View style={{flexDirection:'row',padding:10}}>
                               <Image source={require('../../images/salon_main_image.jpg')}
                                      style={{height:65,width:65,padding:5}} />
                               <View style={{flex:1,flexDirection:'column'}}>
                                   <Text style={styles.textname}> Chocklate Cake Name </Text>


                                   <View style={{flexDirection:'row',}}>
                                   <View style={{marginLeft:'10%',top:5}}>
                                           <TouchableOpacity style={{borderColor:'green',borderRight:1}}>
                                           <Image source={require('../../images/styllon_new_minus.png')}
                                                  style={{width:20,height:20,bottom:2,}} />
                                       </TouchableOpacity>
                                       <Text style={{bottom:3,right:2}}> 1</Text>
                                       <TouchableOpacity>
                                           <Image source={require('../../images/styllon_new_plus.png')}
                                                  style={{width:20,height:20,bottom:2,left:3,borderLeftWidth:1}} />
                                       </TouchableOpacity>


                                   </View>


                                   <View style={{flexDirection:'row',paddingLeft:0,paddingTop:'3%',marginLeft:'27%'}}>
                                       <Image source={require('../../images/styllon_black_rupee.png')}
                                              style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                       <Text style={styles.service_subprice}>2000</Text>
                                   </View>

                                   </View>


                               </View>
                           </View>



                           <View style={styles.seperator_lines}></View>


                           <View style={{flexDirection:'row',padding:10}}>
                               <Image source={require('../../images/salon_main_image.jpg')}
                                      style={{height:65,width:65,padding:5}} />
                               <View style={{flex:1,flexDirection:'column'}}>
                                   <Text style={styles.textname}> Chocklate Cake Name </Text>


                                   <View style={{flexDirection:'row',}}>
                                       <View style={{marginLeft:'10%',top:5}}>
                                               <TouchableOpacity style={{borderColor:'green',borderRight:1}}>
                                           <Image source={require('../../images/styllon_new_minus.png')}
                                                  style={{width:20,height:20,bottom:2,}} />
                                       </TouchableOpacity>
                                       <Text style={{bottom:3,right:2}}> 1</Text>
                                       <TouchableOpacity>
                                           <Image source={require('../../images/styllon_new_plus.png')}
                                                  style={{width:20,height:20,bottom:2,left:3,borderLeftWidth:1}} />
                                       </TouchableOpacity>


                                       </View>


                                       <View style={{flexDirection:'row',paddingLeft:0,paddingTop:'3%',marginLeft:'27%'}}>
                                           <Image source={require('../../images/styllon_black_rupee.png')}
                                                  style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                           <Text style={styles.service_subprice}>2000</Text>
                                       </View>

                                   </View>


                               </View>
                           </View>

                            <View style={styles.seperator_lines}></View>

                           <View style={{flexDirection:'row',padding:10}}>
                               <Image source={require('../../images/salon_main_image.jpg')}
                                      style={{height:65,width:65,padding:5}} />
                               <View style={{flex:1,flexDirection:'column'}}>
                                   <Text style={styles.textname}> Chocklate Cake Name </Text>


                                   <View style={{flexDirection:'row',}}>
                                       <View style={{marginLeft:'10%',top:5}}>
                                               <TouchableOpacity style={{borderColor:'green',borderRight:1}}>
                                           <Image source={require('../../images/styllon_new_minus.png')}
                                                  style={{width:20,height:20,bottom:2,}} />
                                       </TouchableOpacity>
                                       <Text style={{bottom:3,right:2}}> 1</Text>
                                       <TouchableOpacity>
                                           <Image source={require('../../images/styllon_new_plus.png')}
                                                  style={{width:20,height:20,bottom:2,left:3,borderLeftWidth:1}} />
                                       </TouchableOpacity>

                                       </View>


                                       <View style={{flexDirection:'row',paddingLeft:0,paddingTop:'3%',marginLeft:'27%'}}>
                                           <Image source={require('../../images/styllon_black_rupee.png')}
                                                  style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                           <Text style={styles.service_subprice}>2000</Text>
                                       </View>

                                   </View>


                               </View>
                           </View>

                           <View style={styles.seperator_lines}></View>
                            <View style={{flexDirection:'row',paddingTop:0,paddingLeft:10,paddingBottom:5,}}>
                                <Text style={[styles.style_name,{width:'68%'}]}> Subtotal</Text>
                                <View style={{flexDirection:'row',paddingLeft:0,paddingTop:5}}>
                                    <Image source={require('../../images/styllon_black_rupee.png')}
                                           style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                    <Text style={styles.service_subprice}>1100</Text>
                                </View>
                            </View>
                       </View>*/}

                        <View style={[styles.cardstyle,{marginTop:0,borderRadius:10,backgroundColor:'#fff'}]}>
                            <View style={{flexDirection:'row',paddingTop:10,paddingLeft:10,paddingRight:10,paddingBottom:0}}>
                                <View style={{flexDirection:'row',width:'90%'}}>
                                    <Image source={require('../../images/styllon_offer_icon.png')}
                                           style={{width:24,height:24}} />
                                    <Text style={this.state.selected_promo_btn === "promo_press"?styles.promocode_style:
                                        [styles.promocode_style,{marginBottom:10}]}
                                          onPress={()=>this.selection_promo_btn_press()}>Apply Coupon</Text>
                                </View>

                                <Image source={require('../../images/styllon_right_side-arrow.png')}
                                       style={{width:24,height:24}} />
                            </View>
                            { this.state.selected_promo_btn === "promo_press"?
                                <View style={{flexDirection:'row',paddingTop:5,paddingLeft:10,paddingRight:10,paddingBottom:0}}>
                                    <View style={{flexDirection:'row',width:'90%'}}>
                                        <Text style={
                                            [styles.promocode_style_code,{marginBottom:5}]}>STYLLON1254</Text>
                                    </View>

                                    <TouchableOpacity onPress={()=>this.setState({ selected_promo_btn: " " })}>
                                        <Image source={require('../../images/styllon_delete_promocode.png')}
                                               style={{width:24,height:24}} />
                                    </TouchableOpacity>
                                </View>
                                :null}



                        </View>

                        <View style={[styles.cardstyle,{marginTop:0,borderRadius:10}]}>
                            <View style={{flexDirection:'row',paddingTop:0,paddingLeft:10,paddingBottom:5,}}>
                                <Text style={[styles.style_name,{width:'65%'}]}> Subtotal</Text>
                                <View style={{flexDirection:'row',paddingLeft:10,paddingTop:5}}>
                                    <Image source={require('../../images/styllon_black_rupee.png')}
                                           style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                    <Text style={styles.service_subprice}>{this.state.total_price}</Text>
                                </View>
                            </View>
                            {this.state.selected_promo_btn === "promo_press"?
                                <View style={{flexDirection:'row',paddingTop:0,paddingLeft:10,paddingBottom:5,}}>
                                    <Text style={[styles.style_name,{width:'65%',color:'green'}]}> Discount</Text>
                                    <View style={{flexDirection:'row',paddingLeft:10,paddingTop:5}}>
                                        <Image source={require('../../images/styllon_black_rupee.png')}
                                               style={{width:18,height:18,paddingRight:10,paddingTop:10}} />
                                        <Text style={[styles.service_subprice,{color:'green'}]}>105</Text>
                                    </View>
                                </View> :null
                            }
                            <View style={{flexDirection:'row',paddingTop:0,paddingLeft:10,paddingBottom:5,}}>
                                <Text style={[styles.grand_style_name,{width:'65%'}]}> Grand Total</Text>
                                <View style={{flexDirection:'row',paddingLeft:10,paddingTop:5}}>
                                    <Image source={require('../../images/styllon_black_rupee.png')}
                                           style={{width:18,height:18,paddingRight:10,paddingTop:20,top:5,}} />
                                    <Text style={styles.grand_service_subprice}>{this.state.total_price}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.cardstyle,{marginTop:0,borderRadius:10,backgroundColor:'#fff'}]}>
                            <View style={{flexDirection:'row',paddingTop:10,paddingLeft:10,paddingRight:10,paddingBottom:0}}>
                                <View style={{flexDirection:'row',width:'90%'}}>
                                    <Image source={require('../../images/styllon_black_rupee.png')}
                                           style={{width:24,height:24}} />
                                    <Text style={this.state.selected_payment_btn === "press"?
                                        styles.promocode_style:[styles.promocode_style,{marginBottom:10}]}>Payment Mode</Text>

                                </View>
                            </View>
                            <Text style={
                                [styles.promocode_style,{color:'green',marginLeft:'10%'}]}>Online</Text>
                        </View>

                        <View style={[styles.cardstyle,{marginTop:0,borderRadius:10,backgroundColor:'#fff'}]}>
                            <View style={{flexDirection:'column',paddingTop:5,paddingLeft:15,paddingRight:10,paddingBottom:0}}>
                                <Text style={{fontWeight:'bold',fontSize:14,padding:0,color:'#333333'}}>Delivering Cake To</Text>

                                <View style={{flexDirection:'row'}}>
                                    <View style={styles.SectionStyle}>

                                        <TextInput
                                            ref='input_mobileno'
                                            style={{flex:1,fontSize:14,padding:0,fontFamily:'Calibri Regular',fontWeight:'bold'}}
                                            placeholder="Enter Delivery Address "
                                            keyboardType = 'text'
                                            autoFocus={false}
                                            underlineColorAndroid="transparent"
                                            onChangeText = {(TextInputText) =>  this.onEnteraddress(TextInputText)} />
                                    </View>
                                    <Text style={{fontWeight:'bold',fontSize:14,padding:5,color:'green',marginTop:'1%'}}>Change</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>

                <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,
                    justifyContent:'center',alignItems:'center',textAlign:'center', }} onPress={()=> this.placeorder()}>
                    <Text style={{color:'#fff',fontSize:17,fontFamily:'Calibri Regular',}}
                          onPress={()=>this.placeorder()}>Place Order</Text>

                </Button>
                <Modal transparent = {true}
                       visible = {this.state.paymentmodal}
                       onRequestClose={() => this.setPaymentModalVisible(false)}
                       onBackdropPress={()=>this.setPaymentModalVisible(false)}>

                    <TouchableWithoutFeedback onPress={() => this.setPaymentModalVisible(false)}>
                        <View style={styles.payment_modal_style}>

                            <View style={[styles.Payment_Card_Style,{borderRadius:10}]}>
                                <Text style={styles.payment_main_text}>Enter Delivery Address </Text>


                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>



                {/*Promo Code Modal*/}
                <Modal transparent = {true}
                       visible = {this.state.promomodal}
                       onRequestClose={() => this.setPromoModalVisible(false)}
                       onBackdropPress={()=>this.setPromoModalVisible(false)}>

                    <TouchableWithoutFeedback onPress={() => this.setPromoModalVisible(false)}>
                        <View style={styles.payment_modal_style}>
                            <Text style={{fontSize:16,fontFamily:'Lato-Bold',color:'#fff',textAlign:'center'}}>Apply Coupons</Text>
                            <View style={{flexDirection:'row'}}>

                                <View style={styles.TextInputStyleClass}>
                                    <TextInput style={styles.textinputsstyless}
                                               underlineColorAndroid = "transparent"
                                               placeholder = "Enter Promo Code"
                                               placeholderTextColor = "#000"
                                               autoCapitalize = "none"/>

                                </View>
                                <Button transparent iconRight light style={{marginTop:5,haight:20,}}
                                        onPress={()=>this.promoclose()}>
                                    <Text style={{fontSize:16,fontFamily:'Lato-Bold',color:'#fff'}}
                                          onPress={()=>this.promoclose()}>Apply</Text>
                                </Button>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/*Promo Code Modal End*/}







            </Container>

        );

    }
}
