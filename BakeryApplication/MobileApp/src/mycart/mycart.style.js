export default {
    header_style:{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65},
    main_salon_name:{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'20%',color:'#fff',fontWeight:'bold',},
    textname: {fontSize:15,  marginTop:5, paddingLeft:8,fontFamily:'Calibri Bold',color:'#000',},
    style_name: {fontSize:14, marginTop:5,fontFamily:'Calibri Bold',color:'#000'},
    grand_style_name: {fontSize:20, marginTop:5,fontFamily:'Calibri Bold',color:'#000'},
    textaddresss: {fontSize:14, padding:3, paddingLeft:10,paddingRight:10,fontFamily:'Calibri Regular',width:'90%'},
    seperator_line: {width:'100%',height:1,backgroundColor:'gray'},
    seperator_lines: {width:'100%',height:1,backgroundColor:'#cccccc'},
    cardstyle:{width:'95%', height:'auto', margin:10,},
    service_subprice:{color:'#666',fontFamily:'Calibri Bold',fontSize:12,paddingLeft:5,},
    grand_service_subprice:{color:'gray',fontFamily:'Calibri Bold',fontSize:18,paddingLeft:5,},
    promocode_style:{color:'#CE3152',paddingLeft:15,},
    promocode_style_code:{color:'green',marginLeft:'10%',fontSize:14,fontFamily:'Calibri Bold'},
    note_style:{fontSize:12,fontFamily:'Calibri Regular',paddingTop:0,paddingLeft:10,paddingRight:10,textAlign:'center',},
    main_client_name:{fontSize:16,fontFamily:'Calibri Bold',textAlign:'center',marginTop:10,marginBottom:5,},
    final_date:{
        fontSize:14,
        fontFamily:'Calibri Bold',
    },
    final_time:{
        fontSize:14,
        fontFamily:'Calibri Bold',
    },
    payment_modal_style:{
        flex: 1,
        width: '100%',
        height: 'auto',
        alignContent:'flex-start',
        justifyContent:'flex-start',
        alignItems:'flex-start',
        backgroundColor:'rgba(0,0,0,0.7)',
        padding:10,
        marginTop:'12%'

    },

    Payment_Card_Style:{
        position: 'absolute',
        width: '95%',
        marginTop:65,
        height: 150,
        backgroundColor:'#fff',
        marginLeft:10,
        marginRight:10,
    },
    Promo_Card_Style:{
        position: 'absolute',
        width: '95%',
        marginTop:'25%',
        height: 140,
        marginLeft:20,
        marginRight:10,
    },
    payment_option_text:{
        fontSize:15,
        fontFamily:'Calibri Bold'

    },
    payment_main_text:{
        fontSize:16,
        fontFamily:'Calibri Bold',
        textAlign:'center',
        margin:5,
        padding:10,
    },


    imagestyless:{
        width:20,
        height:20,
        marginTop:7,
        marginLeft:10,

    },
    textinputsstyless: {

        paddingTop:8,
        color:'gray',
        fontSize:13,
        paddingLeft:10,
        fontFamily:'Calibri Bold',

    },
    promo_code_style:{
        color:'#000',
        fontSize:14,
        fontFamily:'Calibri Bold'
    },
    promo_code_apply_style:{
        fontFamily:'Calibri Bold',
        fontSize:14,
        color:'green'
    },
    promo_code_detail:{
        fontSize:14,
        fontFamily:'Calibri Bold',
        color:'#000000',
        textAlign: 'center',
        top:5,
    },
    promo_code_description:{
        fontSize:12,
        fontFamily:'Lato-Semibold',
        color:'gray',
        textAlign: 'justify',
        padding:10,

    },

    TextInputStyleClass:{
        flexDirection:'row',
        width:'82%',
        height: 38,
        borderWidth: 0.2,
        borderColor: '#000',
        borderRadius: 6 ,
        margin:10,
        backgroundColor : "#FFFFFF"

    },


    textareaContainer:{
        backgroundColor: '#fff',
        borderWidth: .5,
        width:'90%',
        borderColor: '#000',
        height: 100,
        borderRadius: 5 ,
        marginTop:0,
        marginLeft:10,
        marginRight:10,
        marginBottom:5,
        paddingLeft:10,
    },
    textarea:{
        fontSize:14,
    },
    SectionStyle: {flexDirection: 'row', backgroundColor: '#fff',
         height: 45, borderRadius: 10 , marginTop:0,marginRight:10,
        marginBottom:5,paddingLeft:0,width:'75%'},


    /*Offer Styles*/
    main_card_style:{
        borderRadius:5,
        width:'100%',
        height:'auto',
        padding:5,
        backgroundColor:'#ccc',

    },
    card_body_style:{
        padding:5,
    },
    main_offer_style:{
        textAlign:'left',
        fontSize:15,
        fontWeight:'bold',
        color:'#474A51',
        paddingTop:'1%',
        paddingLeft:0,
        paddingRight:5,
        paddingBottom:2,
    },
    submain_offer_style: {
        textAlign:'left',
        fontSize:12,
        color:'#333',
        paddingTop:1,
        paddingLeft:0,
        paddingRight:5,
        paddingBottom:1,

    },
    sub_offer_style:{
        textAlign:'left',
        fontSize:12,
        color:'#333',
        paddingLeft:0,
        paddingRight:5,
        paddingBottom:5,
    },

    more_btn:{
        color:'#7B98C1',
        fontSize:15,
        paddingTop:3,
        fontWeight:'bold',
        paddingLeft:0,
        paddingRight:3,
        paddingBottom:5,
    },

    underline_view:{
        width:'100%',
        height:0.5,
        backgroundColor:'#666',
        margin:5,
    },
    promo_code:{
        flexDirection:'row',
        padding:10,
        height:55,
        width:'auto',

    },
    first_offer:{
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:0,
        paddingRight:5,
        backgroundColor:'#FFFAE6',
        borderColor:'#ccc',
        borderWidth:0.5,
        borderTopRightRadius:5,
        borderBottomRightRadius:5,
        justifyContent:'center',
        alignItems:'center',
    },
    second_offer:{
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:5,
        paddingRight:5,
        backgroundColor:'#FFFAE6',
        borderColor:'#ccc',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        borderWidth:0.5,
        borderRadius:5,
        marginLeft:-7,
    },
    promo_code_text_details:{
        textAlign:'center',
        color:'#000',
        fontSize:14,
        fontWeight:'bold'
    },
    error_message:{
        color:'red',
        paddingLeft:10,
        paddingRight:5,
        fontFamily:'Calibri Bold',
        paddingTop:3,
        paddingBottom:3,
        fontSize:14,
        marginBottom:10,
    },

    address_SectionStyle: {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderWidth: .5,
        borderColor: '#000', height: 40, borderRadius: 5 , marginTop:0,marginLeft:10,marginRight:10,
        marginBottom:10,paddingLeft:10,},

    text_name:{fontSize:12,fontFamily:'Calbri -Bold',padding:5,color:'gray'},

    service_name:{
        fontSize:14,
        fontFamily:'Lato-Medium',
        paddingTop:0,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        color:'#000',
        textAlign:'center'
    },
    sel_service_name:{
        fontSize:14,
        fontFamily:'Lato-Medium',
        paddingTop:0,
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        color:'#fff',
        textAlign:'center'
    },

    technician_cardstyle:{
        width:80,
        height:55,
        margin:10,
        borderColor:'#000',
        borderRadius:5,
        borderWidth:1,
        alignItems:'center',
        paddingTop:5,
        paddingBotom:5,
    },
    selected_technician_cardstyle:{
        width:80,
        height:55,
        margin:10,
        borderColor:'#EF50A4',
        borderRadius:5,
        borderWidth:1,
        alignItems:'center',
        paddingTop:5,
        paddingBotom:5,
        backgroundColor:'#EF50A4',
    },
}