/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar, PermissionsAndroid, ListView, Image,TouchableOpacity} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text, Title, Left, Button, Icon, Right} from 'native-base';

import styles from './emptycart.style';
import Dashboard from "../dashboard/Dashboard";

type Props = {};
export default class EmptyCartScreen extends Component<Props> {

    onbackpress=()=>{
        this.props.navigation.navigate('MyDrawerNavigator');
    }
    render() {
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                    <Title style={styles.main_salon_name}>Empty Cart</Title>

                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={styles.container}>
                        <Image
                               source={require('../../images/sad_smilee.png')}
                               style={{width:100,height:100}} />

                        <Text style={[styles.empty_vart_text_one,{fontSize:22,fontWeight:'bold',padding:10,}]}> Your cart is empty </Text>
                        <Text style={[styles.empty_vart_text_one,{fontSize:16,padding:5}]}> Add cakes to it now. </Text>


                        <Button  style={{backgroundColor:'#CE3152',width:'75%',paddingTop:5,paddingBottom:10,bottom:0,marginLeft:'15%',marginTop:'5%',
                            justifyContent:'center',alignItems:'center',textAlign:'center',height:60}}
                            onPress={()=>this.props.navigation.navigate('Category')}>

                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 14,
                                        fontWeight: 'bold',
                                        fontFamily: 'Calibri Bold',
                                    }}
                                          onPress={()=>this.props.navigation.navigate('Category')}>Order Now </Text>

                        </Button>


                    </View>

                </Content>

            </Container>

        );
    }
}

