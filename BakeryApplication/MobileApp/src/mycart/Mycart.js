import React, { Component } from 'react';
import {
    Platform, StyleSheet, View, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity,
    ActivityIndicator, Modal, TouchableWithoutFeedback, Linking, Alert,Text,StatusBar,FlatList
} from 'react-native';
import { Container, Header, Item, Input, Icon, Button,Right,Content,Body,Title,Left, Card, CardItem,Badge} from 'native-base';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import RazorpayCheckout from 'react-native-razorpay';
import Textarea from 'react-native-textarea';

import styles from './mycart.style';
import Thankyou from "../thankyou/Thankyou";
import Toast from "react-native-simple-toast";
import Snackbar from 'react-native-snackbar';
import EmptyCartScreen from "./EmptyCartScreen";

var Realm = require('realm');
let realm ;

export default class Mycart extends Component<> {


    /*============================
    * Constructor Called
    * ===========================*/
    constructor(props) {

        super(props);
        try{
            realm = new Realm({ path: 'CakeDatabase.realm' });
            var Cart_Details = realm.objects('Cart_Details');

            var total=0;
            for (let i = 0; i < Cart_Details.length; i++) {
                var cart_size=Cart_Details[i];
                total= total+parseInt(Cart_Details[i].cart_price)
            }


        }
        catch (e) {
            this.setState({
                dataSource: [],
                //dataSource:[],
                total_price:0,
                grand_total_price:0,
            })
        }
        this.state = {
            isLoading: true,
            paymentmodal:false,
            promomodal:false,
            selected_payment_btn:null,
            selected_promo_btn:null,
            value:1,
            dataSource: Cart_Details,
            //dataSource:[],
            total_price:total,
            grand_total_price:total,
            //total_price:555,
            address: '',
            ErrorStatus_address: false,
            ErrorStatus_reedem_pt: false,
            promo_code_value:null,
            promo_code_name:'',
            offer_id:null,
            btn_press_val:'',
            payment_id:'',
            payment_status:'',
            offer_type:'',
            point_use:0,
            user_id:'',
            rewardPoints:'',
            referalCode:'',
            reedem_pt:0,
            click_reedem_btn:false,
            open_address_modal:false,
            flatname:'',
            city:'',
            pincode:'',
            state:'',
            country:'',
            ErrorStatus_flatname : false,
            ErrorStatus_city : false,
            ErrorStatus_pincode : false,
            ErrorStatus_state : false,
            ErrorStatus_country : false,
            address_btn_type:'',
            address_data:[],
            user_address:'',
            user_city:'',
            user_state:'',
            user_pincode:'',
            no_login:false,

        };

    }

    /*========================
    * Open Promo Modal
    * ========================*/
    openPromoModal=()=>{
        this.setState({
            promomodal:true,
            rewardPoints:parseInt(this.state.rewardPoints)+parseInt(this.state.reedem_pt),
            grand_total_price:parseInt(this.state.grand_total_price)+parseInt(this.state.reedem_pt),
            reedem_pt:0,
            place_reedem_pt:'',


        })
    }
    close_promo_Modal=()=>{
        this.setState({
            promomodal:false
        })
    }

    /*===============================
    * ComponentDidMount Method
    * ===============================*/
    componentDidMount(){


        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.offerDetails();
                if (realm.objects('Cart_Details').length <= 0) {
                    this.props.navigation.navigate('EmptyCartScreen');
                }
                else {
                    try {
                        realm = new Realm({path: 'CakeDatabase.realm'});
                        var User_Details = realm.objects('User_Details');
                        var mobile_number = User_Details[0].mobile_number;
                        this.setState({no_login:false});
                        this.getDateTime();
                        this.willFocusSubscription = this.props.navigation.addListener(
                            'willFocus',
                            () => {
                                this.getDateTime();
                                this.getuserDetails(mobile_number);

                            }
                        );

                        this.getuserDetails();

                    }
                    catch (e) {
                        this.setState({
                            no_login: true,
                        })
                    }
                }
            });

    }
/*==========================
* get Date and Time
* ==========================*/
getDateTime = () =>{
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    var full_date=year+"-"+month+"-"+date+"T"+hours+":"+min+":"+sec
    this.setState({complete_date:full_date})
}
/*=====================
* get User Address
* ======================*/
getAddress = (user_id) => {
    //fetch('http://3.13.212.113:3000/user/address/' + user_id)
    fetch('http://3.13.212.113:3000/user/address/'+user_id)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                isLoading: false,
                address_data:responseJson
            }, function () {
                if(responseJson.length === 0)
                {
                    this.setState({user_address:'No Address Selected'})
                    Snackbar.show({
                        title: 'Please Mention atleast one delivery addresss.',
                        duration: Snackbar.LENGTH_INDEFINITE | Snackbar.LENGTH_SHORT ,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                }
                else {


                    this.state.address_data.map((item, index) => {
                        if (item.is_default == true) {
                            // this.setState({sel_id:item.id});
                            this.setState({
                                sel_index: index,
                                address_id: item.id,
                                user_address: item.address,
                                user_city: item.city,
                                user_state: item.state,
                                user_pincode: item.pincode.toString()
                            });
                        }

                    });
                }
            })
                .catch((error) => {
                    console.error(error);
                });
        });
}
    /*==============================
    * Reedem Point Text Input
    * ==============================*/
    onEnterreedempoint = (TextInputValue) =>{
        if(TextInputValue.trim() != 0){
            this.setState({reedem_pt : TextInputValue, ErrorStatus_reedem_pt : false,}) ;
        }else{
            this.setState({reedem_pt : TextInputValue, ErrorStatus_reedem_pt : true}) ;
        }
    }

    /*==============================
    * Address Checker
    * ==============================*/
    onEnteraddress = (TextInputValue) =>{
        if(TextInputValue.trim() != 0){
            this.setState({address : TextInputValue, ErrorStatus_address : false,}) ;
        }else{
            this.setState({address : TextInputValue, ErrorStatus_address : true}) ;
        }
    }

    //this.selection_promo_btn_press("promo_press");


    placeorder=()=>{
        if(this.state.ErrorStatus_address===true)
        {
            alert("Enter Delivery Addresss")
        }
        else {
            this.callforPaymentgateway();
        }
    }

    /*=============================
    * Update Quantity
    * =============================*/


    plusupdateQuantity=(cart_id)=>
    {

        realm.write(() => {

            var obj = realm
                .objects('Cart_Details')
                .filtered('cart_cake_id =' + cart_id);

            if (obj.length > 0) {
                obj[0].cart_quantity=(parseInt(obj[0].cart_quantity)+1).toString();
                obj[0].cart_price=(parseInt(obj[0].Base_price)*parseInt(obj[0].cart_quantity)).toString();

            }
        });

        var Cart_Details = realm.objects('Cart_Details');
        var total=0;
        for (let i = 0; i < Cart_Details.length; i++) {
            total= total+parseInt(Cart_Details[i].cart_price)
        }
        if(this.state.promo_code_value>0 && this.state.reedem_pt > 0)
        {
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total-this.state.promo_code_value-this.state.reedem_pt,
            })
        }
        else if(this.state.promo_code_value>0)
        {
            console.warn("277");
            console.warn("total 277 ::",total);
            console.warn("this.state.promo_code_value ::",this.state.promo_code_value);
            console.warn("this.state.promo_code_value ::",total-this.state.promo_code_value);

            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total-this.state.promo_code_value,
            })
            console.warn("287::"+this.state.grand_total_price)
        }
        else if (this.state.reedem_pt > 0)
        {
            console.warn("288");
            console.warn(289,total);
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total-this.state.reedem_pt,
            })
        }
        else
        {
            console.warn("295");
            console.warn("total 299 ::",total);
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total,
            })
        }
        /*this.setState({
            dataSource: Cart_Details,
            total_price:total,
            grand_total_price:total,
        })*/
    }

    /*===========================
    * Delete Quantity
    * ===========================*/
    minus_update_quantity=(cart_id)=>{

        realm.write(() => {

            var obj = realm
                .objects('Cart_Details')
                .filtered('cart_cake_id =' + cart_id);

            if (obj.length > 0) {
                if(parseInt(obj[0].cart_quantity)>1)
                {
                    obj[0].cart_quantity=(parseInt(obj[0].cart_quantity)-1).toString();
                    obj[0].cart_price=(parseInt(obj[0].Base_price)*parseInt(obj[0].cart_quantity)).toString();
                }
                else
                {

                    Toast.show("Product Quantity Should Not Be Zero", Toast.SHORT);
                }

            }
        });
        /*  const ds = new ListView.DataSource({
              rowHasChanged: (r1, r2) => r1 !== r2,
          });
      */

        var Cart_Details = realm.objects('Cart_Details');
        var total=0;
        for (let i = 0; i < Cart_Details.length; i++) {
            total= total+parseInt(Cart_Details[i].cart_price)
        }
        if(this.state.promo_code_value>0 && this.state.reedem_pt > 0)
        {
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total-this.state.promo_code_value-this.state.reedem_pt,
            })
        }
       else  if(this.state.promo_code_value>0)
        {
            console.warn("343");
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total-this.state.promo_code_value,
            })
        }
        else if (this.state.reedem_pt > 0)
        {
            console.warn("352");
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total-this.state.reedem_pt,
            })
        }
        else
        {
            console.warn("361");
            this.setState({
                dataSource: Cart_Details,
                total_price:total,
                grand_total_price:total,
            })
        }

    }

    /*========================
    * Delete From Cart
    * ========================*/

    delete_item_from_cart = (cart_id) =>{
        realm.write(() => {

            if (realm.objects('Cart_Details').filtered('cart_productId =' + cart_id).length > 0) {
                realm.delete(realm.objects('Cart_Details').filtered('cart_productId =' + cart_id));
                Toast.show("Item Deleted Syccessfully", Toast.SHORT);
                var Cart_Details = realm.objects('Cart_Details');
                var total=0;
                for (let i = 0; i < Cart_Details.length; i++) {
                    total= total+parseInt(Cart_Details[i].cart_price)
                }
                this.setState({
                    dataSource: Cart_Details,
                    total_price:total,
                    grand_total_price:total,
                    open_address_modal:false,
                });
                if(realm.objects('Cart_Details').length<=0)
                {
                    this.props.navigation.navigate('EmptyCartScreen');
                    Snackbar.show({
                        title: 'Please Mention atleast one delivery addresss.',
                        duration: Snackbar.LENGTH_INDEFINITE | Snackbar.LENGTH_SHORT ,
                        color:'#fff',
                        action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                        },
                    });
                }
            }
        });
    }

/*===========================
* Apply Promo Code
* ==========================*/
applycode=(promo_code_value,promo_code_name,offer_id,btn_press_val,offer_type,promo_points)=>{

    if(offer_type!='points')
        {
            if(this.state.grand_total_price != 0)
            {
                this.setState({
                    promo_code_value:promo_code_value,
                    promo_code_name:promo_code_name,
                    offer_id:offer_id,
                    btn_press_val:btn_press_val,
                    grand_total_price:this.state.grand_total_price-promo_code_value,
                    offer_type:offer_type,
                    point_use:this.state.reedem_pt
                })
            }
            else
            {
                this.setState({
                    rewardPoints:parseInt(this.state.rewardPoints)+parseInt(this.state.reedem_pt),
                    reedem_pt:0,
                    promo_code_value:promo_code_value,
                    promo_code_name:promo_code_name,
                    offer_id:offer_id,
                    btn_press_val:btn_press_val,
                    grand_total_price:this.state.total_price-promo_code_value,
                    offer_type:offer_type,
                    point_use:this.state.reedem_pt
                })
            }

        }
        else {
            var total_point=this.state.rewardPoints;
            if(this.state.grand_total_price === 0)
            {
                if (promo_code_value<this.state.total_price) {

                    this.setState({
                        promo_code_value: promo_code_value,
                        promo_code_name: promo_code_name,
                        offer_id: offer_id,
                        btn_press_val: btn_press_val,
                        grand_total_price: this.state.total_price - promo_code_value,
                        offer_type: offer_type,
                        point_use: promo_points,

                    });
                    Toast.show("Grand Total is Zero", Toast.SHORT);


                }
                else {

                    Toast.show("Offer Value should be greater than grand total", Toast.SHORT);

                }

            }
            else if(Math.abs(promo_points)<= total_point) {
                if (promo_code_value < this.state.grand_total_price) {
                    this.setState({
                        promo_code_value: promo_code_value,
                        promo_code_name: promo_code_name,
                        offer_id: offer_id,
                        btn_press_val: btn_press_val,
                        grand_total_price: this.state.grand_total_price - promo_code_value,
                        offer_type: offer_type,
                        point_use: promo_points,
                    });
                    Toast.show("Point Debited", Toast.SHORT);


                }
                else {
                    Toast.show("Offer Value should be greater than grand total", Toast.SHORT);

            }
            }
            else
            {

                Toast.show("Sorry You dont have Enough Points in your account", Toast.SHORT);
            }
        }


        this.close_promo_Modal();

    }


    /*===========================
    * Reddem Reward
    * ===========================*/

    reddem_Reward = () => {

        var total_point=this.state.rewardPoints;


        if(this.state.reedem_pt==0)
        {
            Snackbar.show({
                title: 'Reedem Point Should be greater than zero',
                duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                color: '#fff',
                action: {
                    title: 'OK',
                    color: 'green',
                    onPress: () => { /* Do something. */
                    },
                },
            });
        }

        else if(this.state.reedem_pt > this.state.grand_total_price)
        {
            Snackbar.show({
                title: 'Reedem Point Should be less than grand total',
                duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                color: '#fff',
                action: {
                    title: 'OK',
                    color: 'green',
                    onPress: () => { /* Do something. */
                    },
                },
            });
        }


        else if(this.state.reedem_pt <= total_point) {

            //Toast.show("Point Debited", Toast.SHORT);
            this.setState({
                place_reedem_pt:this.state.reedem_pt,
                reedem_press_val:"reedem_press",
                grand_total_price: this.state.grand_total_price-this.state.reedem_pt,
                rewardPoints:this.state.rewardPoints-this.state.reedem_pt,
                point_use:parseInt(this.state.point_use)-parseInt(this.state.reedem_pt),
            });
            Snackbar.show({
                title: 'Point Reedem Succseefully',
                duration:Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                color: '#fff',
                action: {
                    title: 'OK',
                    color: 'green',
                    onPress: () => { /* Do something. */
                    },
                },
            });

        }
        else {
            Snackbar.show({
                title: 'Sorry You Dont have enough point to Reedem',
                duration: Snackbar.LENGTH_INDEFINITE|Snackbar.LENGTH_SHORT,
                color: '#fff',
                action: {
                    title: 'OK',
                    color: 'green',
                    onPress: () => { /* Do something. */
                    },
                },
            });
        }
        this.setState({click_reedem_btn: false})
    }

deleteReedemPt = () =>{
        this.setState({
            rewardPoints:parseInt(this.state.rewardPoints)+parseInt(this.state.reedem_pt),
            grand_total_price:parseInt(this.state.grand_total_price)+parseInt(this.state.reedem_pt),
            reedem_pt:0,
            reedem_press_val:'',
            place_reedem_pt:'',


        })
}
 /*==============================
* Promo Code API Called
* ==============================*/

offerDetails = () =>{

        this.setState({offer_data:[]});
        return fetch('http://3.13.212.113:3000/offer/list',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        start: 0,
                        limit: 10
                    })

            }).then((response) => response.json()).then((response) =>
        {

            var totalCount =response.totalCount;
            var offer_record=response.offers;
            this.setState({
                totalCount:totalCount,
                offer_data:[...this.state.offer_data , ...offer_record],
                isLoading: false,
                refreshing:false,
                //dataSource: response.data.items,
            });

        }).catch((error) =>
        {
            console.error(error);

        });

}


    /*==============================
    * Start Payment Gateway
    *==============================*/
    callforPaymentgateway=()=>{

        var options = {
            description: 'Payment',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_IJ4OY7kZ9f3S0O',
            amount: '100',
            name: 'Couches',
            prefill: {
                email: 'arjundalal38@gmail.com',
                contact: '8830729622',
                name: 'Bekary App'
            },
            theme: {color: '#F37254'}
        }
        RazorpayCheckout.open(options).then((data) => {
            // handle success
            // alert(`Success: ${data.razorpay_payment_id}`);
            console.warn(`Success: ${data.razorpay_payment_id}`);
            //this.props.navigation.navigate('Thankyou');

            this.setState({
                payment_id:data.razorpay_payment_id,
                payment_status:'Success'
            })

            this.placeOrder_two(this.state.payment_status,this.state.payment_id);
        }).catch((error) => {
            // handle failure
            //alert(`Error: ${error.code} | ${error.description}`);

            this.setState({
                payment_id:data.razorpay_payment_id,
                payment_status:'Fail'
            })
            this.placeOrder_two(this.state.payment_status,this.state.payment_id);
        });
    }

    /*===================================
    * Demo Function (Low Preerence)
    * ===================================*/

    selectionOnPress(userType) {
        this.setState({ selected_payment_btn: userType });
    }


    selection_promo_btn_press() {
        this.props.navigation.navigate('MyCartOffer');
        this.setState({ selected_promo_btn: "promo_press" });
    }
    setPaymentModalVisible(visible) {

        this.setState({paymentmodal: visible});
    }
    FunctionToOpenMobileNo = () => {

        alert("The Data WIll comming soon .....");
    }

    handleBackPress = () => {
        this.props.navigation.navigate('DescriptionScreen');
    }
    navigateScreen=()=>{
        this.setPaymentModalVisible(false),
            this.props.navigation.navigate('Thankyou');
    }

    /*========================
     *onSelect Function
     * ======================= */
    // End Payment Gateway
    onSelect(index, value){
        this.setState({
            text: `${value}`
        })
        this.setPaymentModalVisible(false)
        this.selectionOnPress("press");
    }


    /*===============================
    * Delete Promo Code
    * ===============================*/

    deletepromocode=()=>{
        this.setState({ selected_promo_btn: " ",
            grand_total_price:this.state.grand_total_price+this.state.promo_code_value,
            promo_code_value:null,
            promo_code_name:'',
            offer_id:'',
            btn_press_val:'',
        })


    }


    /*===============================
    * get USer DEtails
    * ===============================*/

    getuserDetails = (mobile_number) => {

        //this.props.navigation.state.params.mobile_numberuserId
        return fetch('http://3.13.212.113:3000/user/details',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },

                body: JSON.stringify(
                    {
                        mobile_no:mobile_number,

                    })

            }).then((response) => response.json()).then((response) =>
        {
            this.getAddress(response.id);
            this.setState({
                user_id:response.id,
                user_name:response.name,
                user_mobile:response.mobile_no,
                user_email:response.email,
                rewardPoints:response.rewardPoints,
                referalCode:response.referalCode,
                isLoading: false,
            });

        }).catch((error) =>
        {
            this.setState({
                isLoading: false,
            });

        });

    }


/*=============================
* Place Order To Server
 * =============================*/
 placeOrder_two=(payment_status,tran_no)=>{

         fetch('http://3.13.212.113:3000/order/create',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                body: JSON.stringify(
                    {
                        //userId: this.state.user_id,
                        userId: this.state.user_id,
                        paymentMode: "CREDIT_CARD",
                        paymentStatus: payment_status,
                        transactionNum:tran_no,
                        paymentDate:this.state.complete_date,
                        totalAmount:this.state.grand_total_price,
                        orderDate:this.state.complete_date,
                        offerId:this.state.offer_id,
                        pointUsed:this.state.point_use,
                        productList:Array.from(this.state.dataSource)

                        /* productList:[
                             {"productId": 1,"weight":null,"quantity":1,"price": "20.00"},
                             {"productId": 4,"weight":null,"quantity":1,"price": "50.00"},
                             {"productId": 5,"weight":"1kg","quantity":1,"price": "250.00"}
                         ]*/

                    })

            }).then((response) => response.json()).then((response) =>
        {
            if(response!="There is some intruption whlie placing your order. Please try in while.")
            {

                realm.write(() => {

                    realm.delete(realm.objects('Cart_Details'));

                });
                this.props.navigation.navigate('SuccessScreen',{orderId:response.orderId});
                Toast.show("Order Placed", Toast.SHORT);


            }
            else
            {

                this.props.navigation.navigate('FailureScreen');
                Toast.show("Order Not Placed", Toast.SHORT);

            }

        }).catch((error) =>
        {
            console.error(error);

        });

}

    /*============================
    * TextInput Functions
    * ============================*/
    onEnterflatname = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({flatname : TextInputValues, ErrorStatus_flatname : false,}) ;
        }else{
            this.setState({flatname : TextInputValues, ErrorStatus_flatname: true}) ;
        }
    }
    onEntercity = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({city : TextInputValues, ErrorStatus_city : false,}) ;
        }else{
            this.setState({city : TextInputValues, ErrorStatus_city: true}) ;
        }
    }
    onEnterpincode = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({pincode : TextInputValues, ErrorStatus_pincode : false,}) ;
        }else{
            this.setState({pincode : TextInputValues, ErrorStatus_pincode: true}) ;
        }
    }
    onEnterstate = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({state : TextInputValues, ErrorStatus_state : false,}) ;
        }else{
            this.setState({state : TextInputValues, ErrorStatus_state: true}) ;
        }
    }
    onEntercountry = (TextInputValues) =>{
        if(TextInputValues.trim() != 0){
            this.setState({country : TextInputValues, ErrorStatus_country : false,}) ;
        }else{
            this.setState({country : TextInputValues, ErrorStatus_country: true}) ;
        }
    }


    /*======================
    * UI Statrted
    * ======================*/
    render() {
        return(

            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}}
                                onPress={()=> this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flexDirection:'row'}}>
                    <Title style={styles.main_salon_name}> My Cart</Title>
                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                {realm.objects('Cart_Details').length <= 0 ? null :
                    <Content>

                        {this.state.user_address != '' ?
                        <View style={[styles.cardstyle, {borderRadius: 10, backgroundColor: '#fff', padding: 10,}]}>
                            <Text style={[styles.displayAddressText, {fontWeight: 'bold'}]}
                                  numberOfLines={1}> {this.state.user_name} </Text>
                            <Text
                                style={styles.displayAddressText}> {this.state.user_address} {this.state.user_city} {this.state.user_state} {this.state.user_pincode}</Text>
                            <Text style={[styles.displayAddressText, {marginBottom: 5}]}
                                  numberOfLines={1}> {this.state.user_mobile} </Text>

                            <Button style={{
                                backgroundColor: '#333333',
                                width: '100%',
                                padding: 10,
                                bottom: 0,
                                justifyContent: 'center',
                                alignItems: 'center',
                                textAlign: 'center',
                            }}
                                    onPress={() => this.props.navigation.navigate('GetAllAddress', {userID: this.state.user_id})}>

                                <Text style={{
                                    color: '#fff',
                                    fontSize: 14,
                                    fontWeight: 'bold',
                                    fontFamily: 'Calibri Bold',
                                }}
                                      onPress={() => this.props.navigation.navigate('GetAllAddress', {
                                          userID: this.state.user_id,
                                          screenID: 0
                                      })}>Change or Add Address </Text>

                            </Button>

                        </View> : null }

                        <ScrollView>

                            <FlatList
                                data={this.state.dataSource}
                                renderItem={({item}) => (
                                    <View style={[styles.cardstyle, {borderRadius: 10}]}>

                                        <View style={{
                                            flexDirection: 'row',
                                            paddingTop: 0,
                                            paddingLeft: 10,
                                            paddingRight: 10,
                                            paddingBottom: 10
                                        }}>
                                            <Image source={{uri: 'http://3.13.212.113:3000' + item.cart_image}}
                                                   style={{height: 65, width: 65, padding: 5}}/>
                                            <View style={{flex: 1, flexDirection: 'column'}}>
                                                <Text style={[styles.textname, {fontWeight: 'bold'}]}
                                                      numberOfLines={1}> {item.cart_name} </Text>

                                                <View style={{flexDirection: 'row',}}>

                                                    <View style={{
                                                        flexDirection: 'row',
                                                        padding: 2,
                                                        width: '23%',
                                                        height: 30,
                                                        borderRadius: 3,
                                                        borderColor: '#000',
                                                        borderWidth: .5,
                                                        backgroundColor: '#fff',
                                                        alignItems: 'center',
                                                        marginLeft: '5%',
                                                        marginTop: '1%'
                                                    }}>

                                                        {item.cart_quantity != 1 ?
                                                            <TouchableOpacity
                                                                onPress={() => this.minus_update_quantity(item.cart_cake_id)}>

                                                                <Icon type="Entypo" name="minus"
                                                                      style={{
                                                                          fontSize: 20,
                                                                          color: 'green',
                                                                          paddingRight: '1%',
                                                                          width: 28,
                                                                          paddingLeft: '2%',
                                                                      }}/>
                                                            </TouchableOpacity> :

                                                            //<TouchableOpacity onPress={()=>this.delete_item_from_cart(item.cart_productId)}>
                                                            <TouchableOpacity onPress={() => this.setState({
                                                                open_address_modal: true,
                                                                delete_cardid: item.cart_productId
                                                            })}>

                                                                <Icon type="MaterialIcons" name="delete"
                                                                      style={{
                                                                          fontSize: 20,
                                                                          color: 'red',
                                                                          paddingRight: '1%',
                                                                          width: 28,
                                                                          paddingLeft: '2%',
                                                                      }}/>
                                                            </TouchableOpacity>
                                                        }

                                                        <Text syle={{
                                                            color: '#ccc',
                                                            fontWeight: 'bold',
                                                            fontSize: 14,
                                                            paddingRight: '1%',
                                                            marginRight: 2,
                                                            backgroundColor: '#ccc',
                                                            width: 35
                                                        }}>{item.cart_quantity}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.plusupdateQuantity(item.cart_cake_id)}>

                                                            <Icon type="Entypo" name="plus"
                                                                  style={{
                                                                      fontSize: 20,
                                                                      color: 'green',
                                                                      paddingRight: '1%',
                                                                      width: 35,
                                                                      paddingLeft: '3%',
                                                                  }}/>
                                                        </TouchableOpacity>
                                                    </View>


                                                    <View style={{
                                                        flexDirection: 'row',
                                                        paddingLeft: 0,
                                                        paddingTop: '3%',
                                                        marginLeft: '27%'
                                                    }}>
                                                        <Image source={require('../../images/styllon_black_rupee.png')}
                                                               style={{
                                                                   width: 18,
                                                                   height: 18,
                                                                   paddingRight: 10,
                                                                   paddingTop: 10
                                                               }}/>
                                                        <Text style={styles.service_subprice}>{item.cart_price}</Text>
                                                    </View>

                                                </View>


                                            </View>
                                        </View>

                                        <View style={styles.seperator_lines}></View>
                                    </View>
                                )}
                            />

                            <View style={[styles.cardstyle, {marginTop: 0, borderRadius: 10, backgroundColor: '#fff'}]}>

                                {this.state.promo_code_name != '' ?


                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 10,
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        paddingBottom: 0
                                    }}>
                                        <View style={{flexDirection: 'row', width: '90%'}}>
                                            <Image source={require('../../images/styllon_offer_icon.png')}
                                                   style={{width: 24, height: 24}}/>
                                            <Text style={[styles.promocode_style, {
                                                marginBottom: 10,
                                                fontWeight: 'bold'
                                            }]}>{this.state.promo_code_name}</Text>
                                        </View>
                                        <TouchableOpacity onPress={() => this.deletepromocode()}>
                                            <Image source={require('../../images/styllon_delete_promocode.png')}
                                                   style={{width: 24, height: 24}}/>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <TouchableOpacity onPress={() => this.openPromoModal()}>

                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 10,
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        paddingBottom: 0
                                    }}>

                                        <View style={{flexDirection: 'row', width: '90%'}}>
                                            <Image source={require('../../images/styllon_offer_icon.png')}
                                                   style={{width: 24, height: 24}}/>
                                            <Text
                                                style={[styles.promocode_style, {marginBottom: 10, fontWeight: 'bold'}]}
                                                onPress={() => this.openPromoModal()}>Apply Coupon</Text>
                                        </View>

                                        <Image source={require('../../images/styllon_right_side-arrow.png')}
                                               style={{width: 24, height: 24}}/>
                                    </View>
                                    </TouchableOpacity>
                                }

                                {/* {this.state.btn_press_val === "promo_press"?
                                <View style={{flexDirection:'row',paddingTop:5,paddingLeft:10,paddingRight:10,paddingBottom:0}}>
                                    <View style={{flexDirection:'row',width:'90%'}}>
                                        <Text style={
                                            [styles.promocode_style_code,{marginBottom:5}]}>
                                            {this.state.promo_code_name}
                                        </Text>
                                    </View>

                                 <TouchableOpacity onPress={()=>this.deletepromocode()}>
                                    <Image source={require('../../images/styllon_delete_promocode.png')}
                                            style={{width:24,height:24}} />
                                 </TouchableOpacity>
                                </View>
                                :null}*/}


                            </View>

                            {/*Reddem Option*/}

                            {this.state.place_reedem_pt > 0 ?

                                <View style={[styles.cardstyle, {
                                    marginTop: 0,
                                    borderRadius: 10,
                                    backgroundColor: '#fff'
                                }]}>

                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 10,
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        paddingBottom: 0
                                    }}>
                                        <View style={{flexDirection: 'row', width: '90%'}}>
                                            <Image source={require('../../images/styllon_offer_icon.png')}
                                                   style={{width: 24, height: 24}}/>
                                            <Text
                                                style={[styles.promocode_style, {marginBottom: 10, fontWeight: 'bold'}]}
                                                onPress={() => this.setState({click_reedem_btn: true,})}>Reedem
                                                Amount</Text>
                                        </View>
                                        <Text style={{
                                            color: "#fff",
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            backgroundColor: '#333333',
                                            borderRadius: 50,
                                            paddingTop: 5,
                                            paddingLeft: 10,
                                            paddingRight: 10,
                                            paddingBottom: 5,
                                            top: -3
                                        }}>
                                            {this.state.rewardPoints}
                                        </Text>


                                        {/* <Image source={require('../../images/styllon_right_side-arrow.png')}
                                           style={{width:24,height:24}} />this.state.rewardPoints*/}
                                    </View>

                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 10,
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        paddingBottom: 0
                                    }}>
                                        <View style={{flexDirection: 'row', width: '90%'}}>
                                            <Image
                                                style={{width: 24, height: 24}}/>
                                            <Text style={[styles.promocode_style, {
                                                marginBottom: 10,
                                                fontWeight: 'bold'
                                            }]}>{this.state.reedem_pt}</Text>
                                        </View>
                                        <TouchableOpacity onPress={() => this.deleteReedemPt()}>
                                            <Image source={require('../../images/styllon_delete_promocode.png')}
                                                   style={{width: 24, height: 24}}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>


                                :

                                <View style={[styles.cardstyle, {
                                    marginTop: 0,
                                    borderRadius: 10,
                                    backgroundColor: '#fff'
                                }]}>

                                    {this.state.rewardPoints>0 ?
                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 10,
                                        paddingLeft: 10,
                                        paddingRight: 10,
                                        paddingBottom: 0
                                    }}>
                                        <View style={{flexDirection: 'row', width: '90%'}}>
                                            <Image source={require('../../images/styllon_offer_icon.png')}
                                                   style={{width: 24, height: 24}}/>
                                            <Text
                                                style={[styles.promocode_style, {marginBottom: 10, fontWeight: 'bold'}]}
                                                onPress={() => this.setState({click_reedem_btn: true,})}>Reedem Amount</Text>
                                        </View>
                                        <Text style={{
                                            color: "#fff",
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            backgroundColor: '#333333',
                                            borderRadius: 50,
                                            paddingTop: 5,
                                            paddingLeft: 10,
                                            paddingRight: 10,
                                            paddingBottom: 5,
                                            top: -3
                                        }}>
                                            {this.state.rewardPoints}
                                        </Text>


                                        {/* <Image source={require('../../images/styllon_right_side-arrow.png')}
                                           style={{width:24,height:24}} />this.state.rewardPoints*/}
                                    </View> : null }

                                    {/*TextInput on Button click*/}

                                    {
                                        this.state.click_reedem_btn ?
                                            <View style={{flexDirection: 'row'}}>
                                                <View style={styles.SectionStyle}>

                                                    <TextInput
                                                        style={{
                                                            flex: 1,
                                                            fontSize: 14,
                                                            paddingLeft: '15%',
                                                            fontFamily: 'Calibri Regular',
                                                            fontWeight: 'bold'
                                                        }}
                                                        placeholder="Enter Reedem Points "
                                                        keyboardType='numeric'
                                                        autoFocus={false}
                                                        underlineColorAndroid="transparent"
                                                        onChangeText={(TextInputText) => this.onEnterreedempoint(TextInputText)}/>
                                                </View>
                                                <Text style={{
                                                    fontWeight: 'bold',
                                                    fontSize: 14,
                                                    padding: 5,
                                                    color: '#CE3152',
                                                    marginTop: '1%'
                                                }}
                                                      onPress={() => this.reddem_Reward()}>Reedem</Text>
                                            </View>
                                            : null
                                    }

                                </View>
                            }


                            {/*Close Reddem Option*/}

                            <View style={[styles.cardstyle, {marginTop: 0, borderRadius: 10}]}>
                                <View style={{flexDirection: 'row', paddingTop: 0, paddingLeft: 10, paddingBottom: 5,}}>
                                    <Text style={[styles.style_name, {width: '65%'}]}> Subtotal</Text>
                                    <View style={{flexDirection: 'row', paddingLeft: 10, paddingTop: 5}}>
                                        <Image source={require('../../images/styllon_black_rupee.png')}
                                               style={{width: 18, height: 18, paddingRight: 10, paddingTop: 10}}/>
                                        <Text style={styles.service_subprice}>{this.state.total_price}</Text>
                                    </View>
                                </View>
                                {this.state.btn_press_val === "promo_press" ?
                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 0,
                                        paddingLeft: 10,
                                        paddingBottom: 5,
                                    }}>
                                        <Text
                                            style={[styles.style_name, {width: '65%', color: 'green'}]}> Discount</Text>
                                        <View style={{flexDirection: 'row', paddingLeft: 10, paddingTop: 5}}>
                                            <Image source={require('../../images/styllon_black_rupee.png')}
                                                   style={{width: 18, height: 18, paddingRight: 10, paddingTop: 10}}/>
                                            <Text style={[styles.service_subprice, {color: 'green'}]}>
                                                {parseInt(this.state.promo_code_value) + parseInt(this.state.reedem_pt)}
                                            </Text>
                                        </View>
                                    </View> : null
                                }
                                {this.state.btn_press_val === "promo_press" ?
                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 0,
                                        paddingLeft: 10,
                                        paddingBottom: 5,
                                    }}>
                                        <Text style={[styles.style_name, {
                                            width: '55%',
                                            color: 'green',
                                            marginLeft: '5%'
                                        }]}> Offer</Text>
                                        <View style={{flexDirection: 'row', paddingLeft: 20, paddingTop: 5}}>
                                            <Image source={require('../../images/styllon_black_rupee.png')}
                                                   style={{width: 18, height: 18, paddingRight: 10, paddingTop: 10}}/>
                                            <Text style={[styles.service_subprice, {color: 'green'}]}>
                                                {this.state.promo_code_value}
                                            </Text>
                                        </View>
                                    </View> : null
                                }
                                {this.state.reedem_press_val === "reedem_press" ?
                                    <View style={{
                                        flexDirection: 'row',
                                        paddingTop: 0,
                                        paddingLeft: 10,
                                        paddingBottom: 5,
                                    }}>
                                        <Text style={[styles.style_name, {
                                            width: '55%',
                                            color: 'green',
                                            marginLeft: '5%'
                                        }]}> Rewards</Text>
                                        <View style={{flexDirection: 'row', paddingLeft: 20, paddingTop: 5}}>
                                            <Image source={require('../../images/styllon_black_rupee.png')}
                                                   style={{width: 18, height: 18, paddingRight: 10, paddingTop: 10}}/>
                                            <Text style={[styles.service_subprice, {color: 'green'}]}>
                                                {this.state.reedem_pt}
                                            </Text>
                                        </View>
                                    </View> : null
                                }
                                <View style={{flexDirection: 'row', paddingTop: 0, paddingLeft: 10, paddingBottom: 5,}}>
                                    <Text style={[styles.grand_style_name, {width: '65%'}]}> Grand Total</Text>
                                    <View style={{flexDirection: 'row', paddingLeft: 10, paddingTop: 5}}>
                                        <Image source={require('../../images/styllon_black_rupee.png')}
                                               style={{
                                                   width: 18,
                                                   height: 18,
                                                   paddingRight: 10,
                                                   paddingTop: 20,
                                                   top: 5,
                                               }}/>
                                        <Text
                                            style={styles.grand_service_subprice}>{this.state.grand_total_price}</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={[styles.cardstyle, {marginTop: 0, borderRadius: 10, backgroundColor: '#fff'}]}>
                                <View style={{
                                    flexDirection: 'row',
                                    paddingTop: 10,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    paddingBottom: 0
                                }}>
                                    <View style={{flexDirection: 'row', width: '90%'}}>
                                        <Image source={require('../../images/styllon_black_rupee.png')}
                                               style={{width: 24, height: 24}}/>
                                        <Text style={this.state.selected_payment_btn === "press" ?
                                            styles.promocode_style : [styles.promocode_style, {marginBottom: 10}]}>
                                            Note - Only Online Payment Mode Available
                                        </Text>

                                    </View>
                                </View>
                            </View>

                        </ScrollView>
                    </Content>
                }

                {this.state.user_address === 'No Address Selected' ? null : realm.objects('Cart_Details').length<=0 ? null : this.state.no_login === true?
                    <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,
                        justifyContent:'center',alignItems:'center',textAlign:'center', }}
                             onPress={()=> this.props.navigation.navigate('MobileNo',{rotateID:3})}>
                        <Text style={{color:'#fff',fontSize:17,fontFamily:'Calibri Regular',}}
                              onPress={()=> this.props.navigation.navigate('MobileNo',{rotateID:3})}>Place Order</Text>

                    </Button>
                        :

                    this.state.grand_total_price!=0 ?
                    <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,
                        justifyContent:'center',alignItems:'center',textAlign:'center', }} onPress={()=> this.placeorder()}>
                        <Text style={{color:'#fff',fontSize:17,fontFamily:'Calibri Regular',}}
                              onPress={()=>this.placeorder()}>Place Order</Text>

                    </Button>

                    :
                    <Button  style={{backgroundColor:'#333333',width:'100%',padding:10,
                        justifyContent:'center',alignItems:'center',textAlign:'center', }} onPress={()=> this.placeOrder_two()}>
                        <Text style={{color:'#fff',fontSize:17,fontFamily:'Calibri Regular',}}
                              onPress={()=> this.placeOrder_two("Success","pay_0000")}>Place Order</Text>

                    </Button>

                }

                {/*Promo Code Modal*/}
                <Modal transparent = {false}
                       visible = {this.state.promomodal}
                       onRequestClose={() => this.close_promo_Modal()}
                       onBackdropPress={()=>this.close_promo_Modal()}>

                    <TouchableWithoutFeedback onPress={() => this.close_promo_Modal()}>
                        <FlatList

                            data={ this.state.offer_data}
                            renderItem={({item}) =>

                                <Card style={styles.main_card_style}>
                                    <CardItem>
                                        <Body style={styles.card_body_style}>
                                        <View style={styles.promo_code}>
                                            <View style={styles.second_offer}>
                                                <Text style={styles.promo_code_text_details}>{item.offerCode}</Text>
                                            </View>
                                            <TouchableOpacity
                                                onPress={()=>this.applycode(item.offerValue,item.offerCode,item.o_id,'promo_press',item.offerType,item.point)}>
                                                <View style={{marginLeft:'75%'}}>
                                                    <Text style={[styles.promo_code_text_details,{color:'green'}]}>Apply</Text>
                                                </View>
                                            </TouchableOpacity>

                                        </View>
                                        <Text style={styles.main_offer_style}>{item.offerDesc}</Text>
                                        <Text style={styles.submain_offer_style}>Use code {item.offerCode}  & {item.offerDesc}</Text>
                                        <Text style={styles.sub_offer_style}>Maximum Discount: {item.offerValue} {item.offerType}</Text>
                                        <Text style={styles.more_btn} onPress={()=>this.openoffermodal(item.offerCode,item.offerDesc)}>More Details</Text>
                                        </Body>
                                    </CardItem>
                                </Card>
                            }
                        />
                    </TouchableWithoutFeedback>
                </Modal>
                {/*Promo Code Modal End*/}

                {/*Save User Address*/}

                <Modal transparent = {true}
                       animated={'slide'}
                       visible = {this.state.open_address_modal}
                       onRequestClose={() => this.setState({open_address_modal:false})} >

                    <View style={{ backgroundColor:'rgba(0, 0, 0, 0.8)',paddingTop:'10%',flex:1,
                        justifyContent:'center',alignItems:'center' }}>

                        <View style={{backgroundColor:'#fff',borderRadius:0,margin:20}}>
                            <Text style={{textAlign:'center',fontSize:15,paddingTop:20,paddingBottom:20,paddingLeft:'12%',paddingRight:'12%',
                                fontWeight:'bold',fontFamily:'Calibri Bold',}}>
                                Are you sure you want to remove this item?
                            </Text>
                            <View style={{flexDirection:'row'}}>

                                <View style={{flexDirection:'row',}}>

                                    <Button  style={{backgroundColor:'#E1A7C5',width:'50%',padding:10,bottom:0,
                                        justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                    onPress={()=> this.delete_item_from_cart(this.state.delete_cardid)}>
                                        <Text style={{color:'#000',fontSize:14,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=> this.delete_item_from_cart(this.state.delete_cardid)}>
                                            OK </Text>
                                    </Button>

                                    <Button  style={{backgroundColor:'#333333',width:'50%',padding:10,bottom:0, justifyContent:'center',alignItems:'center',textAlign:'center',}}
                                             onPress={()=>this.setState({open_address_modal:false})}>

                                        <Text style={{color:'#fff',fontSize:12,fontWeight:'bold',fontFamily:'Calibri Bold',}}
                                              onPress={()=>this.setState({open_address_modal:false})}>Cancel</Text>

                                    </Button>
                                </View>



                            </View>
                        </View>
                    </View>
                </Modal>





            </Container>

        );

    }
}
