import React, { Component } from 'react';

import { Platform, StyleSheet, View, Text, Image, TouchableOpacity, Alert,StatusBar } from 'react-native';
import {StackNavigator} from 'react-navigation';


export default class Welcome extends Component<{}>
{

    constructor(){

        super();

        this.state={

            isVisible : true,

        }

    }

    Hide_Splash_Screen=()=>{

        this.setState({
            isVisible : false

        });

    }

    componentDidMount(){
        // Start counting when the page is loaded
        this.timeoutHandle = setTimeout(()=>{
            // Add your logic for the transition
            this.props.navigation.navigate('MyDrawerNavigator') // what to push here?
        }, 3000);
    }


    render()
    {
        let Splash_Screen = (

            <View style={styles.SplashScreen_RootView}>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />

                <View style={styles.SplashScreen_ChildView}>

                    {/* Put all your components Image and Text here inside Child view which you want to show in Splash Screen. */}

                    <Image source={require('../../images/logo_app.png')}
                           style={{width:'100%', height:150, }} />

                </View>

            </View> )

        return(

            <View style = { styles.MainContainer }>

                {
                    (this.state.isVisible === true) ? Splash_Screen : null
                }


            </View>

        );
    }
}



const styles = StyleSheet.create(
    {
        MainContainer:
            {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
            },

        SplashScreen_RootView:
            {
                flex:1,
                margin: 10,
                position: 'absolute',
                width: '100%',
                height: '100%',
                backgroundColor:'#333333'

            },

        SplashScreen_ChildView:
            {
                flex:1,
                justifyContent:'center',
                alignItems:'center',
            },

        TouchableOpacity_Style:{

            width:25,
            height: 25,
            top:9,
            right:9,
            position: 'absolute'

        }
    });