export default {
    floating_btn_text:{
        position: 'relative',
        color:'#fff',
        fontSize:12,
        fontFamily:'Calibri Bold',
        right:3,
        left:3,
        textAlign:'center',

    },
    fabopencard:{
        position: 'absolute',
        width: 230,
        height: 320,
        right: 10,
        backgroundColor:'#fff',
        borderRadius:5,
        bottom: -2,


    },
    newmodalstyle:{
        flex: 1,
        width: '100%',
        height: 300,
        alignContent:'flex-end',
        justifyContent:'flex-end',
        alignItems:'flex-end',
        backgroundColor:'rgba(0,0,0,0.7)',
    },
    rowViewContainers: {
        fontSize: 14,
        paddingTop: 8,
        paddingLeft: 4,
        paddingRight: 4,
        paddingBottom: 8,
        color:'#333333',
        marginTop:3,
        marginBottom:5,
        width:170,
    },
    floating_btn:{
        position: 'absolute',
        flexDirection:'row',
        width: 145,
        height:45,
        right: 10,
        padding:10,
        paddingRight:15,
        bottom: 0,
        backgroundColor:'#333333'
    },
    main_cardstyle:{
        width:'90%',
        height:135,
        marginTop:10,
        marginLeft:15,
        padding:10,
        marginRight:10,
        marginBottom:5,
        backgroundColor:'#fff'
    },
    main_view:{
        width:'100%',
        height:'auto',
        padding:10,
        backgroundColor:'#fff'


    },
    kavita_name:{
        fontSize:16,
        paddingTop:5,  paddingLeft:5,  paddingRight:5,  paddingBottom:0,
        fontFamily:'Calibri Bold'
    },
    lekhak_name:{
        fontSize:14,
        paddingTop:5,  paddingLeft:5,  paddingRight:5,  paddingBottom:0,
        fontFamily:'Calibri Bold',
        color:'gray'
    },
    rate_style:{
        fontSize:14,
        fontFamily:'Calibri Bold',
        color:'gray'
    },
    header_text_style:{fontSize:14,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'45%',color:'#fff',fontWeight:'bold',},
    status_right : {
        justifyContent:'flex-end',
        alignItems:'flex-end',
        textAlign:'right',
        right:0,
        backgroundColor:'#D977AB',
        alignSelf: 'flex-end',

    },
    status_right_text : {
        fontSize:14,
        color:'#fff',
        paddingTop:1,
        fontWeight:'bold',
        paddingLeft:3,
        paddingRight:3,
        paddingBottom:0,
        textAlign:'center',
        fontFamily:'Calibri Bold',

    },

    view_section:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
    },
    left_view:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-start',
    },
    right_view:{
        flex:1,
        flexDirection:'column',
        justifyContent:'flex-end',
        alignItems:'flex-end',

    },
    cake_name:{
        fontWeight:'bold',
        fontSize:15,
        color:'#282828',
        fontFamily:'Calibri Bold',
    },
    cake_desc:{
        fontWeight:'bold',
        fontSize:12,
        paddingTop:'2%',
        paddingBottom:'2%',
        color:'#8A8A8C',
        fontFamily:'Calibri Bold',
    },
    cake_price:{
        fontWeight:'bold',
        fontSize:12,
        paddingTop:'1%',
        paddingBottom:'2%',
        color:'#8A8A8C',
        fontFamily:'Calibri Bold',
    },
    underlineview:{

        backgroundColor:'#000',
        width:'100%',
        height:0.5,
        margin:10,
    },
}