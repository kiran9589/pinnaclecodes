export default
{
    "totalCount": "3",
    "products": [
    {
        "p_id": 1,
        "productCode": "",
        "productName": "BELGIAN TRUFFLE",
        "category": "CUPCAKES",
        "imageName": "1560597533887_BELGIANTRUFFLE-32.jpg",
        "imagePath": "/productImg/1560597533887_BELGIANTRUFFLE-32.jpg",
        "weightPrice": [
            "{\"weight\":\"0\",\"price\":32}"
        ]
    },
    {
        "p_id": 4,
        "productCode": "",
        "productName": "BLACK FOREST",
        "category": "CUPCAKES",
        "imageName": "1560598693997_BLACKFOREST-38.jpg",
        "imagePath": "/productImg/1560598693997_BLACKFOREST-38.jpg",
        "weightPrice": [
            "{\"weight\":\"0\",\"price\":38}"
        ]
    },
    {
        "p_id": 5,
        "productCode": "",
        "productName": "BLUEBERRY",
        "category": "CUPCAKES",
        "imageName": "1560600270197_BLUEBERRY-60.jpg",
        "imagePath": "/productImg/1560600270197_BLUEBERRY-60.jpg",
        "weightPrice": [
            "{\"weight\":\"0\",\"price\":60}"
        ]
    }
]
}
