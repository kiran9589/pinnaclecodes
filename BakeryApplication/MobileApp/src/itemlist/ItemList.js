import React, {Component} from 'react';
import {ImageBackground, StatusBar,AsyncStorage,StyleSheet,  View,Image,TextInput,
    Modal,ScrollView,Dimensions,ActivityIndicator,TouchableOpacity,FlatList} from 'react-native';
import { Container, Header, Content, Button, Text,Icon,Body,Left,Right,Title,Badge} from 'native-base';
import styles from './itemlist.style';
var Realm = require('realm');
let realm ;
export default class ItemList extends Component {

    constructor() {
        super();

        this.state = {

            ActivityIndicator_Loading: false,
            modalVisibleThree:false,
            dataSource: {},
            start:0,
            totalCount:0,
            data:[],
            isLoading: true,
            refreshing:false,
            weightPrice:'',
            page_count:1,
            total_cart_count:0,

        }
    }
    FunctionToOpen_DetailPoem=(product_id,item_overallRating)=>{
        this.props.navigation.navigate('DescriptionScreen',{product_id,item_overallRating});
    }

    LoadDetailService_ModalCLose = () =>{

        /* this.selectionOnPress("btn_clicked");*/
        this.toggleModalThree(false);

    }

    componentWillMount(){
        this.loadItemList();
    }

    toggleModalThree(visible) {
        this.setState({ modalVisibleThree: visible });
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Category');
    }

/*==================================
* Component DId Mount Method Called
* =====================================*/
componentDidMount()
{
    this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {
            try{
                realm = new Realm({ path: 'CakeDatabase.realm'});
                var cart_count = realm.objects('Cart_Details').length;
                this.setState({total_cart_count:cart_count})
            }
            catch (e) {
                this.setState({total_cart_count:0})
            }


        }
    );

}


    /*=====================
    * Load Item List using Flatlist
    * =====================*/

    loadItemList=()=>{
        return fetch('http://3.13.212.113:3000/product/list',
            {
                method: 'POST',
                headers:
                    {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },

                body: JSON.stringify(
                    {
                        start: this.state.start,
                        limit: 10,
                        category: this.props.navigation.state.params.item_id,
                    })

            }).then((response) => response.json()).then((response) =>
        {

            var totalCount= response.totalCount;
            var productlist= response.products;
            var weightPrice=response.products.weightPrice;
            this.setState({
                totalCount:totalCount,
                data:[...this.state.data , ...productlist],
                isLoading: false,
                refreshing:false,
                weightPrice:weightPrice,
                //dataSource: response.data.items,
            });


        }).catch((error) =>
        {
            console.error(error);

        });


    }
onMyCart=()=>{
        this.props.navigation.navigate('Mycart');
}

/*===================
* Flat List Event
* ===================*/

handleRefresh = () => {
        this.setState({

                start: 0,
                refreshing: true,
                data:[],
                page_count:1,
            },
            ()=>{
                this.loadItemList();
            })
        console.log("Pages Number at handleRefresh = " +this.state.page_count);
}

handleLoadMore = () =>{
        this.setState({
            start:this.state.start + 10,
            page_count:this.state.page_count+1,

        },()=>{
            var pagecount=this.state.totalCount/10;
            var total_page=Math.ceil(pagecount);

            if(this.state.page_count<=total_page)
            {
                this.loadItemList();
            }
            else
            {
                console.log("Data Not Found");
            }

        })
}

/*=======================
* UI Started
* ======================*/

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size={'large'} color={'#D977AB'}/>
                    <Text style={{color:'#D977AB'}}>Please Wait .....</Text>
                </View>
            );
        }
        return (
            <Container style={{backgroundColor:'#FCECF4'}}>
                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}} onPress={()=>this.handleBackPress()}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                    <Body>
                        <Text style={[styles.header_text_style,{paddingTop:3}]}>{this.props.navigation.state.params.item_name}</Text>

                    </Body>
                    <Right>
                        {this.state.total_cart_count!=0 ?
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>

                            :
                            <TouchableOpacity onPress={()=>this.onMyCart()}>
                                <Badge style={{marginLeft:16,top:15,backgroundColor:'#D977AB',width:22,height:22}} >
                                    <Text style={{fontSize:10,top:-2,fontWeight:'bold'}}>{this.state.total_cart_count}</Text>
                                </Badge>
                                <Icon type="SimpleLineIcons" name="basket"
                                      style={{fontSize: 30, color: '#D977AB',paddingRight:'1%',}}/>
                            </TouchableOpacity>
 }

                    </Right>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10,flexDirection:'column',marginBottom:10,}}>


                    <FlatList
                        data={ this.state.data}
                        keyExtractor={(item, index) => index}
                        refreshing={this.state.refreshing}
                        onRefresh={()=> this.handleRefresh()}
                        onEndReached={()=>this.handleLoadMore()}
                        renderItem={({item}) =>

                        <View style={{borderRadius:5,height:'auto',backgroundColor:'#fff',margin:5}}>
                            <TouchableOpacity onPress={()=>this.FunctionToOpen_DetailPoem(item.p_id,item.overallRating)}>
                                <View style={{flexDirection:'row',width:'100%'}}>
                                    <View style={{padding:5,width:'25%',marginRight:'3%'}}>
                                        <Image source = {{uri:'http://3.13.212.113:3000'+item.imagePath}}
                                               style={{width:80,height:80,}}/>
                                    </View>
                                    <View style={{flexDirection:'column',width:'70%',padiingLeft:'1%'}}>
                                        {
                                            item.overallRating===0 ?
                                                <View style={[styles.status_right,{backgroundColor:'#fff'}]}>
                                                    <Text style={[styles.status_right_text,{padding:2}]} >{item.overallRating}</Text>
                                                </View> :

                                            <View style={[styles.status_right,]}>
                                                <Text style={[styles.status_right_text,{padding:2}]} >{item.overallRating}</Text>
                                            </View>
                                        }



                                        <Text style={[styles.cake_name,{fontSize:14,width:'80%'}]} numberOfLines={1}>{item.productName} </Text>
                                        <Text style={styles.cake_desc}>Fresho Signature Tea Cake - Fruit, 250 g </Text>
                                        <View style={{flexDirection:'row',paddingBottom:'1%',paddingTop:0}}>
                                            <Icon type="FontAwesome" name="rupee"
                                                  style={{fontSize: 18, color: '#E9C789',paddingTop:'1%',paddingRight:'2%'}}/>
                                            <Text style={styles.cake_price}>{JSON.parse(item.weightPrice).price}</Text>

                                            {
                                                JSON.parse(item.weightPrice).weight > 0 ?
                                                    <View style={{flexDirection:'row',paddingLeft:'20%',}}>
                                                    <Icon type="FontAwesome5" name="weight"
                                                          style={{fontSize: 18, color: '#F4C9C7',paddingTop:'1%',paddingRight:'2%'}}/>
                                                    <Text style={styles.cake_price}>{JSON.parse(item.weightPrice).weight} Kg.</Text>

                                                </View> : null

                                            }
                                        </View>



                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    />






                </Content>


                <Modal animationType = {"slide"} transparent = {true}
                       visible = {this.state.modalVisibleThree}
                       onRequestClose={() => this.toggleModalThree(false)}
                       onBackdropPress={()=>thid.toggleModalThree(false)}>


                    <View style={styles.newmodalstyle}>
                        <View
                            style={styles.fabopencard}>

                                    <View style={{flexDirection:'column',}}>
                                        <View style={{flexDirection:'row',margin:0}}>
                                            <Image source={require('../../images/cake_logo.png')}
                                                   style={{width:20,height:20,marginTop:10,marginLeft:5}}/>

                                            <Text style={styles.rowViewContainers}
                                                  onPress={()=>{this.LoadDetailService_ModalCLose()}}> All </Text>

                                        </View>

                                        <View style={{flexDirection:'row',margin:0}}>
                                            <Image source={require('../../images/cake_logo.png')}
                                                   style={{width:20,height:20,marginTop:10,marginLeft:5}}/>

                                            <Text style={styles.rowViewContainers}
                                                  onPress={()=>{this.LoadDetailService_ModalCLose()}}> Weeding </Text>

                                        </View>

                                        <View style={{flexDirection:'row',margin:0}}>
                                            <Image source={require('../../images/cake_logo.png')}
                                                   style={{width:20,height:20,marginTop:10,marginLeft:5}}/>

                                            <Text style={styles.rowViewContainers}
                                                  onPress={()=>{this.LoadDetailService_ModalCLose()}}> Customizing </Text>

                                        </View>

                                        <View style={{flexDirection:'row',margin:0}}>
                                            <Image source={require('../../images/cake_logo.png')}
                                                   style={{width:20,height:20,marginTop:10,marginLeft:5}}/>

                                            <Text style={styles.rowViewContainers}
                                                  onPress={()=>{this.LoadDetailService_ModalCLose()}}> Sponge</Text>

                                        </View>

                                        <View style={{flexDirection:'row',margin:0}}>

                                            <Image source={require('../../images/cake_logo.png')}
                                                    style={{width:20,height:20,marginTop:10,marginLeft:5}}/>
                                            <Text style={styles.rowViewContainers}
                                                  onPress={()=>{this.LoadDetailService_ModalCLose()}}> More </Text>
                                        </View>
                                    </View>

                        </View>
                    </View>
                </Modal>

            </Container>
        );
    }
}

