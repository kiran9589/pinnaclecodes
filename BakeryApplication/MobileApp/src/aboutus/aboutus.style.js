export default {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    welcome: {
        fontSize:20,fontFamily:'Calibri Regular',marginLeft:'10%',marginTop:10,
        color:'#D977AB',fontWeight:'bold',textAlign:'center',alignItems:'center'
    },
    instructions: {
        textAlign: 'justify',
        color: '#333333',
        marginBottom: 5,
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:20,
        paddingRight:20,
        fontSize:16,
        lineHeight:30,
    },
    header_style:{backgroundColor:'#333333',padding:10,height:60},
    main_salon_name:{fontSize:18,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'25%',color:'#fff',fontWeight:'bold',},
}