/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
    StatusBar,
    PermissionsAndroid,
    ListView,
    Image,
    TouchableOpacity
} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text, Title, Left, Button, Icon, Right} from 'native-base';

import styles from './aboutus.style';
import Dashboard from "../dashboard/Dashboard";

type Props = {};
export default class Aboutus extends Component<Props> {

onbackpress=()=>{
    this.props.navigation.navigate('MyDrawerNavigator');
}
    render() {
        return (
            <Container style={{backgroundColor:'#FCECF4',}}>

                <Header  style={styles.header_style}>
                    <Left style={{flexDirection:'row'}}>
                        <Button transparent style={{marginTop:10}} onPress={()=>this.onbackpress()}>
                            <Icon name='arrow-back' />
                        </Button>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.main_salon_name}>About Us</Title>

                    </Body>


                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />
                <Content style={{padding:10}}>
                    <View style={styles.container}>
                        {/*<Image source={require('../../images/user_image.jpg')}
                               style={{height:120,width:120,}}  />
                        */}<Text style={styles.welcome}>Welcome to the COUCHES ..</Text>
                        <Text style={styles.instructions}>THE PATISSERIE, a Pune based online bakery!
                            It all started by Anushka Jaju, who embarked on her career as a Chef from Institute of Hotel Management (IHM),Jaipur.
                            With a passion for baking, she took her talent and creativity to the next level by baking a very finest range of customized cakes that can immediately bring a Joy to any event. Here we season each of our cake with the European and modern techniques to take you to the rich creamy world.
                            With a belief that‘Cakes are the Trademarks of celebration’, she started an online Premium Cake venture- COUCHES .. THE PATISSERIE!</Text>
                    </View>
                </Content>

            </Container>

        );
    }
}

