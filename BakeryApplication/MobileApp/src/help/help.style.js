export default {
    logo: {width: 100, height:100,},
    image_container: {justifyContent: 'center', alignItems: 'center',marginTop:'20%',},
    help_cardstyle:{
        width:'90%',
        padding:10,
        marginTop:'10%',
        marginLeft:'5%',
        marginRight:'5%',
        backgroundColor:'#fff',
        borderRadius:10
    },
    help_text:{fontSize:16,fontFamily:'Calibri Bold',textAlign:'center'},
    help_email:{fontSize:16,fontFamily:'Calibri Regular',textAlign:'center',marginLeft:'10%'},
    btn_style:{
        alignItems:'center',
        justifyContent:'center',
        textAlign: 'center',
        width:'60%',
        height: 50,
        borderWidth:1,
        borderRadius: 5,
        marginTop:5,
        marginLeft:'20%',
        marginBottom:10,
    },
}