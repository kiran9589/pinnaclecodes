/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View, Image, ImageBackground, Linking, StatusBar, TouchableOpacity} from 'react-native';
import { Container,Content, Header, Item, Input, Icon, Button, Text,Right,Title, Subtitle,Body,Left } from 'native-base';
import Toast from 'react-native-simple-toast';
import styles from './help.style';

type Props = {};

export default class Help extends Component<Props> {
    render() {
        return (
            <Container style={{backgroundColor:'#FCECF4'}}>


                <Header style={{backgroundColor:'#333333',paddingTop:5,paddingLeft:10,paddingRight:10,height:65}}>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyDrawerNavigator')}>
                            <Image source={require('../../images/logo_app.png')}
                                   style={{height:60,width:100,}}  />
                        </TouchableOpacity>
                    </Left>

                    <Body style={{padding:5,}}>
                    <Title style={{fontSize:16,fontFamily:'Proxima Nova Reg',paddingTop:6,paddingLeft:'20%',color:'#fff',fontWeight:'bold',}}>
                        Help Us</Title>

                    </Body>

                </Header>
                <StatusBar
                    backgroundColor="#333333"
                    barStyle="light-content"
                />

                <ImageBackground source={require('../../images/help_back.jpg')}
                                 style={{width:'100%',height:'100%'}}>

                    <View style={{backgroundColor:'rgba(#D977AB,0.8)',width:'100%',height:'100%'}}>
                    <View style={styles.image_container}>
                        <Image

                            style={styles.logo}

                        />
                    </View>
                    <View
                        style={styles.help_cardstyle}>
                        <Text style={styles.help_text}>If You have any Query then Contact to Our Customer Care</Text>

                        <View style={{flexDirection:'row',padding:10}}>
                            {/*<Image source={require('../../images/styllon_help_email.png')}
                                   style={{width:24,height:24}} />*/}


                            <Icon type="Fontisto" name="email"
                                  style={{fontSize: 24, color: '#D977AB',}}/>

                            <Text style={styles.help_email}> couches.cakery@gmail.com</Text>
                        </View>

                        <View style={{flexDirection:'row',padding:10}}>
                         {/*   <Image source={require('../../images/styllon_help_call.png')}
                                   style={{width:24,height:24}} />*/}

                            <Icon type="Zocial" name="call"
                                  style={{fontSize: 24, color: '#D977AB',}}/>

                            <Text style={styles.help_email}> (+91) 700-042-4471</Text>
                        </View>



                    </View>
                    </View>
                </ImageBackground>


            </Container>
        );
    }
}

